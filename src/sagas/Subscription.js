import { all, call, fork, put, takeEvery } from "redux-saga/effects";
import { push } from 'react-router-redux';
import axios from '../services';
import * as actionTypes from "../constants/ActionTypes";
import * as actions from '../actions';
import { toast } from "react-toastify";
import moment from 'moment';

function* subscriptionGetUserSubscriptionPlans({ payload }) {
    try {
        yield put(actions.subscriptionShowLoader("mainLoader"));
        let response = yield axios.get("/subscription/");
        if (response.status == 200) {
            yield put(actions.subscriptionGetUserSubscriptionPlansSuccess(response.data.data));
            yield put(actions.subscriptionHideLoader("mainLoader"));
        } else {
            yield put(actions.subscriptionHideLoader("mainLoader"));

            yield toast.error("Something went wrong", {
                toastId: "offlinePayment",
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
        }
    } catch (error) {

        yield put(actions.subscriptionHideLoader("mainLoader"));
        if (error == "Error: Request failed with status code 405") {
            yield put(push("/signin"));
            yield put(actions.userSignOutSuccess());
            yield localStorage.clear();
            yield toast.error("Please Login Again", {
                toastId: "offlinePayment",
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
        }
    }
}


function* subscriptionGetUserSubscriptionDetails({ payload }) {
    try {
        yield put(actions.subscriptionShowLoader("mainLoader"));
        let response = yield axios.get("/subscription/active");
        if (response.status == 200) {
        yield put(actions.subscriptionGetUserSubscriptionDetailsSuccess(response.data.data));
          
        } else {
            yield put(actions.locationHideLoader());
            yield toast.error("Something went wrong", {
                toastId: "offlinePayment",
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
        }
    } catch (error) {

        yield put(actions.locationHideLoader());
        if (error == "Error: Request failed with status code 405") {
            yield put(push("/signin"));
            yield put(actions.userSignOutSuccess());
            yield localStorage.clear();
            yield toast.error("Please Login Again", {
                toastId: "offlinePayment",
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
        }
    }
}

function* subscriptionBuySubscriptionPlan({ payload }) {
    const { planId, partyId, customerId } = payload;
    console.log("TCL: function*subscriptionBuySubscriptionPlan -> payload", payload)
    try {
        yield put(actions.subscriptionShowLoader("buyLoader"));
        let response = yield axios.post(`/subscription/buy/${planId}`, { partyId, customerId });
        if (response.status == 200) {
            // yield put(actions.subscriptionGetUserSubscriptionPlansSuccess(response.data.data));
            yield put(actions.subscriptionHideLoader("buyLoader"));
            yield toast.success("Subscription buy successfully", {
                toastId: "offlinePayment",
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
        } else {
            yield put(actions.subscriptionHideLoader("buyLoader"));
            yield toast.error("Something went wrong", {
                toastId: "offlinePayment",
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
        }
    } catch (error) {

        yield put(actions.subscriptionHideLoader("buyLoader"));
        if (error == "Error: Request failed with status code 405") {
            yield put(push("/signin"));
            yield put(actions.userSignOutSuccess());
            yield localStorage.clear();
            yield toast.error("Please Login Again", {
                toastId: "offlinePayment",
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
        }
    }
}

export default function* rootSaga() {
    yield takeEvery(actionTypes.SUBSCRIPTION_GET_USER_SUBSCRIPTION_DETAILS, subscriptionGetUserSubscriptionDetails);
    yield takeEvery(actionTypes.SUBSCRIPTION_GET_USER_SUBSCRIPTION_PLANS, subscriptionGetUserSubscriptionPlans);
    yield takeEvery(actionTypes.SUBSCRIPTION_BUY_SUBSCRIPTION_PLAN, subscriptionBuySubscriptionPlan)
}