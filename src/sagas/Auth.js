import { all, call, fork, put, takeEvery } from "redux-saga/effects";
import axios from '../services';
import { toast } from "react-toastify";
import { showAuthMessage, userSignInSuccess, forgetPasswordSuccess, resetPasswordSuccess } from "actions/Auth";
import {
    hideAuthLoader,
    showAuthMessageSuccess,
    createPasswordSuccess
} from "../actions/Auth";
import { SIGNIN_USER, FORGET_PASSWORD, RESET_PASSWORD, CREATE_PASSWORD } from "../constants/ActionTypes";

// Sigin in API integration
function* signInUserWithEmailPassword({ payload }) {
    const { email, password } = payload;
    try {
        let signInUser = yield axios.post("/login", { email, password });
        console.log("TCL: function*signInUserWithEmailPassword -> signInUser", signInUser)
        if (signInUser.status == 200) {
            localStorage.setItem('user_id', signInUser.data.data.userId);
            localStorage.setItem('token', signInUser.data.data.accessToken);
            localStorage.setItem('role', signInUser.data.data.customer.roleIs);
            yield put(userSignInSuccess(signInUser.data.data.userId));
        } else {
            yield toast.error(signInUser.response.data.message, {
                toastId: "offlinePayment",
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
            yield put(showAuthMessage(""));
        }
    } catch (error) {
    console.log("TCL: function*signInUserWithEmailPassword -> error", error.message)
        if(error.message == "Request failed with status code 401"){
        yield toast.error("Invalid Password", {
            toastId: "offlinePayment",
            hideProgressBar: true,
            autoClose: 3000,
            position: toast.POSITION.TOP_RIGHT
        });
    }else if(error.message == "Request failed with status code 404"){
        yield toast.error("User Not found", {
            toastId: "offlinePayment",
            hideProgressBar: true,
            autoClose: 3000,
            position: toast.POSITION.TOP_RIGHT
        });
    }
        yield put(showAuthMessage(""));

    }
}
// Forget Password API integration
function* forgetPass({ payload }) {
    const { email, resend } = payload
    try {
        let signInUserEmail = yield axios
            .post("/forgot-password", {
                email: email
            });


        if (signInUserEmail.status == 200) {
            // localStorage.setItem('user_id', signInUserEmail.data.data.userId);
            yield put(hideAuthLoader());
            if (resend) {
                yield toast.success("Verification code is resended to registered mail.", {
                    toastId: "offlinePayment",
                    hideProgressBar: true,
                    autoClose: 3000,
                    position: toast.POSITION.TOP_RIGHT
                });
            } else {
                yield toast.success(signInUserEmail.data.message, {
                    toastId: "offlinePayment",
                    hideProgressBar: true,
                    autoClose: 3000,
                    position: toast.POSITION.TOP_RIGHT
                });
            }

            yield put(showAuthMessageSuccess(signInUserEmail.data.message));
            yield put(forgetPasswordSuccess());
        } else {
            yield toast.error(signInUserEmail.response.data.message, {
                toastId: "offlinePayment",
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
        }
    } catch (error) {
        if (error.message === "Request failed with status code 404") {
            yield toast.error("User doesn't exist", {
                toastId: "offlinePayment",
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
        }
        yield put(showAuthMessage(error.message));
    }
}
// Reset Password API integration
function* resetPass({ payload }) {
    const { otp, password } = payload;

    try {
        let resetPasswordResponse = yield axios.post("/reset-password", {
            verificationCode: otp,
            newPassword: password,
            confirmPassword: password
        });
        if (resetPasswordResponse.status == 200) {
            // localStorage.setItem('user_id', resetPasswordResponse.data.data.userId);
            yield put(hideAuthLoader());
            yield put(showAuthMessageSuccess(resetPasswordResponse.data.message));
            yield put(resetPasswordSuccess());
            yield toast.success("Password Reset Successfully", {
                toastId: "offlinePayment",
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
        } else {
            yield put(showAuthMessage(resetPasswordResponse.response.data.message));
        }
    } catch (error) {

        yield put(showAuthMessage(error.message));
    }
}
//Create New Passoword API 
function* createPassword({ payload }) {
    const { otp, password } = payload;

    try {
        let createPasswordResponse = yield axios.post("/create-password", {
            verificationCode: otp,
            newPassword: password,
            confirmPassword: password
        });
        if (createPasswordResponse.status == 200) {
            // localStorage.setItem('user_id', createPasswordResponse.data.data.userId);
            yield put(hideAuthLoader());
            yield put(showAuthMessageSuccess(createPasswordResponse.data.message));
            yield put(createPasswordSuccess());
            yield toast.success("Password Created Successfully", {
                toastId: "offlinePayment",
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
        } else {
            yield put(showAuthMessage(createPasswordResponse.response.data.message));
        }
    } catch (error) {

        yield put(showAuthMessage(error.message));
    }
}
export default function* rootSaga() {
    yield all([
        yield takeEvery(SIGNIN_USER, signInUserWithEmailPassword),
        yield takeEvery(FORGET_PASSWORD, forgetPass),
        yield takeEvery(RESET_PASSWORD, resetPass),
        yield takeEvery(CREATE_PASSWORD, createPassword)

    ]);

}