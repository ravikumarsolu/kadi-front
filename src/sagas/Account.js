import { all, call, fork, put, takeEvery } from "redux-saga/effects";
import { push } from 'react-router-redux';
import axios from '../services';
import * as actionTypes from "../constants/ActionTypes";
import * as actions from '../actions';
import { toast } from "react-toastify";
import moment from 'moment';
function* accountGetAllDevices({ payload }) {
    try {
        yield put(actions.accountShowLoader());
        let response = yield axios.get("/device?skip=0&limit=100");
        if (response.status == 200) {
            yield put(actions.accountGetAllDevicesSuccess(response.data.data));
            yield put(actions.accountHideLoader());
        } else {
            yield put(actions.accountHideLoader());
            yield toast.error("Something went wrong", {
                toastId: "offlinePayment",
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
        }
    } catch (error) {

        yield put(actions.accountHideLoader());
        if (error == "Error: Request failed with status code 405") {
            yield put(push("/signin"));
            yield put(actions.userSignOutSuccess());
            yield localStorage.clear();
            yield toast.error("Please Login Again", {
                toastId: "offlinePayment",
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
        }
    }
}

function* accountHelpSupportRequest({ payload }) {
    try {
        yield put(actions.accountShowLoader("helpLoader"));
        let response = yield axios.post("/ticket/helpAndSupport",payload);
        console.log("TCL: function*accountHelpSupportRequest -> response", response)
        if (response.status == 200) {
            // yield put(actions.accountHelpSupportRequestSuccess(response.data.data));
            yield toast.success(response.data.message, {
                toastId: "offlinePayment",
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
            yield put(actions.accountHideLoader("helpLoader"));
        } else {
            yield put(actions.accountHideLoader("helpLoader"));
            yield toast.error("Something went wrong", {
                toastId: "offlinePayment",
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
        }
    } catch (error) {

        yield put(actions.accountHideLoader());
        if (error == "Error: Request failed with status code 405") {
            yield put(push("/signin"));
            yield put(actions.userSignOutSuccess());
            yield localStorage.clear();
            yield toast.error("Please Login Again", {
                toastId: "offlinePayment",
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
        }
    }
}

function* accountChangePasswordRequest({ payload }) {
    try {
        yield put(actions.accountShowLoader("changePassword"));
        let response = yield axios.post("/change-password",payload);
        console.log("TCL: function*accountChangePasswordRequest -> response", response)
        if (response.status == 200) {
            yield put(actions.accountClearChangePassword());
            yield toast.success(response.data.message, {
                toastId: "offlinePayment",
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
            yield put(actions.accountHideLoader("changePassword"));
        } if (response.status == 400) {
            yield toast.error(response.message, {
                toastId: "offlinePayment",
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
         } else {
            yield put(actions.accountHideLoader("changePassword"));
            yield toast.error("Something went wrong", {
                toastId: "offlinePayment",
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
        }
    } catch (error) {
    console.log("TCL: function*accountChangePasswordRequest -> error", error)

        yield put(actions.accountHideLoader());
        if (error == "Error: Request failed with status code 405") {
            yield put(push("/signin"));
            yield put(actions.userSignOutSuccess());
            yield localStorage.clear();
            yield toast.error("Please Login Again", {
                toastId: "offlinePayment",
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
        }else if("Error: Request failed with status code 400"){
            yield toast.error("The old password is incorrect. Please try again", {
                toastId: "offlinePayment",
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
        }
    }
}

export default function* rootSaga() {
    yield takeEvery(actionTypes.ACCOUNT_GET_ALL_DEVICES, accountGetAllDevices);
    yield takeEvery(actionTypes.ACCOUNT_HELP_SUPPORT_REQUEST,accountHelpSupportRequest);
    yield takeEvery(actionTypes.ACCOUNT_CHANGE_PASSWORD_REQUEST,accountChangePasswordRequest)
}