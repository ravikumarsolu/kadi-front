import { all, call, fork, put, takeEvery } from "redux-saga/effects";
import { push } from 'react-router-redux';
import axios from '../services';
import * as actionTypes from "../constants/ActionTypes";
import * as actions from '../actions';
import { toast } from "react-toastify";
import moment from 'moment';
function* deviceGetAllDeviceRequest({ payload }) {
    try {
        // yield put(actions.deviceShowLoader());
        let response = yield axios.get("/device?skip=0&limit=100");
        if (response.status == 200) {
            yield put(actions.deviceGetAllDeviceRequestSuccess(response.data.data));
            yield put(actions.deviceHideLoader());
        } else {
            yield put(actions.deviceHideLoader());
            yield toast.error("Something went wrong", {
                toastId: "offlinePayment",
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
        }
    } catch (error) {

        yield put(actions.deviceHideLoader());
        if (error == "Error: Request failed with status code 405") {
            yield put(push("/signin"));
            yield put(actions.userSignOutSuccess());
            yield localStorage.clear();
            yield toast.error("Please Login Again", {
                toastId: "offlinePayment",
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
        }
    }
}

function* deleteDevice({ payload }) {
    try {
        yield put(actions.deviceShowLoader());
        let response = yield axios.delete(`/device/${payload}`);
        if (response.status == 200) {
            yield put(actions.deviceGetAllDeviceRequest());
            yield toast.success("Device deleted", {
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
        } else {

        }
    } catch (error) {
        if (error == "Error: Request failed with status code 405") {
            yield put(push("/signin"));
            yield put(actions.userSignOutSuccess());
            yield localStorage.clear();
            yield toast.error("Please Login Again", {
                toastId: "offlinePayment",
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
        }
    }
}

function* deviceDeviceStatusChanged({ payload }) {
    const { id, status, index, from } = payload;
    let response;
    try {
        if (from == "device") {
            response = yield axios.put(`/device/${id}`, {
                isActive: status
            });
        } else if (from == "solar") {
            response = yield axios.put(`/device/${id}`, {
                solarStatus: status
            });
        } else if (from == "battery") {
            response = yield axios.put(`/device/${id}`, {
                batteryStatus: status
            });
        } else if (from == "generator") {
            response = yield axios.put(`/device/${id}`, {
                generatorStatus: status
            });
        }

        if (response.status == 200) {
            yield put(actions.deviceDeviceStatusChangedSuccess({ index, from }));
            yield toast.success("Device Status Changed", {
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
        } else {
            yield toast.error("Error in Status Change", {
                toastId: "offlinePayment",
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
        }
    } catch (error) {
        if (error == "Error: Request failed with status code 405") {
            yield put(push("/signin"));
            yield put(actions.userSignOutSuccess());
            yield localStorage.clear();
            yield toast.error("Please Login Again", {
                toastId: "offlinePayment",
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
        }
    }
}

function* deviceDeviceAddRequest({ payload }) {
    const { isEdit, deviceImei, deviceName, deviceIp, locationId } = payload;
    try {
        if (isEdit) {
            let response = yield axios.put(`/device/${deviceImei}`, {
                deviceIp, deviceName, locationId
            });
            if (response.status == 200) {
                // yield put(staffHideLoader());
                yield put(actions.deviceDeviceClear());
                yield put(actions.deviceGetAllDeviceRequest());
                yield toast.success("Device Updated", {
                    toastId: "offlinePayment",
                    hideProgressBar: true,
                    autoClose: 3000,
                    position: toast.POSITION.TOP_RIGHT
                });
            } else {
                yield toast.error("Device Not Updated", {
                    toastId: "offlinePayment",
                    hideProgressBar: true,
                    autoClose: 3000,
                    position: toast.POSITION.TOP_RIGHT
                });
            }
        } else {
            let response = yield axios.post("/device", payload);
            if (response.status == 200) {
                yield put(actions.deviceDeviceClear());
                yield put(actions.deviceGetAllDeviceRequest());
                yield toast.success("Device Added", {
                    toastId: "offlinePayment",
                    hideProgressBar: true,
                    autoClose: 3000,
                    position: toast.POSITION.TOP_RIGHT
                });
            } else {
                yield toast.error("Device Not Added", {
                    toastId: "offlinePayment",
                    hideProgressBar: true,
                    autoClose: 3000,
                    position: toast.POSITION.TOP_RIGHT
                });
            }
        }
    } catch (error) {
        yield put(actions.deviceHideLoader());
        if (error == "Error: Request failed with status code 405") {
            yield put(push("/signin"));
            yield put(actions.userSignOutSuccess());
            yield localStorage.clear();
            yield toast.error("Please Login Again", {
                toastId: "offlinePayment",
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
        }
    }
}

function* deviceGetDeviceRequest({ payload }) {
    try {
        yield put(actions.deviceShowLoader());
        let response = yield axios.get(`/device/${payload}`);
        if (response.status == 200) {
            yield put(actions.deviceGetDeviceRequestSuccess(response.data.data));
            yield put(actions.deviceHideLoader());
        } else {
            yield put(actions.deviceHideLoader());
            yield toast.error("Something went wrong", {
                toastId: "offlinePayment",
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
        }
    } catch (error) {
        yield put(actions.deviceHideLoader());
        if (error == "Error: Request failed with status code 405") {
            yield put(push("/signin"));
            yield put(actions.userSignOutSuccess());
            yield localStorage.clear();
            yield toast.error("Please Login Again", {
                toastId: "offlinePayment",
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
        }
    }
}

function* deviceGetDeviceStatistics({ payload }) {
    const { id, trend, from, to } = payload;

    try {
        // yield put(actions.deviceShowLoader());
        let response;
        if (trend === "custom") {
            response = yield axios.get(`aws/?id=${id}&trend=${trend}`, { params: { from: moment(from).format("YYYY-MM-DD"), to: moment(to).format("YYYY-MM-DD") } });
        }
        else {
            response = yield axios.get(`aws/?id=${id}&trend=${trend}`);
        }
        if (response.status == 200) {
            if (trend === "current") {
                yield put(actions.deviceGetDeviceStatisticsSuccess({ data: response.data.data.data.Items[0], trend: trend }));
            } else if (trend === "daily") {
                yield put(actions.deviceGetDeviceStatisticsSuccess({ data: response.data.data, trend: trend }));
            } else if (trend === "weekly") {
                yield put(actions.deviceGetDeviceStatisticsSuccess({ data: response.data.data, trend: trend }));
            } else if (trend === "monthly") {
                yield put(actions.deviceGetDeviceStatisticsSuccess({ data: response.data.data, trend: trend }));
            } else if (trend === "yearly") {
                yield put(actions.deviceGetDeviceStatisticsSuccess({ data: response.data.data, trend: trend }));
            } else {
                yield put(actions.deviceGetDeviceStatisticsSuccess({ data: response.data.data, trend: trend }));

            }
            yield put(actions.deviceHideLoader());
        } else {
            yield put(actions.deviceHideLoader());
            yield toast.error("Something went wrong", {
                toastId: "offlinePayment",
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
        }
    } catch (error) {
        yield put(actions.deviceHideLoader());
        if (error == "Error: Request failed with status code 405") {
            yield put(push("/signin"));
            yield put(actions.userSignOutSuccess());
            yield localStorage.clear();
            yield toast.error("Please Login Again", {
                toastId: "offlinePayment",
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
        }
    }
}
function* deviceGetAllLocations({ payload }) {
    try {
        let response = yield axios.get("/location?skip=0&limit=100");
        if (response.status == 200) {
            yield put(actions.deviceGetAllLocationRequestSuccess(response.data.data));
            // yield put(actions.deviceHideLoader());
        } else {
            yield put(actions.deviceHideLoader());
            yield toast.error("Something went wrong", {
                toastId: "offlinePayment",
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
        }
    } catch (error) {

        yield put(actions.locationHideLoader());
        if (error == "Error: Request failed with status code 405") {
            yield put(push("/signin"));
            yield put(actions.userSignOutSuccess());
            yield localStorage.clear();
            yield toast.error("Please Login Again", {
                toastId: "offlinePayment",
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
        }
    }
}
export default function* rootSaga() {
    yield takeEvery(actionTypes.DEVICE_GET_ALL_DEVICE_REQUEST, deviceGetAllDeviceRequest);
    yield takeEvery(actionTypes.DEVICE_DELETE_DEVICE, deleteDevice);
    yield takeEvery(actionTypes.DEVICE_DEVICE_STATUS_CHANGED, deviceDeviceStatusChanged);
    yield takeEvery(actionTypes.DEVICE_DEVICE_ADD_REQUEST, deviceDeviceAddRequest);
    yield takeEvery(actionTypes.DEVICE_GET_DEVICE_REQUEST, deviceGetDeviceRequest);
    yield takeEvery(actionTypes.DEVICE_GET_DEVICE_STATISTICS, deviceGetDeviceStatistics);
    yield takeEvery(actionTypes.DEVICE_GET_ALL_LOCATION_REQUEST, deviceGetAllLocations);
}