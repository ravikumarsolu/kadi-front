import { all, call, fork, put, takeEvery } from "redux-saga/effects";
import { push } from 'react-router-redux';
import axios from '../services';
import * as actionTypes from "../constants/ActionTypes";
import * as actions from '../actions';
import { toast } from "react-toastify";
import moment from 'moment';
function* locationGetAllLocations({ payload }) {
    try {
        let response = yield axios.get("/location?skip=0&limit=100");
        if (response.status == 200) {
            yield put(actions.locationGetAllLocationsSuccess(response.data.data));
            yield put(actions.locationHideLoader());
        } else {
            yield put(actions.locationHideLoader());
            yield toast.error("Something went wrong", {
                toastId: "offlinePayment",
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
        }
    } catch (error) {

        yield put(actions.locationHideLoader());
        if (error == "Error: Request failed with status code 405") {
            yield put(push("/signin"));
            yield put(actions.userSignOutSuccess());
            yield localStorage.clear();
            yield toast.error("Please Login Again", {
                toastId: "offlinePayment",
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
        }
    }
}

function* deleteLocation({ payload }) {
    try {
        yield put(actions.locationShowLoader());
        let response = yield axios.delete(`/location/${payload}`);
        if (response.status == 200) {
            yield put(actions.locationGetAllLocations());
            yield toast.success("Location deleted", {
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
        } else {
            yield put(actions.locationHideLoader());
            yield toast.error("Something went wrong", {
                toastId: "offlinePayment",
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
        }
    } catch (error) {

        yield put(actions.locationHideLoader());
        if (error == "Error: Request failed with status code 405") {
            yield put(push("/signin"));
            yield put(actions.userSignOutSuccess());
            yield localStorage.clear();
            yield toast.error("Please Login Again", {
                toastId: "offlinePayment",
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
        }
    }
}
function* locationAddLocation({ payload }) {
    const { isEdit, locationId } = payload;
    try {
        yield put(actions.locationShowLoader());
        if (isEdit) {

            console.log("TCL: function*locationAddLocation -> payload", payload)
            let response = yield axios.put(`/location/${locationId}`,payload);
            if (response.status == 200) {
                // yield put(staffHideLoader());
                yield put(actions.locationClear());
                yield put(actions.locationGetAllLocations());
                yield toast.success("Location Updated", {
                    toastId: "offlinePayment",
                    hideProgressBar: true,
                    autoClose: 3000,
                    position: toast.POSITION.TOP_RIGHT
                });
            } else {
                yield toast.error("Location Not Updated", {
                    toastId: "offlinePayment",
                    hideProgressBar: true,
                    autoClose: 3000,
                    position: toast.POSITION.TOP_RIGHT
                });
            }
        } else {
            let response = yield axios.post("/location", payload);
            if (response.status == 200) {
                yield put(actions.locationClear());
                yield put(actions.locationGetAllLocations());
                yield toast.success("Location Added", {
                    toastId: "offlinePayment",
                    hideProgressBar: true,
                    autoClose: 3000,
                    position: toast.POSITION.TOP_RIGHT
                });
            } else {
                yield toast.error("Location Not Added", {
                    toastId: "offlinePayment",
                    hideProgressBar: true,
                    autoClose: 3000,
                    position: toast.POSITION.TOP_RIGHT
                });
            }
        }
    } catch (error) {
        yield put(actions.locationHideLoader());
        if (error == "Error: Request failed with status code 405") {
            yield put(push("/signin"));
            yield put(actions.userSignOutSuccess());
            yield localStorage.clear();
            yield toast.error("Please Login Again", {
                toastId: "offlinePayment",
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
        }
    }
}


function* locationGetLocation({ payload }) {
    try {
        yield put(actions.locationShowLoader());
        let response = yield axios.get(`/location/${payload}`);
        if (response.status == 200) {
            // console.log("TCL: function*locationGetLocation -> response", response)
            yield put(actions.locationGetLocationSuccess(response.data.data[0]));
        } else {
            yield put(actions.locationHideLoader());
            yield toast.error("Something went wrong", {
                toastId: "offlinePayment",
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
        }
    } catch (error) {

        yield put(actions.locationHideLoader());
        if (error == "Error: Request failed with status code 405") {
            yield put(push("/signin"));
            yield put(actions.userSignOutSuccess());
            yield localStorage.clear();
            yield toast.error("Please Login Again", {
                toastId: "offlinePayment",
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
        }
    }
}

export default function* rootSaga() {
    yield takeEvery(actionTypes.LOCATION_GET_ALL_LOCATIONS, locationGetAllLocations);
    yield takeEvery(actionTypes.LOCATION_DELETE_LOCATION, deleteLocation);
    yield takeEvery(actionTypes.LOCATION_ADD_LOCATION, locationAddLocation);
    yield takeEvery(actionTypes.LOCATION_GET_LOCATION, locationGetLocation)
}