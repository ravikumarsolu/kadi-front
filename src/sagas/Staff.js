import { all, call, fork, put, takeEvery } from "redux-saga/effects";
import axios from '../services';
import { push } from 'react-router-redux';
import * as actionTypes from "../constants/ActionTypes";
import * as actions from '../actions';
import { toast } from "react-toastify";

// Add Staff API integration
function* staffUserAddRequest({ payload }) {
    const { isEdit, userId } = payload;
    try {
        if (isEdit) {
            let response = yield axios.put(`/customer/${userId}`, payload);
            if (response.status == 200) {
                // yield put(staffHideLoader());
                yield put(actions.staffUserClear());
                yield put(actions.staffUserGetAllUnallocatedDevices());
                yield put(actions.staffUserGetAllStaffUsersRequest());
                yield toast.success("Staff Updated", {
                    toastId: "offlinePayment",
                    hideProgressBar: true,
                    autoClose: 3000,
                    position: toast.POSITION.TOP_RIGHT
                });
            } else {
                yield toast.error("Staff Not Updated", {
                    toastId: "offlinePayment",
                    hideProgressBar: true,
                    autoClose: 3000,
                    position: toast.POSITION.TOP_RIGHT
                });
            }
        } else {
            let response = yield axios.post("/customer/", payload);
            if (response.status == 200) {
                // yield put(staffHideLoader());
                yield put(actions.staffUserClear());
                yield put(actions.staffUserGetAllUnallocatedDevices());
                yield put(actions.staffUserGetAllStaffUsersRequest());
                yield toast.success("Staff Added", {
                    toastId: "offlinePayment",
                    hideProgressBar: true,
                    autoClose: 3000,
                    position: toast.POSITION.TOP_RIGHT
                });
            } else {
                yield toast.error("Staff Not Added", {
                    toastId: "offlinePayment",
                    hideProgressBar: true,
                    autoClose: 3000,
                    position: toast.POSITION.TOP_RIGHT
                });
            }
        }
    } catch (error) {
        console.log("TCL: function*staffUserAddRequest -> error", error)
        yield put(actions.staffHideLoader());
        if (error == "Error: Request failed with status code 405") {
            yield put(push("/signin"));
            yield put(actions.userSignOutSuccess());
            yield localStorage.clear();
            yield toast.error("Please Login Again", {
                toastId: "offlinePayment",
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
        }
    }
}

//Staff Listing API integration
function* staffUserGetAllStaffUsersRequest({ action }) {
    try {
        let response = yield axios.get("/customer?skip=0&limit=10")

        if (response.status == 200) {
            yield put(actions.staffUserGetAllStaffUsersRequestSuccess(response.data.data));
            yield put(actions.staffHideLoader());
        } else {
            yield toast.error("Errors in Users", {
                toastId: "offlinePayment",
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
        }
    } catch (error) {
        yield put(actions.staffHideLoader());
        if (error == "Error: Request failed with status code 405") {
            yield put(push("/signin"));
            yield put(actions.userSignOutSuccess());
            yield localStorage.clear();
            yield toast.error("Please Login Again", {
                toastId: "offlinePayment",
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
        }
    }
}

//Staff User details API integration
function* staffUserGetStaffUsersRequest({ payload }) {
    try {
        let response = yield axios.get(`/customer/${payload}`)
        if (response.status == 200) {
            yield put(actions.staffUserGetStaffUsersRequestSuccess(response.data.data));
        } else {
            yield toast.success("Something went wrong", {
                toastId: "offlinePayment",
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
        }
    } catch (error) {
        yield put(actions.staffHideLoader());
        if (error == "Error: Request failed with status code 405") {
            yield put(push("/signin"));
            yield put(actions.userSignOutSuccess());
            yield localStorage.clear();
            yield toast.error("Please Login Again", {
                toastId: "offlinePayment",
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
        }
    }
}

//Staff Delete API integration
function* deleteStaffUser({ payload }) {
    try {
        yield put(actions.staffShowLoader());
        let response = yield axios.delete(`/customer/${payload}`);
        if (response.status == 200) {
            yield put(actions.staffUserGetAllStaffUsersRequest());
            yield toast.success("Staff Deleted", {
                toastId: "offlinePayment",
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });

        } else {
            yield put(actions.staffHideLoader());
            yield toast.error("Error in delete staff", {
                toastId: "offlinePayment",
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
        }
    } catch (error) {
        yield put(actions.staffHideLoader());
        if (error == "Error: Request failed with status code 405") {
            yield put(push("/signin"));
            yield put(actions.userSignOutSuccess());
            yield localStorage.clear();
            yield toast.error("Please Login Again", {
                toastId: "offlinePayment",
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
        }
    }
}

//Staff User Status change API integration
function* staffUserStatusChanged({ payload }) {
    const { id, status, index } = payload
    try {
        let response = yield axios.put(`/customer/${id}`, {
            status: status
        });
        console.log("TCL: function*staffUserStatusChanged -> response", response)

        if (response.status == 200) {
            yield put(actions.staffUserStatusChangedSuccess(index));
            yield toast.success("Staff Status Changed", {
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
        } else {
            yield toast.error("Error in Status Change", {
                toastId: "offlinePayment",
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
        }
    } catch (error) {
        yield put(actions.staffHideLoader());
        if (error == "Error: Request failed with status code 405") {
            yield put(push("/signin"));
            yield put(actions.userSignOutSuccess());
            yield localStorage.clear();
            yield toast.error("Please Login Again", {
                toastId: "offlinePayment",
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
        }
    }
}
// function* staffUserGetUserDevices({ payload }) {
//     const userId= localStorage.getItem("user_id");
//     try {
//         let response = yield axios.get(`device/user/${userId}`);

//         if (response.status == 200) {
//             console.log("status Changed");
//             yield put(actions.staffUserGetUserDevicesSuccess(response.data.data));
//         } else {
//             console.log("NOt");
//         }
//     } catch (error) {

//         // yield put(showAuthMessage(error.message));
//     }
// }

//All Unallocated Devices of admin API integration
function* staffUserGetAllUnallocatedDevices(action) {
    try {
        let response = yield axios.get("/device/unallocated");

        if (response.status == 200) {
            yield put(actions.staffUserGetAllUnallocatedDevicesSuccess(response.data.data));
        }
    } catch (error) {
        if (error == "Error: Request failed with status code 405") {
            yield put(push("/signin"));
            yield put(actions.userSignOutSuccess());
            yield localStorage.clear();
            yield toast.error("Please Login Again", {
                toastId: "offlinePayment",
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
        }
    }
}
export default function* rootSaga() {
    yield takeEvery(actionTypes.STAFF_USER_ADD_REQUEST, staffUserAddRequest);
    yield takeEvery(actionTypes.STAFF_USER_GET_ALL_STAFF_USERS_REQUEST, staffUserGetAllStaffUsersRequest);
    yield takeEvery(actionTypes.STAFF_USER_GET_STAFF_USERS_REQUEST, staffUserGetStaffUsersRequest);
    yield takeEvery(actionTypes.DELETE_STAFF_USER, deleteStaffUser);
    yield takeEvery(actionTypes.STAFF_USER_STATUS_CHANGED, staffUserStatusChanged);
    yield takeEvery(actionTypes.STAFF_USER_GET_ALL_UNALLOCATED_DEVICES, staffUserGetAllUnallocatedDevices);
    // yield takeEvery(actionTypes.STAFF_USER_GET_USER_DEVICES,staffUserGetUserDevices);
}