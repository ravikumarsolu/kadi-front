import { all, call, fork, put, takeEvery } from "redux-saga/effects";
import { push } from 'react-router-redux';
import axios from '../services';
import * as actionTypes from "../constants/ActionTypes";
import * as actions from '../actions';
import { toast } from "react-toastify";
import moment from 'moment';
function* dashboardGetAllDevices({ payload }) {
    try {
        yield put(actions.dashboardShowLoader());
        let response = yield axios.get("/device?skip=0&limit=100");
        if (response.status == 200) {
            yield put(actions.dashboardGetAllDevicesSuccess(response.data.data));
            // yield put(actions.dashboardDashboardDeviceChanged({ id: response.data.data[0].deviceIMEI, trend: "current" }));
            // yield put(actions.dashboardDashboardDeviceChanged({ id: response.data.data[0].deviceIMEI, trend: "daily" }));
            // yield put(actions.dashboardDashboardDeviceChanged({ id: response.data.data[0].deviceIMEI, trend: "weekly" }));
            // yield put(actions.dashboardDashboardDeviceChanged({ id: response.data.data[0].deviceIMEI, trend: "monthly" }));
            // yield put(actions.dashboardDashboardDeviceChanged({ id: response.data.data[0].deviceIMEI, trend: "quarterly" }));
            // yield put(actions.dashboardDashboardDeviceChanged({ id: response.data.data[0].deviceIMEI, trend: "yearly" }));
            yield put(actions.deviceHideLoader());
       
        } else {
            yield put(actions.deviceHideLoader());
            yield toast.error("Something went wrong", {
                toastId: "offlinePayment",
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
        }
    } catch (error) {

        yield put(actions.dashboardHideLoader());
        if (error == "Error: Request failed with status code 405") {
            yield put(push("/signin"));
            yield put(actions.userSignOutSuccess());
            yield localStorage.clear();
            yield toast.error("Please Login Again", {
                toastId: "offlinePayment",
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
        }
    }
}
function* dashboardDashboardDeviceChanged({ payload }) {
    const { id, trend } = payload;

    try {
        let response = yield axios.get(`aws/?id=${id}&trend=${trend}`);
        console.log("TCL: function*dashboardDashboardDeviceChanged -> response", response)
        if (response.status == 200) {
            if (trend === "current") {
                yield put(actions.dashboardDashboardDeviceChangedSuccess({ data: response.data.data.data.Items[0], trend: trend }));
            } else if (trend === "daily") {
                yield put(actions.dashboardDashboardDeviceChangedSuccess({ data: response.data.data, trend: trend }));
            } else if (trend === "weekly") {
                yield put(actions.dashboardDashboardDeviceChangedSuccess({ data: response.data.data, trend: trend }));
            } else if (trend === "monthly") {
                yield put(actions.dashboardDashboardDeviceChangedSuccess({ data: response.data.data, trend: trend }));
            } else if (trend === "yearly") {
                yield put(actions.dashboardDashboardDeviceChangedSuccess({ data: response.data.data, trend: trend }));
            } else if (trend === "quarterly") {
                yield put(actions.dashboardDashboardDeviceChangedSuccess({ data: response.data.data, trend: trend }));
            } else {
                yield put(actions.dashboardDashboardDeviceChangedSuccess({ data: response.data.data, trend: trend }));

            }
        } else {
            yield put(actions.dashboardHideLoader());
            yield toast.error("Something went wrong", {
                toastId: "offlinePayment",
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
        }
    } catch (error) {
        yield put(actions.deviceHideLoader());
        if (error == "Error: Request failed with status code 405") {
            yield put(push("/signin"));
            yield put(actions.userSignOutSuccess());
            yield localStorage.clear();
            yield toast.error("Please Login Again", {
                toastId: "offlinePayment",
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
        }
    }
}


export default function* rootSaga() {
    yield takeEvery(actionTypes.DASHBOARD_GET_ALL_DEVICES, dashboardGetAllDevices);
    yield takeEvery(actionTypes.DASHBOARD_DASHBOARD_DEVICE_CHANGED, dashboardDashboardDeviceChanged);
}