import {all} from 'redux-saga/effects';
import authSagas from './Auth';
import staffSagas from './Staff';
import devicesSagas from './Devices';
import dashboardSagas from './Dashboard';
import locationSagas from './Location';
import subscriptionSagas from './Subscription';
import accountSagas from './Account';
import billingInformation from './BillingInformation';
export default function* rootSaga(getState) {
    yield all([
        authSagas(),
        staffSagas(),
        devicesSagas(),
        dashboardSagas(),
        locationSagas(),
        subscriptionSagas(),
        accountSagas(),
        billingInformation()
    ]);
}
