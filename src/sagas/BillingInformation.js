import { all, call, fork, put, takeEvery } from "redux-saga/effects";
import { push } from 'react-router-redux';
import axios from '../services';
import * as actionTypes from "../constants/ActionTypes";
import * as actions from '../actions';
import { toast } from "react-toastify";
import moment from 'moment';

function* billingGetAllPaymentHistory({ payload }) {
    const { type, month, year } = payload;
    try {
        yield put(actions.billingShowLoader());
        let response;
        if (type == "all") {
            response = yield axios.get("/billing");
        }
        else if (type == "custom") {
            response = yield axios.get(`/billing/time/`, { month: moment(month).format("MMMM"), year })
        }
        if (response.status == 200) {
            yield put(actions.billingGetAllPaymentHistorySuccess(response.data.data));
            yield put(actions.billingHideLoader());
        } else {
            yield put(actions.billingHideLoader());
            yield toast.error("Something went wrong", {
                toastId: "offlinePayment",
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
        }
    } catch (error) {
        yield put(actions.billingHideLoader());
        if (error == "Error: Request failed with status code 405") {
            yield put(push("/signin"));
            yield put(actions.userSignOutSuccess());
            yield localStorage.clear();
            yield toast.error("Please Login Again", {
                toastId: "offlinePayment",
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
        }
    }
}

function* billingGetInvoiceDetails({ payload }) {
    try {
        yield put(actions.billingShowLoader());
        let response = yield axios.get(`/billing/${payload}`);
        if (response.status == 200) {
            yield put(actions.billingGetInvoiceDetailsSuccess(response.data.data));
            yield put(actions.billingHideLoader());
        } else {
            yield put(actions.billingHideLoader());
            yield toast.error("Something went wrong", {
                toastId: "offlinePayment",
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
        }
    } catch (error) {
        yield put(actions.billingHideLoader());
        if (error == "Error: Request failed with status code 405") {
            yield put(push("/signin"));
            yield put(actions.userSignOutSuccess());
            yield localStorage.clear();
            yield toast.error("Please Login Again", {
                toastId: "offlinePayment",
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
        }
    }
}

function* billingPayBill({ payload }) {
    try {
        const { partyId, billingId, amount } = payload;
        yield put(actions.billingShowLoader());
        let response = yield axios.post(`/billing/pay/${billingId}`, { partyId, amount });
        console.log("TCL: function*billingPayBill -> response", response)
        if (response.status == 200) {
            yield put(actions.billingHideLoader());
            yield toast.success(response.data.message, {
                toastId: "offlinePayment",
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
        } else {
            yield put(actions.billingHideLoader());
            yield toast.error("Something went wrong", {
                toastId: "offlinePayment",
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
        }
    } catch (error) {
        yield put(actions.billingHideLoader());
        if (error == "Error: Request failed with status code 405") {
            yield put(push("/signin"));
            yield put(actions.userSignOutSuccess());
            yield localStorage.clear();
            yield toast.error("Please Login Again", {
                toastId: "offlinePayment",
                hideProgressBar: true,
                autoClose: 3000,
                position: toast.POSITION.TOP_RIGHT
            });
        }
    }
}


export default function* rootSaga() {
    yield takeEvery(actionTypes.BILLING_GET_ALL_PAYMENT_HISTORY, billingGetAllPaymentHistory);
    yield takeEvery(actionTypes.BILLING_GET_INVOICE_DETAILS, billingGetInvoiceDetails);
    yield takeEvery(actionTypes.BILLING_PAY_BILL, billingPayBill);
}