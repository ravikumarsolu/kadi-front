import React from 'react';
import {ConnectedRouter} from 'connected-react-router'
import {Provider} from 'react-redux';
import {Route, Switch} from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';
import configureStore, {history} from './store';
import './firebase/firebase';
import App from './containers/App';
import 'react-toastify/dist/ReactToastify.css';
export const store = configureStore();
toast.configure()
const MainApp = () =>
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <Switch>
        <Route path="/" component={App}/>
      </Switch>
    </ConnectedRouter>
  </Provider>;


export default MainApp;