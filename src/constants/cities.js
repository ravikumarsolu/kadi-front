const data=[{
    "id": 83,
    "name": "Ghana",
    "iso3": "GHA",
    "iso2": "GH",
    "phone_code": "233",
    "capital": "Accra",
    "currency": "GHS",
    "states": {
        "Ashanti": [
            "Agogo",
            "Bekwai",
            "Konongo",
            "Kumasi",
            "Mampong",
            "Mankranso",
            "Obuasi",
            "Ofinso",
            "Tafo"
        ],
        "Brong-Ahafo": [
            "Bechem",
            "Berekum",
            "Duayaw Nkwanta",
            "Kintampo",
            "Sunyani",
            "Techiman",
            "Wenchi"
        ],
        "Central": [
            "Apam",
            "Cape Coast",
            "Dunkwa",
            "Elmina",
            "Foso",
            "Komenda",
            "Mauri",
            "Mumford",
            "Nyakrom",
            "Okitsiu",
            "Saltpond",
            "Swedru",
            "Winneba"
        ],
        "Eastern": [
            "Aburi",
            "Ada",
            "Akim Swedru",
            "Akropong",
            "Asamankese",
            "Begoro",
            "Kade",
            "Kibi",
            "Koforidua",
            "Mpraeso",
            "Nkawkaw",
            "Nsawam",
            "Oda",
            "Somanya",
            "Suhum"
        ],
        "Greater Accra": [
            "Aburi",
            "Ada",
            "Akim Swedru",
            "Akropong",
            "Asamankese",
            "Begoro",
            "Kade",
            "Kibi",
            "Koforidua",
            "Mpraeso",
            "Nkawkaw",
            "Nsawam",
            "Oda",
            "Somanya",
            "Suhum"
        ],
        "Northern": [
            "Kpandae",
            "Salaga",
            "Savelugu",
            "Tamale",
            "Yendi"
        ],
        "Upper East": [
            "Kpandae",
            "Salaga",
            "Savelugu",
            "Tamale",
            "Yendi"
        ],
        "Upper West": [
            "Kpandae",
            "Salaga",
            "Savelugu",
            "Tamale",
            "Yendi"
        ],
        "Volta": [
            "Aflao",
            "Anloga",
            "Ho",
            "Hohoe",
            "Keta",
            "Kete-Krachi",
            "Kpandu"
        ],
        "Western": [
            "Aboso",
            "Anomabu",
            "Axim",
            "Bibiani",
            "Prestea",
            "Sekondi",
            "Shama",
            "Takoradi",
            "Tarkwa"
        ]
    }
}]

export default data;
