import axios from "axios";
const baseURL = "http://34.240.27.119:3000/api/v1";
// const baseURL = "http://34.244.214.9:4000/api/v1";
// const baseURL = "http://34.240.16.226:4000/api/v1";



// const baseURL = "http://52.215.204.109:3000/api/v1";
// const baseURL="http://192.168.10.99:3000/api/v1";
const token = localStorage.getItem("token");

const user = axios.create({
  baseURL: baseURL,
  headers: {
    "Content-Type": "application/json"
  },
});

export default user;
