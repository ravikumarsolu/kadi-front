import React from 'react';
import { Link } from 'react-router-dom'
import { connect } from 'react-redux';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import IntlMessages from 'util/IntlMessages';
import CircularProgress from '@material-ui/core/CircularProgress';
import { toast } from "react-toastify";
import {
  hideMessage,
  showAuthLoader,
  showAuthMessage,
  userFacebookSignIn,
  userGithubSignIn,
  userGoogleSignIn,
  userSignIn,
  userTwitterSignIn,
  statusClean
} from 'actions/Auth';

class SignIn extends React.Component {
  constructor() {
    super();
    this.state = {
      email: '',
      password: ''
    }
  }

  componentDidUpdate() {
    if (this.props.showMessage) {
      setTimeout(() => {
        this.props.hideMessage();
      }, 100);
    }
    if (this.props.authUser !== null) {
      this.props.history.push('/');
    }
  }
  keyPressed(event,value) {
    if (event.key === "Enter") {
      console.log("ENTER",value);
      
    }
  }
  componentWillUnmount(){
    this.props.statusClean();
  }
  render() {
    const { email, password } = this.state;
    const { showMessage, loader, alertMessage,  forgetPasswordSuccess, resetPasswordSuccess, createPasswordSuccess } = this.props;
    if( forgetPasswordSuccess || resetPasswordSuccess || createPasswordSuccess){
      this.props.statusClean();
    }
    return (
      <div
        className="app-login-container d-flex justify-content-center align-items-center animated slideInUpTiny animation-duration-3">
        <div className="app-login-main-content">

          <div className="app-login-content">

            <img className="text-center mb-4" src={require("assets/images/logo.svg")} alt="Kadi Energy Company" title="Kadi Energy Company" height="40" width="100%" />

            <div className="app-login-header mb-4 text-center">
              <h1 className="text-white">Login into account</h1>
              <p className="text-white opacity-08">Use your credentials to access your account.</p>
            </div>

            <div className="app-login-form">
              <form>
                <fieldset>
                  <div className="formgroup">
                    <img src={require('../assets/images/mail.png')} />
                    <TextField
                      fullWidth
                      placeholder="Email Address"
                      onChange={(event) => this.setState({ email: event.target.value })}
                      defaultValue={email}
                      margin="normal"
                      className="mt-0 my-0 custominput"
                      onKeyPress={this.keyPressed}
                    />
                  </div>
                  <div className="formgroup">
                    <img src={require('../assets/images/password.png')} />
                    <TextField
                      type="password"
                      placeholder="Password"
                      fullWidth
                      onChange={(event) => this.setState({ password: event.target.value })}
                      defaultValue={password}
                      margin="normal"
                      className="mt-0 my-0 custominput"
                      onKeyPress={this.keyPressed}
                    />
                  </div>

                  <div className="mb-3 d-flex justify-content-end">
                    <Link to="/forgotpassword" className="col-primary">
                      Forgot Password?
                    </Link>
                  </div>

                  <Button className="w-100 customButton" 
                  onKeyPress={this.keyPressed}
                  onClick={() => {
                    // Email Passowrd Validation 
                    if (email.trim() == '') {
                      toast.error("Email cannot be blank", {
                        toastId: "offlinePayment",
                        hideProgressBar: true,
                        autoClose: 3000,
                        position: toast.POSITION.TOP_RIGHT
                      });
                      this.props.showAuthMessage('Email cannot be blank');
                    } else if (!/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$/.test(email)) {
                      toast.error("Invalid Email", {
                        toastId: "offlinePayment",
                        hideProgressBar: true,
                        autoClose: 3000,
                        position: toast.POSITION.TOP_RIGHT
                      });
                      this.props.showAuthMessage('Invalid Email');
                    } else if (password == '') {
                      toast.error("Password cannot be blank", {
                        toastId: "offlinePayment",
                        hideProgressBar: true,
                        autoClose: 3000,
                        position: toast.POSITION.TOP_RIGHT
                      });
                      this.props.showAuthMessage('Password cannot be blank');
                    } else if (password.length < 8) {
                      toast.error("Password must be atleast 8 characters long", {
                        toastId: "offlinePayment",
                        hideProgressBar: true,
                        autoClose: 3000,
                        position: toast.POSITION.TOP_RIGHT
                      });
                      this.props.showAuthMessage('Password must be atleast 8 characters long');
                    } else {
                      this.props.showAuthLoader();
                      this.props.userSignIn({ email, password });
                    }
                  }} variant="contained">
                    Login
                  </Button>
                </fieldset>
              </form>
            </div>
          </div>

        </div>
        {
          loader &&
          <div className="loader-view">
            <CircularProgress />
          </div>
        }
        {showMessage && NotificationManager.error(alertMessage)}
        <NotificationContainer />
      </div>
    );
  }
}

const mapStateToProps = ({ auth }) => {
  const { loader, alertMessage, showMessage, authUser, forgetPasswordSuccess, resetPasswordSuccess, createPasswordSuccess } = auth;
  return { loader, alertMessage, showMessage, authUser,  forgetPasswordSuccess, resetPasswordSuccess, createPasswordSuccess }
};

export default connect(mapStateToProps, {
  userSignIn,
  hideMessage,
  showAuthLoader,
  userFacebookSignIn,
  userGoogleSignIn,
  userGithubSignIn,
  userTwitterSignIn,
  showAuthMessage,
  statusClean
})(SignIn);
