import React from 'react';
import { connect } from 'react-redux';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import CircularProgress from '@material-ui/core/CircularProgress';
import { Link } from 'react-router-dom';
import IntlMessages from 'util/IntlMessages';
import {
  hideMessage,
  hideMessageSuccess,
  showAuthLoader,
  userFacebookSignIn,
  userGithubSignIn,
  showAuthMessage,
  userGoogleSignIn,
  userSignUp,
  userTwitterSignIn,
  forgetPassword,
  emailChanged
} from 'actions/Auth';

class SignUp extends React.Component {
  constructor() {
    super();
    this.state = { email: '' }
  }

  componentDidUpdate() {
    if (this.props.authUser !== null) {
      this.props.history.push('/');
    }
    // else if (this.props.showMessage) {
    //   setTimeout(() => {
    //     this.props.hideMessage();
    //   }, 3000);
    // }
    // else if (this.props.showMessageSuccess) {
    //   setTimeout(() => {
    //     this.props.hideMessageSuccess();
    //   }, 3000);
    // }
   
  }
  
  render() {
    const {
      email
    } = this.state;
    const { showMessage, showMessageSuccess, loader, alertMessage, alertMessageSuccess, forgetPasswordSuccess } = this.props;
    if (forgetPasswordSuccess == true) {
      this.props.history.push('/reset-password')
    }
    return (
      <div
        className="app-login-container d-flex justify-content-center align-items-center animated slideInUpTiny animation-duration-3">
        <div className="app-login-main-content">
          <div className="app-login-content">
            <img className="text-center mb-4" src={require("assets/images/logo.svg")} alt="Kadi Energy Company" title="Kadi Energy Company" height="40" width="100%" />
            <div className="app-login-header mb-4 text-center">
              <h1 className="text-white">Forgot Password</h1>
              <p className="text-white opacity-08">We will send you a verification code on your registered email ID.</p>
            </div>
            <div className="app-login-form">
                <div className="formgroup">
                  <img src={require('../assets/images/mail.png')} />
                  <TextField
                    fullWidth
                    placeholder="Email Address"
                    onChange={(event) => {
                      this.setState({ email: event.target.value })
                      this.props.emailChanged(event.target.value)
                    }}
                    defaultValue={email}
                    margin="normal"
                    className="mt-0 my-0 custominput"
                  />
                </div>
                <Button className="w-100 customButton" onClick={() => {
                  if (email.trim() == '') {
                    this.props.showAuthMessage('Email cannot be blank');
                  } else if (!/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$/.test(email)) {
                    this.props.showAuthMessage('Invalid Email');
                  }
                  else {
                    this.props.showAuthLoader();
                    this.props.forgetPassword({ email: email, resend: false });
                  }
                }} variant="contained">
                  Submit
                </Button>
            </div>
          </div>

        </div>
        {
          loader &&
          <div className="loader-view">
            <CircularProgress />
          </div>
        }
        {showMessage && NotificationManager.error(alertMessage)}
        {showMessageSuccess && NotificationManager.success(alertMessageSuccess)}

        <NotificationContainer />
      </div>
    )
  }
}

const mapStateToProps = ({ auth }) => {
  const { loader, alertMessage, showMessage, showMessageSuccess, authUser, forgetPasswordSuccess, alertMessageSuccess, email } = auth;
  return { loader, alertMessage, showMessage, showMessageSuccess, authUser, forgetPasswordSuccess, alertMessageSuccess, email }
};

export default connect(mapStateToProps, {
  userSignUp,
  hideMessage,
  hideMessageSuccess,
  showAuthLoader,
  showAuthMessage,
  userFacebookSignIn,
  userGoogleSignIn,
  userGithubSignIn,
  userTwitterSignIn,
  forgetPassword,
  emailChanged
})(SignUp);
