import React, { Component } from 'react';
import { Link, NavLink, withRouter } from 'react-router-dom';
import CustomScrollbars from 'util/CustomScrollbars';
import { connect } from 'react-redux';
import * as actions from '../../actions';

class SidenavContent extends Component {
  componentDidMount() {
    const { history } = this.props;
    const that = this;
    const pathname = `${history.location.pathname}`;// get current path

    const menuLi = document.getElementsByClassName('menu');
    for (let i = 0; i < menuLi.length; i++) {
      menuLi[i].onclick = function (event) {
        for (let j = 0; j < menuLi.length; j++) {
          const parentLi = that.closest(this, 'li');
          if (menuLi[j] !== this && (parentLi === null || !parentLi.classList.contains('open'))) {
            menuLi[j].classList.remove('open')
          }
        }
        this.classList.toggle('open');
      }
    }

    const activeLi = document.querySelector('a[href="' + pathname + '"]');// select current a element
    try {
      const activeNav = this.closest(activeLi, 'ul'); // select closest ul
      if (activeNav.classList.contains('sub-menu')) {
        this.closest(activeNav, 'li').classList.add('open');
      } else {
        this.closest(activeLi, 'li').classList.add('open');
      }
    } catch (error) {

    }
  }

  componentWillReceiveProps(nextProps) {
    const { history } = nextProps;
    const pathname = `${history.location.pathname}`;// get current path

    const activeLi = document.querySelector('a[href="' + pathname + '"]');// select current a element
    try {
      const activeNav = this.closest(activeLi, 'ul'); // select closest ul
      if (activeNav.classList.contains('sub-menu')) {
        this.closest(activeNav, 'li').classList.add('open');
      } else {
        this.closest(activeLi, 'li').classList.add('open');
      }
    } catch (error) {

    }
  }

  closest(el, selector) {
    try {
      let matchesFn;
      // find vendor prefix
      ['matches', 'webkitMatchesSelector', 'mozMatchesSelector', 'msMatchesSelector', 'oMatchesSelector'].some(function (fn) {
        if (typeof document.body[fn] == 'function') {
          matchesFn = fn;
          return true;
        }
        return false;
      });

      let parent;

      // traverse parents
      while (el) {
        parent = el.parentElement;
        if (parent && parent[matchesFn](selector)) {
          return parent;
        }
        el = parent;
      }
    } catch (e) {

    }

    return null;
  }

  render() {
    let userRole = localStorage.getItem("role");
    const pathname = `${this.props.history.location.pathname}`;
    return (

      <CustomScrollbars className=" scrollbar">

        <Link className="app-logo d-none d-xl-block" to="/">
          <img src={require("assets/images/logo.svg")} alt="Kadi" title="Kadi" />
        </Link>


        {/* <li>
            <ul className="d-none d-lg-block d-xl-block">
              
            </ul>
          </li> */}
        {userRole == "user" ?
          <ul className="nav-menu">
            <li className="menu no-arrow">
              <NavLink className="prepend-icon" to="/app/user/dashboard">
                <span className="nav-text">Dashboard</span>
              </NavLink>
            </li>
            <li className="menu no-arrow">
              <NavLink className="prepend-icon" to="/app/user/devices">
                <span className="nav-text">Devices</span>
              </NavLink>
            </li>
        
            <li className="menu no-arrow">
              <NavLink className="prepend-icon" to="/app/user/myaccount">
                <span className="nav-text">Account & Settings</span>
              </NavLink>
            </li>
            <li className="menu no-arrow">
              <NavLink className="prepend-icon" to="/signin" onClick={() => {
                this.props.userSignOutSuccess();
                localStorage.clear();
              }}>
                <span className="nav-text">Logout</span>
              </NavLink>
            </li>
          </ul>
          : null}
        {userRole == "admin" ?
          <ul className="nav-menu">
            <li className="menu no-arrow">
              <NavLink className="prepend-icon" to="/app/dashboard">
                <span className="nav-text">Dashboard</span>
              </NavLink>
            </li>
            <li className="menu no-arrow">
              <NavLink className="prepend-icon" to="/app/statistics">
                <span className="nav-text">Statistics</span>
              </NavLink>
            </li>
            <li className="menu no-arrow">
              <NavLink className="prepend-icon" to="/app/locations">
                <span className="nav-text">Locations</span>
              </NavLink>
            </li>
            <li className="menu no-arrow">
              <NavLink className="prepend-icon" to="/app/devices">
                <span className="nav-text">Devices</span>
              </NavLink>
            </li>
            <li className="menu no-arrow">
              <NavLink className="prepend-icon" to="/app/staffusers">
                <span className="nav-text">Staff Users</span>
              </NavLink>
            </li>
            <li className="menu no-arrow">
              <NavLink className="prepend-icon" to="/app/billinginfo">
                <span className="nav-text text-transform-none">Billing Information</span>
              </NavLink>
            </li>
            <li className="menu no-arrow">
              <NavLink className="prepend-icon" to="/app/subscription">
                <span className="nav-text">Subscriptions</span>
              </NavLink>
            </li>
            <li className="menu no-arrow">
              <NavLink className="prepend-icon" to="/app/control-devices">
                <span className="nav-text">Control Devices</span>
              </NavLink>
            </li>
            <li className="menu no-arrow">
              <NavLink className="prepend-icon" to="/app/myaccount">
                <span className="nav-text">Account & Settings</span>
              </NavLink>
            </li>
            <li className="menu no-arrow">
              <NavLink className="prepend-icon" to="/signin" onClick={() => {
                this.props.userSignOutSuccess();
                localStorage.clear();
              }}>
                <span className="nav-text">Logout</span>
              </NavLink>
            </li>
          </ul>

          : null}
      </CustomScrollbars>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  userSignOutSuccess: () => dispatch(actions.userSignOutSuccess()),

});
export default withRouter(connect(null, mapDispatchToProps)(SidenavContent));
