import React, { Component } from 'react';
import { Link, NavLink, withRouter } from 'react-router-dom';
import Button from '@material-ui/core/Button';

import IntlMessages from 'util/IntlMessages';
import CustomScrollbars from 'util/CustomScrollbars';


class SidenavContentMobile extends Component {
  componentDidMount() {
    const { history } = this.props;
    const that = this;
    const pathname = `${history.location.pathname}`;// get current path

    const menuLi = document.getElementsByClassName('menu');
    for (let i = 0; i < menuLi.length; i++) {
      menuLi[i].onclick = function (event) {
        for (let j = 0; j < menuLi.length; j++) {
          const parentLi = that.closest(this, 'li');
          if (menuLi[j] !== this && (parentLi === null || !parentLi.classList.contains('open'))) {
            menuLi[j].classList.remove('open')
          }
        }
        this.classList.toggle('open');
      }
    }

    const activeLi = document.querySelector('a[href="' + pathname + '"]');// select current a element
    try {
      const activeNav = this.closest(activeLi, 'ul'); // select closest ul
      if (activeNav.classList.contains('sub-menu')) {
        this.closest(activeNav, 'li').classList.add('open');
      } else {
        this.closest(activeLi, 'li').classList.add('open');
      }
    } catch (error) {

    }
  }

  componentWillReceiveProps(nextProps) {
    const { history } = nextProps;
    const pathname = `${history.location.pathname}`;// get current path

    const activeLi = document.querySelector('a[href="' + pathname + '"]');// select current a element
    try {
      const activeNav = this.closest(activeLi, 'ul'); // select closest ul
      if (activeNav.classList.contains('sub-menu')) {
        this.closest(activeNav, 'li').classList.add('open');
      } else {
        this.closest(activeLi, 'li').classList.add('open');
      }
    } catch (error) {

    }
  }

  closest(el, selector) {
    try {
      let matchesFn;
      // find vendor prefix
      ['matches', 'webkitMatchesSelector', 'mozMatchesSelector', 'msMatchesSelector', 'oMatchesSelector'].some(function (fn) {
        if (typeof document.body[fn] == 'function') {
          matchesFn = fn;
          return true;
        }
        return false;
      });

      let parent;

      // traverse parents
      while (el) {
        parent = el.parentElement;
        if (parent && parent[matchesFn](selector)) {
          return parent;
        }
        el = parent;
      }
    } catch (e) {

    }

    return null;
  }

  render() {
    let userRole = localStorage.getItem("role");

    return (

      <ul className="nav-menu-mobile">
        {userRole == "admin" ?
          <>
            <li className="menu no-arrow">
              <NavLink className="prepend-icon" to="/app/statistics">
                <img className="showdefault" src={require('../../assets/images/statistics-light.svg')} />
                <img className="showclick" src={require('../../assets/images/statistics-dark.svg')} />
                <span className="nav-text">Statistics</span>
              </NavLink>
            </li>
            <li className="menu no-arrow">
              <NavLink className="prepend-icon" to="/app/dashboard">
                <img className="showdefault" src={require('../../assets/images/dashboard-light.svg')} />
                <img className="showclick" src={require('../../assets/images/dashboard-dark.svg')} />
                <span className="nav-text">Dashboard</span>
              </NavLink>
            </li>
            <li className="menu no-arrow">
              <NavLink className="prepend-icon" to="/app/devices">
                <img className="showdefault" src={require('../../assets/images/devices-light.svg')} />
                <img className="showclick" src={require('../../assets/images/devices-dark.svg')} />
                <span className="nav-text">Devices</span>
              </NavLink>
            </li></>
          : userRole == "user" ? 
          <>
            <li className="menu no-arrow">
              <NavLink className="prepend-icon" to="/app/user/dashboard">
                <img className="showdefault" src={require('../../assets/images/dashboard-light.svg')} />
                <img className="showclick" src={require('../../assets/images/dashboard-dark.svg')} />
                <span className="nav-text">Dashboard</span>
              </NavLink>
            </li>
            <li className="menu no-arrow">
              <NavLink className="prepend-icon" to="/app/user/devices">
                <img className="showdefault" src={require('../../assets/images/devices-light.svg')} />
                <img className="showclick" src={require('../../assets/images/devices-dark.svg')} />
                <span className="nav-text">Devices</span>
              </NavLink>
            </li>
            <li className="menu no-arrow">
              <NavLink className="prepend-icon" to="/app/user/myaccount">
                <img className="showdefault" src={require('../../assets/images/statistics-light.svg')} />
                <img className="showclick" src={require('../../assets/images/statistics-dark.svg')} />
                <span className="nav-text">Account</span>
              </NavLink>
            </li>
          </> : null}
      </ul>
    );
  }
}

export default withRouter(SidenavContentMobile);
