import React from 'react';
import { connect } from 'react-redux';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import CircularProgress from '@material-ui/core/CircularProgress';
import { toast } from "react-toastify";

import {
  hideMessage,
  showAuthLoader,
  showAuthMessage,
  createPassword
} from 'actions/Auth';

class NewPassword extends React.Component {
  constructor() {
    super();
    this.state = {
      password: '',
      passwordShow: false,
      newPassword: '',
      newPasswordShow: false,
      otp: ''
    }
  }

  componentDidUpdate() {
    if (this.props.showMessage) {
      setTimeout(() => {
        this.props.hideMessage();
      }, 3000);
    }
    if (this.props.authUser !== null) {
      this.props.history.push('/app/dashboard');
      toast.error("Logout and try again", {
        toastId: "offlinePayment",
        hideProgressBar: true,
        autoClose: 3000,
        position: toast.POSITION.TOP_RIGHT
      });
    }
  }

  render() {
    const { newPassword, newPasswordShow, password, passwordShow, otp } = this.state;
    const { showMessage, loader, alertMessage, showMessageSuccess, createPasswordSuccess, authUser } = this.props;
    if (authUser !== null) {
      this.props.history.push('/app/dashboard')
    }
    else if (createPasswordSuccess == true) {
      this.props.history.push('/signin')
    }
    return (
      <div
        className="app-login-container d-flex justify-content-center align-items-center animated slideInUpTiny animation-duration-3">
        <div className="app-login-main-content">
          <div className="app-login-content">
            <img className="text-center mb-4" src={require("assets/images/logo.svg")} alt="Kadi Energy Company" title="Kadi Energy Company" height="40" width="100%" />
            <div className="app-login-header mb-4 text-center">
              <h1 className="text-white">Create Password</h1>
              <p className="text-white opacity-08">Enter your verification code below to create password.</p>
            </div>
            <div className="app-login-form resetpass">
              <div className="formgroup">
                <TextField
                  fullWidth
                  placeholder="Verification Code"
                  onChange={(event) => this.setState({ otp: event.target.value })}
                  defaultValue={onpointerup}
                  margin="normal"
                  className="mt-0 my-0 custominput"
                />
                {/* <a href="#" className="col-primary">Resend</a> */}
              </div>
              <div className="formgroup">
                <TextField
                  type={passwordShow ? "text" : "password"}
                  placeholder="New Password"
                  fullWidth
                  onChange={(event) => this.setState({ newPassword: event.target.value })}
                  defaultValue={password}
                  margin="normal"
                  className="mt-0 my-0 custominput"
                />
                <img style={{ cursor: "pointer" }} src={passwordShow ? require('../assets/images/eye-close.png') : require('../assets/images/eye.png')}
                  onClick={() => { this.setState({ passwordShow: !passwordShow }) }}
                />
              </div>
              <div className="formgroup">
                <TextField
                  type={newPasswordShow ? "text" : "password"}
                  placeholder="Confirm New Password"
                  fullWidth
                  onChange={(event) => this.setState({ password: event.target.value })}
                  defaultValue={password}
                  margin="normal"
                  className="mt-0 my-0 custominput"
                />
                <img style={{ cursor: "pointer" }} src={newPasswordShow ? require('../assets/images/eye-close.png') : require('../assets/images/eye.png')}
                  onClick={() => { this.setState({ newPasswordShow: !newPasswordShow }) }}

                />
              </div>
              <Button className="w-100 customButton" onClick={() => {
                if (otp == '') {
                  toast.error("Verification code cannot be blank", {
                    toastId: "offlinePayment",
                    hideProgressBar: true,
                    autoClose: 3000,
                    position: toast.POSITION.TOP_RIGHT
                  });
                } else if (password == '') {
                  toast.error("Password cannot be blank", {
                    toastId: "offlinePayment",
                    hideProgressBar: true,
                    autoClose: 3000,
                    position: toast.POSITION.TOP_RIGHT
                  });
                  this.props.showAuthMessage('Password cannot be blank');
                } else if (password.length < 8) {
                  toast.error("Password must be atleast 8 characters long", {
                    toastId: "offlinePayment",
                    hideProgressBar: true,
                    autoClose: 3000,
                    position: toast.POSITION.TOP_RIGHT
                  });
                  this.props.showAuthMessage('Password must be atleast 8 characters long');
                } else if (password != newPassword) {
                  toast.error("Password and Confirm password must be match", {
                    toastId: "offlinePayment",
                    hideProgressBar: true,
                    autoClose: 3000,
                    position: toast.POSITION.TOP_RIGHT
                  });
                  this.props.showAuthMessage('Password must be atleast 8 characters long');
                } else {
                  this.props.showAuthLoader();
                  this.props.createPassword({ otp, password });
                }
              }} variant="contained">
                Submit
                </Button>
            </div>
          </div>

        </div>

        {
          loader &&
          <div className="loader-view">
            <CircularProgress />
          </div>
        }
        {showMessage && NotificationManager.error(alertMessage)}
        <NotificationContainer />
      </div>
    )
  }
}

const mapStateToProps = ({ auth }) => {
  const { loader, alertMessage, showMessage, authUser, createPasswordSuccess, email } = auth;
  return { loader, alertMessage, showMessage, authUser, createPasswordSuccess, email }
};

export default connect(mapStateToProps, {
  hideMessage,
  showAuthLoader,
  showAuthMessage,
  createPassword
})(NewPassword);
