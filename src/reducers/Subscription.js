import {  SUBSCRIPTION_SHOW_LOADER, SUBSCRIPTION_HIDE_LOADER, SUBSCRIPTION_GET_USER_SUBSCRIPTION_PLANS, SUBSCRIPTION_GET_USER_SUBSCRIPTION_DETAILS, SUBSCRIPTION_GET_USER_SUBSCRIPTION_DETAILS_SUCCESS, SUBSCRIPTION_GET_USER_SUBSCRIPTION_PLANS_SUCCESS, SUBSCRIPTION_MOBILE_NUMBER_CHANGED } from "../constants/ActionTypes";

const INIT_STATE = {
    loader: false,
    buyLoader:false,
    subscriptionPlans:[],
    subscriptionDetails:[],
    mobileNumber:""
};


export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case SUBSCRIPTION_SHOW_LOADER: {
            if(action.payload == "buyLoader"){

                return {
                    ...state,
                    buyLoader: true
                }
            }else if(action.payload == "mainLoader"){
                return {
                    ...state,
                    loader: true
                }
            }
        }

        case SUBSCRIPTION_HIDE_LOADER: {

            if(action.payload == "buyLoader"){

                return {
                    ...state,
                    buyLoader: false
                }
            }else if(action.payload == "mainLoader"){
                return {
                    ...state,
                    loader: false
                }
            }
        }

        case SUBSCRIPTION_GET_USER_SUBSCRIPTION_PLANS_SUCCESS: {
            return{
                ...state,
                subscriptionPlans:action.payload ? action.payload : []
            }
        }
  
        
        case SUBSCRIPTION_GET_USER_SUBSCRIPTION_DETAILS_SUCCESS: {
            return{
                ...state,
                subscriptionDetails:action.payload ? action.payload : []
            }
        }

        case SUBSCRIPTION_MOBILE_NUMBER_CHANGED:{
            return{
                ...state,
                mobileNumber:action.payload
            }
        }
        

        default:
            return state;
    }
}
