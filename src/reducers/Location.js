import { LOCATION_LOCATION_ADDRESS_1_CHANGED, LOCATION_LOCATION_ADDRESS_2_CHANGED, LOCATION_GET_ALL_LOCATIONS_SUCCESS, LOCATION_SHOW_LOADER, LOCATION_HIDE_LOADER, LOCATION_LOCATION_STATE_CHANGED, LOCATION_LOCATION_CITY_CHANGED, LOCATION_LOCATION_EDIT_FILLUP, LOCATION_LOCATION_CLEAR, LOCATION_GET_LOCATION_SUCCESS } from "../constants/ActionTypes";

const INIT_STATE = {
    loader: false,
    address1: "",
    city: "",
    state: "",
    address2: "",
    allLocations: [],
    locationInformation:[],
    locationId:""
};


export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case LOCATION_LOCATION_ADDRESS_1_CHANGED: {
            return {
                ...state,
                address1: action.payload
            }
        }

        case LOCATION_LOCATION_ADDRESS_2_CHANGED: {
            return {
                ...state,
                address2: action.payload
            }
        }

        case LOCATION_LOCATION_STATE_CHANGED: {
            return {
                ...state,
                state: action.payload
            }
        }

        case LOCATION_LOCATION_CITY_CHANGED: {
            return {
                ...state,
                city: action.payload
            }
        }

        case LOCATION_GET_ALL_LOCATIONS_SUCCESS: {
            return {
                ...state,
                allLocations: action.payload.length > 0 ? action.payload : []
            }
        }
        case LOCATION_SHOW_LOADER: {
            return {
                ...state,
                loader: true
            }
        }
        case LOCATION_HIDE_LOADER: {
            return {
                ...state,
                loader: false
            }
        }
        case LOCATION_LOCATION_EDIT_FILLUP:{
            return{
                ...state,
                city:action.payload.city,
                state:action.payload.state,
                address1:action.payload.address,
                locationId:action.payload.locationId
            }
        }

        case LOCATION_LOCATION_CLEAR:{
            return{
                ...state,
                city:"",
                state:"",
                address1:"",
                locationId:""
            }
        }

        case LOCATION_GET_LOCATION_SUCCESS:{
            return{
                ...state,
                locationInformation:action.payload ? action.payload : [] 
            }
        }
        default:
            return state;
    }
}
