import { DEVICE_DEVICE_NAME_CHANGED, DEVICE_GET_DEVICE_STATISTICS_SUCCESS, DASHBOARD_DASHBOARD_DEVICE_CHANGED, DASHBOARD_GET_ALL_DEVICES_SUCCESS, DASHBOARD_DASHBOARD_DEVICE_CHANGED_SUCCESS } from "../constants/ActionTypes";

const INIT_STATE = {
    loader: false,
    allDevices: [],
    deviceInformation: [],
    dashboardDeviceCurrentStatus: [],
    dashboardDeviceDailyStatistics: [],
    dashboardDeviceWeeklyStatistics: [],
    dashboardDeviceMonthlyStatistics: [],
    dashboardDeviceYearlyStatistics: [],
    dashboardDeviceQuarterlyStatistics:[]
};


export default (state = INIT_STATE, action) => {
    switch (action.type) {

        case DASHBOARD_GET_ALL_DEVICES_SUCCESS: {
            return {
                ...state,
                allDevices: action.payload ? action.payload:[]
            }
        }
        case DASHBOARD_DASHBOARD_DEVICE_CHANGED_SUCCESS: {
            const { trend, data } = action.payload;
            if (trend === "current") {
                return {
                    ...state,
                    dashboardDeviceCurrentStatus: data ? data : []
                }
            } else if (trend === "daily") {
                return {
                    ...state,
                    dashboardDeviceDailyStatistics: data ? data : []
                }
            } else if (trend === "weekly") {
                return {
                    ...state,
                    dashboardDeviceWeeklyStatistics: data ? data : []
                }
            } else if (trend === "monthly") {
                return {
                    ...state,
                    dashboardDeviceMonthlyStatistics: data ? data : []
                }
            } else if (trend === "yearly") {
                return {
                    ...state,
                    dashboardDeviceYearlyStatistics: data ? data : []
                }
            }else if (trend === "quarterly") {
                return {
                    ...state,
                    dashboardDeviceQuarterlyStatistics: data ? data : []
                }
            } else {
                return {
                    ...state,
                    deviceCustomStatistics: data ? data : []
                }
            }
        }
        default:
            return state;
    }
}
