import { DEVICE_DEVICE_CLEAR, DEVICE_SHOW_LOADER, DEVICE_HIDE_LOADER, DEVICE_GET_ALL_DEVICE_REQUEST_SUCCESS, DEVICE_DEVICE_NAME_CHANGED, DEVICE_DEVICE_IMEI_CHANGED, DEVICE_DEVICE_LOCATION_CHANGED, DEVICE_DEVICE_IP_CHANGED, DEVICE_GET_DEVICE_REQUEST_SUCCESS, DEVICE_DEVICE_EDIT_FILL_UP, DEVICE_DEVICE_STATUS_CHANGED_SUCCESS, DEVICE_GET_DEVICE_STATISTICS_SUCCESS, DEVICE_GET_ALL_LOCATION_REQUEST, DEVICE_GET_ALL_LOCATION_REQUEST_SUCCESS } from "../constants/ActionTypes";

const INIT_STATE = {
    loader: false,
    deviceName: "",
    deviceIMEI: "",
    deviceIP: "",
    deviceLocation: "",
    deviceStatus: "",
    allDevices: [],
    allLocations: [],
    deviceInformation: [],
    deviceCurrentStatus: [],
    deviceDailyStatistics: [],
    deviceWeeklyStatistics: [],
    deviceMonthlyStatistics: [],
    deviceYearlyStatistics: [],
    deviceCustomStatistics: []
};


export default (state = INIT_STATE, action) => {
    switch (action.type) {

        case DEVICE_DEVICE_NAME_CHANGED: {
            return {
                ...state,
                deviceName: action.payload
            }
        }

        case DEVICE_DEVICE_IMEI_CHANGED: {
            return {
                ...state,
                deviceIMEI: action.payload
            }
        }

        case DEVICE_DEVICE_LOCATION_CHANGED: {
            return {
                ...state,
                deviceLocation: action.payload
            }
        }

        case DEVICE_DEVICE_IP_CHANGED: {
            return {
                ...state,
                deviceIP: action.payload
            }
        }

        case DEVICE_GET_ALL_DEVICE_REQUEST_SUCCESS: {
            return {
                ...state,
                allDevices: action.payload
            }
        }
        case DEVICE_DEVICE_CLEAR: {
            return {
                ...state,
                deviceName: "",
                deviceIMEI: "",
                deviceIP: "",
                deviceStatus: "",
                deviceLocation: "",
                deviceInformation: []
            }
        }

        case DEVICE_SHOW_LOADER: {
            return {
                ...state,
                loader: true
            }
        }

        case DEVICE_HIDE_LOADER: {
            return {
                ...state,
                loader: false
            }
        }

        case DEVICE_GET_DEVICE_REQUEST_SUCCESS: {
            return {
                ...state,
                deviceInformation: action.payload
            }
        }

        case DEVICE_DEVICE_EDIT_FILL_UP: {
            return {
                ...state,
                deviceName: action.payload.deviceName,
                deviceIMEI: action.payload.deviceIMEI,
                deviceIP: action.payload.deviceIp,
                deviceLocation: action.payload.locationId
            }
        }

        case DEVICE_DEVICE_STATUS_CHANGED_SUCCESS: {
            const { allDevices } = state;
            const { index, from } = action.payload;
            if (from == "device") {
                allDevices[index].isActive = !allDevices[index].isActive;
            } else if (from == "solar") {
                allDevices[index].solarStatus = !allDevices[index].solarStatus;

            } else if (from == "battery") {
                allDevices[index].batteryStatus = !allDevices[index].batteryStatus;

            } else if (from == "generator") {
                allDevices[index].generatorStatus = !allDevices[index].generatorStatus;

            }
            return {
                ...state,
                allDevices,
            }
        }

        case DEVICE_GET_DEVICE_STATISTICS_SUCCESS: {
            const { trend, data } = action.payload;
            if (trend === "current") {
                return {
                    ...state,
                    deviceCurrentStatus: data ? data : []
                }
            } else if (trend === "daily") {
                return {
                    ...state,
                    deviceDailyStatistics: data ? data : []
                }
            } else if (trend === "weekly") {
                return {
                    ...state,
                    deviceWeeklyStatistics: data ? data : []
                }
            } else if (trend === "monthly") {
                return {
                    ...state,
                    deviceMonthlyStatistics: data ? data : []
                }
            } else if (trend === "yearly") {
                return {
                    ...state,
                    deviceYearlyStatistics: data ? data : []
                }
            } else {
                return {
                    ...state,
                    deviceCustomStatistics: data ? data : []
                }
            }
        }

        case DEVICE_GET_ALL_LOCATION_REQUEST_SUCCESS: {
            return {
                ...state,
                allLocations: action.payload.length > 0 ? action.payload : []
            }
        }
        default:
            return state;
    }
}
