import {combineReducers} from 'redux';
import {connectRouter} from 'connected-react-router'
import Settings from './Settings';
import Auth from './Auth';
import Staff from './Staff';
import Devices from './Devices';
import Location from './Location';
import Dashboard from './Dashboard';
import Subscription from './Subscription';
import Account from './Account';
import BillingInformation from './BillingInformation';
export default (history) => combineReducers({
  router: connectRouter(history),
  settings: Settings,
  auth: Auth,
  staff : Staff,
  devices: Devices,
  location:Location,
  dashboard: Dashboard,
  subscription: Subscription,
  account:Account,
  billing:BillingInformation
});
