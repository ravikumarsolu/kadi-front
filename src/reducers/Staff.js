import { STAFF_USER_CONTACT_NUMBER_CHANGED, STAFF_USER_EMAIL_CHANGED, STAFF_USER_DEVICE_NAME_CHANGED, STAFF_USER_DEVICE_CHANGED, STAFF_USER_FIRST_NAME_CHANGED, STAFF_USER_LAST_NAME_CHANGED, STAFF_USER_CLEAR, STAFF_USER_GET_ALL_STAFF_USERS_REQUEST_SUCCESS, STAFF_SHOW_MESSAGE, STAFF_HIDE_MESSAGE, STAFF_SHOW_MESSAGE_SUCCESS, STAFF_HIDE_MESSAGE_SUCCESS, STAFF_SHOW_LOADER, STAFF_HIDE_LOADER, STAFF_USER_GET_STAFF_USERS_REQUEST, STAFF_USER_GET_STAFF_USERS_REQUEST_SUCCESS, STAFF_USER_EDIT_FILL_UP, STAFF_USER_GET_USER_DEVICES_SUCCESS, STAFF_USER_GET_ALL_UNALLOCATED_DEVICES_SUCCESS, STAFF_USER_ADDRESS_CHANGED, STAFF_USER_STATUS_CHANGED_SUCCESS } from "../constants/ActionTypes";

const INIT_STATE = {
    loader: false,
    alertMessage: '',
    alertMessageSuccess: '',
    showMessage: false,
    showMessageSuccess: false,
    firstName: "",
    lastName: "",
    contactNumber: "",
    email: "",
    address:"",
    devices:[],
    availableDevices:[],
    deviceName: "",
    deviceIMEI: "",
    staffUsers: [],
    staffUserDetails:[],
};


export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case STAFF_USER_FIRST_NAME_CHANGED: {
            return {
                ...state,
                firstName: action.payload
            }
        }

        case STAFF_USER_LAST_NAME_CHANGED: {
            return {
                ...state,
                lastName: action.payload
            }
        }

        case STAFF_USER_CONTACT_NUMBER_CHANGED: {
            return {
                ...state,
                contactNumber: action.payload
            }
        }

        case STAFF_USER_EMAIL_CHANGED: {
            return {
                ...state,
                email: action.payload
            }
        }

        case STAFF_USER_ADDRESS_CHANGED: {
            return {
                ...state,
                address: action.payload
            }
        }

        case STAFF_USER_DEVICE_NAME_CHANGED: {
            return {
                ...state,
                deviceName: action.payload
            }
        }

        case STAFF_USER_DEVICE_CHANGED: {
            return {
                ...state,
                deviceIMEI: action.payload
            }
        }
        case STAFF_USER_CLEAR: {
            return {
                ...state,
                firstName: "",
                lastName: "",
                contactNumber: "",
                email: "",
                deviceName: "",
                deviceIMEI: "",
                address:""
            }
        }
        
        case STAFF_USER_GET_ALL_STAFF_USERS_REQUEST_SUCCESS: {
            return {
                ...state,
                staffUsers: action.payload
            }
        }
        
        case STAFF_USER_GET_STAFF_USERS_REQUEST_SUCCESS: {
            return{
                ...state,
                staffUserDetails:action.payload
            }
        }
        case STAFF_SHOW_MESSAGE: {
            return {
                ...state,
                alertMessage: action.payload,
                showMessage: true,
                loader: false
            }
        }
        case STAFF_HIDE_MESSAGE: {
            return {
                ...state,
                alertMessage: '',
                showMessage: false,
                loader: false
            }
        }

        case STAFF_SHOW_MESSAGE_SUCCESS: {
            return {
                ...state,
                alertMessageSuccess: action.payload,
                showMessageSuccess: true,
                loader: false
            }
        }
        case STAFF_HIDE_MESSAGE_SUCCESS: {
            return {
                ...state,
                alertMessageSuccess: '',
                showMessageSuccess: false,
                loader: false
            }
        }

        case STAFF_SHOW_LOADER: {
            return {
                ...state,
                loader: true
            }
        }

        case STAFF_HIDE_LOADER: {
            return {
                ...state,
                loader: false
            }
        }

        case STAFF_USER_EDIT_FILL_UP:{
            const { firstName, lastName, email, contactNumber, devices, id, address } = action.payload
            return{
                ...state,
                firstName,
                lastName,
                email,
                contactNumber,
                address,
                devices,
            }
        }

        case STAFF_USER_GET_ALL_UNALLOCATED_DEVICES_SUCCESS: {
            return{
                ...state,
                availableDevices:action.payload
            }
        }

        case STAFF_USER_STATUS_CHANGED_SUCCESS: {
            const { staffUsers } = state;
            staffUsers.customers[action.payload].status = !staffUsers.customers[action.payload].status;
            return{
                ...state,
                staffUsers,
            }
        }
        default:
            return state;
    }
}
