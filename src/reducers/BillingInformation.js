import {BILLING_SHOW_LOADER, BILLING_GET_ALL_PAYMENT_HISTORY_SUCCESS, BILLING_HIDE_LOADER, BILLING_GET_INVOICE_DETAILS, BILLING_GET_INVOICE_DETAILS_SUCCESS, BILILNG_MOBILE_NUMBER_CHANGED } from "../constants/ActionTypes";

const INIT_STATE = {
    loader: false,
    allPaymentHistory: [],
    invoiceDetails:{},
    mobileNumber:""
};


export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case BILLING_SHOW_LOADER: {
            return {
                ...state,
                loader: true
            }
        }

        case BILLING_HIDE_LOADER: {
            return {
                ...state,
                loader: false
            }
        }

        case BILLING_GET_ALL_PAYMENT_HISTORY_SUCCESS:{
            return{
                ...state,
                allPaymentHistory:action.payload.length > 0 ? action.payload : []
            }
        }

        case BILLING_GET_INVOICE_DETAILS_SUCCESS:
            {
                return{
                    ...state,
                    invoiceDetails: Object.keys(action.payload).length > 0 ? action.payload : {}
                }
            }
        case BILILNG_MOBILE_NUMBER_CHANGED:{
            return{
                ...state,
                mobileNumber:action.payload
            }
        }
        default:
            return state;
    }
}
