import { ACCOUNT_SHOW_LOADER, ACCOUNT_HIDE_LOADER, ACCOUNT_GET_ALL_DEVICES_SUCCESS, ACCOUNT_HELP_SUPPORT_DESCRIPTION_CHANGED, ACCOUNT_CURRENT_PASSWORD_CHANGED, ACCOUNT_NEW_PASSWORD_CHANGED, ACCOUNT_CONFIRM_PASSWORD_CHANGED, ACCOUNT_CLEAR_CHANGE_PASSWORD } from "../constants/ActionTypes";

const INIT_STATE = {
    loader: false,
    helpLoader:false,
    changePasswordLoader:false,
    allDevices: [],
    helpDescription: "",
    currentPassword:"",
    newPassword:"",
    confirmPassword:""
};


export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case ACCOUNT_SHOW_LOADER: {
            if(action.payload == "helpLoader"){
            return {
                ...state,
                helpLoader: true
            }}else if(action.payload ="changePassword"){
                return{
                ...state,
                changePasswordLoader: true
                }
            }
        }

        case ACCOUNT_HIDE_LOADER: {
            if(action.payload == "helpLoader"){
                return {
                    ...state,
                    helpLoader: false
                }}else if(action.payload ="changePassword"){
                    return{
                    ...state,
                    changePasswordLoader: false
                    }
                }
        }

        case ACCOUNT_GET_ALL_DEVICES_SUCCESS: {
            return {
                ...state,
                allDevices: action.payload.length > 0 ? action.payload : []
            }
        }

        case ACCOUNT_HELP_SUPPORT_DESCRIPTION_CHANGED: {
            return {
                ...state,
                helpDescription:action.payload
            }
        }

        case ACCOUNT_CURRENT_PASSWORD_CHANGED:{
            return{
                ...state,
                currentPassword:action.payload
            }
        }

        case ACCOUNT_NEW_PASSWORD_CHANGED:{
            return{
                ...state,
                newPassword:action.payload
            }
        }

        case ACCOUNT_CONFIRM_PASSWORD_CHANGED:{
            return{
                ...state,
                confirmPassword:action.payload
            }
        }
        
        case ACCOUNT_CLEAR_CHANGE_PASSWORD:{
            return{
                ...state,
                currentPassword:"",
                newPassword:"",
                confirmPassword:""
            }
        }

        default:
            return state;
    }
}
