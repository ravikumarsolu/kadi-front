import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../../../actions';
import Button from '@material-ui/core/Button';
import { withRouter, Redirect, DelayedRedirect } from 'react-router-dom';
import ContainerHeader from "components/ContainerHeader/index";

class InvoiceDownload extends Component {
    state = { counter: 0 }

    componentWillMount() {
        this.props.billingGetInvoiceDetails(window.location.href.split("/")[5])
    }

    renderBill = () => {
        const { invoiceDetails, match } = this.props;

        return (
            <div className="app-wrapper">
                <ContainerHeader match={match} title={"Invoice"} />
                <div className="row accountsetting">

                    <div className="col-12">

                        <div className="jr-card card">
                            <div className="row">
                                <div className="col-12 col-md-6">

                                    <ul className="profiledetail">
                                        <li>
                                            <label>Invoice Month</label>
                                            <span>{invoiceDetails.month}, {invoiceDetails.year}</span>
                                        </li>
                                        <li>
                                            <label>Terms</label>
                                            <span>30 Days</span>
                                        </li>
                                    </ul>
                                </div>
                                <div className="col-12 col-md-6">
                                    <table className="table invoice-table">
                                        <thead>
                                            <tr>
                                                <th>Source</th>
                                                <th style={{ textAlign: "right" }}>Units</th>
                                                <th style={{ textAlign: "right" }}>Per unit charge</th>
                                                <th style={{ textAlign: "right" }}>Amount</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Solar</td>
                                                <td align="right">{invoiceDetails.solarUnits}</td>
                                                <td align="right">{invoiceDetails.amountPerUnit}</td>
                                                <td align="right">{invoiceDetails.solarUnits * invoiceDetails.amountPerUnit}</td>
                                            </tr>
                                            <tr>
                                                <td>Grid</td>
                                                <td align="right">{invoiceDetails.gridUnits}</td>
                                                <td align="right">{invoiceDetails.amountPerUnit}</td>
                                                <td align="right">{invoiceDetails.gridUnits * invoiceDetails.amountPerUnit}</td>
                                            </tr>
                                            <tr>
                                                <td>Generator</td>
                                                <td align="right">{invoiceDetails.generatorUnits}</td>
                                                <td align="right">{invoiceDetails.amountPerUnit}</td>
                                                <td align="right">{invoiceDetails.generatorUnits * invoiceDetails.amountPerUnit}</td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td>Total</td>
                                                <td align="right">{(parseInt(invoiceDetails.solarUnits)) + (parseInt(invoiceDetails.gridUnits)) + (parseInt(invoiceDetails.generatorUnits))}</td>
                                                <td align="right">-</td>
                                                <td align="right">{((parseInt(invoiceDetails.solarUnits)) + (parseInt(invoiceDetails.gridUnits)) + (parseInt(invoiceDetails.generatorUnits))) * invoiceDetails.amountPerUnit}</td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                    {/* <ul className="invoice-table">
                                    <li>
                                        <label>Source</label>
                                        <label>Units</label>
                                        <label>Per unit charge</label>
                                        <label>Amount</label>
                                    </li>
                                    <li>
                                        <label>Solar</label>
                                        <label>{invoiceDetails.solarUnits}</label>
                                        <label>{invoiceDetails.amountPerUnit}</label>
                                        <label>{invoiceDetails.solarUnits * invoiceDetails.amountPerUnit}</label>
                                    </li>
                                    <li>
                                        <label>Grid</label>
                                        <label>{invoiceDetails.gridUnits}</label>
                                        <label>{invoiceDetails.amountPerUnit}</label>
                                        <label>{invoiceDetails.gridUnits * invoiceDetails.amountPerUnit}</label>
                                    </li>
                                    <li>
                                        <label>Generator</label>
                                        <label>{invoiceDetails.generatorUnits}</label>
                                        <label>{invoiceDetails.amountPerUnit}</label>
                                        <label>{invoiceDetails.generatorUnits * invoiceDetails.amountPerUnit}</label>
                                    </li>
                                    <li className="invoice-footer">
                                        <label>Total</label>
                                        <label>{(parseInt(invoiceDetails.solarUnits)) + (parseInt(invoiceDetails.gridUnits)) + (parseInt(invoiceDetails.generatorUnits))}</label>
                                        <label>-</label>
                                        <label>{((parseInt(invoiceDetails.solarUnits)) + (parseInt(invoiceDetails.gridUnits)) + (parseInt(invoiceDetails.generatorUnits))) * invoiceDetails.amountPerUnit}</label>
                                    </li>
                                </ul> */}
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>)

    }
    render() {
        if (Object.keys(this.props.invoiceDetails).length > 0 && this.state.counter == 0) {
            this.setState({ counter: 1 })
            setTimeout(() => {
                window.print();
                this.props.history.push('/app/billinginfo');
            }, 500);
        }
        return (
            this.renderBill()
        );
    }
}


const mapStateToProps = ({ billing }) => {
    const { loader, invoiceDetails } = billing;
    return { loader, invoiceDetails }
};
const mapDispatchToProps = dispatch => ({
    billingShowLoader: () => dispatch(actions.billingShowLoader()),
    billingHideLoader: () => dispatch(actions.billingHideLoader()),
    billingGetInvoiceDetails: payload => dispatch(actions.billingGetInvoiceDetails(payload))


});
export default connect(mapStateToProps, mapDispatchToProps)(InvoiceDownload);