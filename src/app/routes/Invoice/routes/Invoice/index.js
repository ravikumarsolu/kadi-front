import React, { Component } from "react";
import ContainerHeader from "components/ContainerHeader/index";
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import CircularProgress from '@material-ui/core/CircularProgress';
import * as actions from '../../../../../actions';
import BillingInformationModal from '../../../components/routes/dialogs/billingInformation/index.js'
class Invoice extends Component {
  constructor(props) {
    super(props);
    this.state = {
      buyModal: false
    }
  }

  toggleBuyModal = () => {
    this.setState({ buyModal: false, planId: "" });
  }

  componentWillMount() {
    this.props.billingGetInvoiceDetails(window.location.href.split("/")[5])
  }


  render() {
    const { match, loader, invoiceDetails } = this.props;
    const { buyModal } = this.state;

    return (
      <div className="dashboard animated slideInUpTiny animation-duration-3">

        <ContainerHeader match={match} title={"Invoice"} />

        {loader ? (<div className="loader-view">
          <CircularProgress />
        </div>) :
          Object.keys(invoiceDetails).length > 0 ?
            <div className="row invoicesetting">

              <div className="col-12">

                <div className="jr-card card">
                  <div className="row">
                    <div className="col-12 col-md-6">

                      <ul className="profiledetail">
                        <li>
                          <label>Invoice Month</label>
                          <span>{invoiceDetails.month}, {invoiceDetails.year}</span>
                        </li>
                        <li>
                          <label>Terms</label>
                          <span>30 Days</span>
                        </li>
                        {/* <li>
                        <label>Due Date</label>
                        <span>19 October, 2019</span>
                      </li> */}
                      </ul>
                    </div>
                    <div className="col-12 col-md-6">
                      <table className="table invoice-table">
                        <thead>
                          <tr>
                            <th>Source</th>
                            <th style={{ textAlign: "right" }}>Units</th>
                            <th style={{ textAlign: "right" }}>Per unit charge</th>
                            <th style={{ textAlign: "right" }}>Amount</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>Solar</td>
                            <td align="right">{invoiceDetails.solarUnits}</td>
                            <td align="right">{invoiceDetails.amountPerUnit}</td>
                            <td align="right">{invoiceDetails.solarUnits * invoiceDetails.amountPerUnit}</td>
                          </tr>
                          <tr>
                            <td>Grid</td>
                            <td align="right">{invoiceDetails.gridUnits}</td>
                            <td align="right">{invoiceDetails.amountPerUnit}</td>
                            <td align="right">{invoiceDetails.gridUnits * invoiceDetails.amountPerUnit}</td>
                          </tr>
                          <tr>
                            <td>Generator</td>
                            <td align="right">{invoiceDetails.generatorUnits}</td>
                            <td align="right">{invoiceDetails.amountPerUnit}</td>
                            <td align="right">{invoiceDetails.generatorUnits * invoiceDetails.amountPerUnit}</td>
                          </tr>
                        </tbody>
                        <tfoot>
                          <tr className="invoice-footer">
                            <td>Total</td>
                            <td align="right">{(parseInt(invoiceDetails.solarUnits)) + (parseInt(invoiceDetails.gridUnits)) + (parseInt(invoiceDetails.generatorUnits))}</td>
                            <td align="right">-</td>
                            <td align="right">{((parseInt(invoiceDetails.solarUnits)) + (parseInt(invoiceDetails.gridUnits)) + (parseInt(invoiceDetails.generatorUnits))) * invoiceDetails.amountPerUnit}</td>
                          </tr>
                          {/* <li className="invoice-footer">
                          <label>Total Payout</label>
                          <span>$20.00</span>
                        </li> */}
                        </tfoot>
                      </table>
                    </div>
                  </div>
                  <div className="d-flex flex-row flex-wrap align-items-center justify-content-center">
                    <Button className="customButton" variant="contained" onClick={() => { this.setState({ buyModal: true }) }}>Pay</Button>
                    <Button className="customButton lineButton" variant="contained">Cancel</Button>
                  </div>
                </div>

              </div>
            </div> : "No record found"}
        <BillingInformationModal
          open={buyModal}
          billingId={window.location.href.split("/")[5]}
          handleRequestClose={this.toggleBuyModal}
          amount={((parseInt(invoiceDetails.solarUnits)) + (parseInt(invoiceDetails.gridUnits)) + (parseInt(invoiceDetails.generatorUnits))) * invoiceDetails.amountPerUnit}
        />
      </div>
    );
  }
}

const mapStateToProps = ({ billing }) => {
  const { loader, invoiceDetails } = billing;
  return { loader, invoiceDetails }
};
const mapDispatchToProps = dispatch => ({
  billingShowLoader: () => dispatch(actions.billingShowLoader()),
  billingHideLoader: () => dispatch(actions.billingHideLoader()),
  billingGetInvoiceDetails: payload => dispatch(actions.billingGetInvoiceDetails(payload))


});
export default connect(mapStateToProps, mapDispatchToProps)(Invoice);