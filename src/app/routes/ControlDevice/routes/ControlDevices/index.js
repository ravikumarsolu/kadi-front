import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import IconButton from '@material-ui/core/IconButton';
import { connect } from 'react-redux';
import * as actions from '../../../../../actions';
import Switch from '@material-ui/core/Switch';
import ChangeStatusDialog from '../../../components/routes/dialogs/statusChange';
import CircularProgress from '@material-ui/core/CircularProgress';

const actionsStyles = theme => ({
  root: {
    flexShrink: 0,
    color: theme.palette.text.secondary,
    marginLeft: theme.spacing.unit * 2.5,
  },
});

class TablePaginationActions extends React.Component {
  handleFirstPageButtonClick = event => {
    this.props.onChangePage(event, 0);
  };

  handleBackButtonClick = event => {
    this.props.onChangePage(event, this.props.page - 1);
  };

  handleNextButtonClick = event => {
    this.props.onChangePage(event, this.props.page + 1);
  };

  handleLastPageButtonClick = event => {
    this.props.onChangePage(
      event,
      Math.max(0, Math.ceil(this.props.count / this.props.rowsPerPage) - 1),
    );
  };


  render() {
    const { classes, count, page, rowsPerPage, theme } = this.props;

    return (
      <div className={classes.root}>
        <IconButton
          onClick={this.handleFirstPageButtonClick}
          disabled={page === 0}
          aria-label="First Page"
        >
          {theme.direction === 'rtl' ?
            <i className="zmdi zmdi-skip-next" /> : <i className="zmdi zmdi-skip-previous" />}
        </IconButton>
        <IconButton
          onClick={this.handleBackButtonClick}
          disabled={page === 0}
          aria-label="Previous Page"
        >
          {theme.direction === 'rtl' ?
            <i className="zmdi zmdi-chevron-right" /> : <i className="zmdi zmdi-chevron-left" />}
        </IconButton>
        <IconButton
          onClick={this.handleNextButtonClick}
          disabled={page >= Math.ceil(count / rowsPerPage) - 1}
          aria-label="Next Page"
        >
          {theme.direction === 'rtl' ?
            <i className="zmdi zmdi-chevron-left" /> : <i className="zmdi zmdi-chevron-right" />}
        </IconButton>
        <IconButton
          onClick={this.handleLastPageButtonClick}
          disabled={page >= Math.ceil(count / rowsPerPage) - 1}
          aria-label="Last Page"
        >
          {theme.direction === 'rtl' ?
            <i className="zmdi zmdi-skip-previous" /> : <i className="zmdi zmdi-skip-next" />}
        </IconButton>
      </div>
    );
  }
}

TablePaginationActions.propTypes = {
  classes: PropTypes.object.isRequired,
  count: PropTypes.number.isRequired,
  onChangePage: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
  theme: PropTypes.object.isRequired,
};

const TablePaginationActionsWrapped = withStyles(actionsStyles, { withTheme: true })(
  TablePaginationActions,
);

let counter = 0;

function createData(name, calories, fat) {
  counter += 1;
  return { id: counter, name, calories, fat };
}

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
  },
  table: {
    minWidth: 500,
  },
  tableWrapper: {
    overflowX: 'auto',
  },
});

class ControlDevices extends React.Component {

  constructor(props, context) {
    super(props, context);

    this.state = {
      page: 0,
      rowsPerPage: 10,
      checkedA: false,
      openDeleteDialog: false,
      statusData: {},
    };
  }
  componentWillMount() {
    this.props.deviceGetAllDeviceRequest();
  }

  handleChangePage = (event, page) => {
    this.setState({ page });
  };
  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
  };
  handleRequestClose = () => {
    this.setState({ openDeleteDialog: false, statusData: {} });

  };


  render() {
    const { classes, allDevices, loader } = this.props;
    const { openDeleteDialog } = this.state;

    return (
      <div className="dashboard animated slideInUpTiny animation-duration-3">

        {/* <ContainerHeader match={match} title={"Account Settings"}/> */}

        <div className="row accountsetting">
          <div className="col-12">
            <div className="jr-card card h-100vh mb-0">
              <div className="d-flex flex-row flex-wrap align-items-center justify-content-between userhead">
                <div className="d-flex flex-row align-items-center justify-content-between">
                  <h2 className="text-white">Control Devices</h2>
                </div>
              </div>
              {loader ? (<div className="loader-view">
                <CircularProgress />
              </div>) : allDevices.length > 0 ?
                  (<div className="row">
                    <div className="col-12">
                      {/* <Paper className={classes.root}> */}
                      <div className={classes.tableWrapper}>

                        <Table className={classes.table}>
                          <TableHead>
                            <TableRow>
                              <TableCell>Device Name</TableCell>
                              <TableCell>Device IMEI</TableCell>
                              <TableCell>Solar</TableCell>
                              <TableCell>Battery</TableCell>
                              <TableCell>Generator</TableCell>
                            </TableRow>
                          </TableHead>
                          <TableBody>
                            {
                              allDevices.map((item, index) => (
                                <TableRow key={index}>
                                  <TableCell>{item.deviceName}</TableCell>
                                  <TableCell>{item.deviceIMEI}</TableCell>
                                  <TableCell>
                                    <Switch
                                      className="customSwitch"
                                      classes={{
                                        checked: 'text-success',
                                        bar: 'bg-white',
                                      }}
                                      defaultChecked={item.solarStatus}
                                      onChange={(e) => {
                                        this.setState({ statusData: { id: item.deviceIMEI, status: e.target.checked, index, from: "solar" }, openDeleteDialog: true });
                                      }}
                                    />
                                  </TableCell>
                                  <TableCell>
                                    <Switch
                                      className="customSwitch"
                                      classes={{
                                        checked: 'text-success',
                                        bar: 'bg-white',
                                      }}
                                      defaultChecked={item.batteryStatus}
                                      onChange={(e) => {
                                        this.setState({ statusData: { id: item.deviceIMEI, status: e.target.checked, index, from: "battery" }, openDeleteDialog: true });
                                      }}
                                    />
                                  </TableCell>
                                  <TableCell>
                                    <Switch
                                      className="customSwitch"
                                      classes={{
                                        checked: 'text-success',
                                        bar: 'bg-white',
                                      }}
                                      defaultChecked={item.generatorStatus}
                                      onChange={(e) => {
                                        this.setState({ statusData: { id: item.deviceIMEI, status: e.target.checked, index, from: "generator" }, openDeleteDialog: true });
                                      }}
                                    />
                                  </TableCell>

                                </TableRow>
                              ))}

                          </TableBody>
                          {/* <TableFooter>
                        <TableRow>
                          <TablePagination
                            count={allDevices.length}
                            rowsPerPage={rowsPerPage}
                            page={page}
                            onChangePage={this.handleChangePage}
                            onChangeRowsPerPage={this.handleChangeRowsPerPage}
                            ActionsComponent={TablePaginationActionsWrapped}
                          />
                        </TableRow>
                      </TableFooter> */}
                        </Table>
                      </div>
                      {/* </Paper> */}
                    </div>
                  </div>) : (<h2 className="text-white"> No records found</h2>)}
            </div>
          </div>
        </div>
        {openDeleteDialog ?
          <ChangeStatusDialog
            open={openDeleteDialog}
            handleRequestClose={this.handleRequestClose}
            statusData={this.state.statusData}
            deviceDeviceStatusChanged={this.props.deviceDeviceStatusChanged}
          /> : null}
      </div>
    );
  }
}



const mapStateToProps = ({ devices }) => {
  const { allDevices, loader } = devices;
  return { allDevices, loader }
};
const mapDispatchToProps = dispatch => ({
  deviceGetAllDeviceRequest: () => dispatch(actions.deviceGetAllDeviceRequest()),
  deviceShowLoader: () => dispatch(actions.deviceShowLoader()),
  deviceHideLoader: () => dispatch(actions.deviceHideLoader()),
  deviceDeviceStatusChanged: payload => dispatch(actions.deviceDeviceStatusChanged(payload)),
});
export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(ControlDevices));