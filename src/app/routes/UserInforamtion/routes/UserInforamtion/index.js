import React, { Component } from "react";
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import ContainerHeader from "components/ContainerHeader/index";
import moment from 'moment';
import { connect } from 'react-redux';
import * as actions from '../../../../../actions';
class UserInformation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedDeviceIndex: 0
    };
  }
  componentWillMount() {
    this.props.staffUserGetStaffUsersRequest(window.location.href.split("/")[5]);
  }
  handleChange = event => {
    this.setState({ selectedDeviceIndex: event.target.value });

  };
  render() {
    const { match, staffUserDetails } = this.props;
    const { customer, device } = staffUserDetails;
    const { selectedDeviceIndex } = this.state;
    return (
      <div className="deviceinfo animated slideInUpTiny animation-duration-3">
        <ContainerHeader match={match} title={"User Details"} />
        <div className="row">
          <div className="col-12">
            <div className="jr-card card">
              <h2 class="text-white">User Information</h2>
              <div className="row">
                <div className="col-xl-6 col-lg-6 col-md-8 col-sm-9 col-12">
                  <ul class="profiledetail">
                    <li>
                      <label>Name</label>
                      <span>{customer ? staffUserDetails.customer.firstName : ""} {customer ? customer.lastName : ""}</span>
                    </li>
                    <li>
                      <label>Contact Number</label>
                      <span>{customer ? customer.contactNumber : ""}</span>
                    </li>
                    <li>
                      <label>Email ID</label>
                      <span>{customer ? customer.email : ""}</span>
                    </li>
                    <li>
                      <label>Address</label>
                      <span>{customer ? customer.address : ""}</span>
                    </li>
                    <li>
                      <label>User Status</label>
                      <span>{customer ? customer.status ? "Active" : "Inactive" : ""}</span>
                    </li>
                  </ul>
                </div>
              </div>

            </div>
            {device != undefined && device.length != 0 ?
              <div className="jr-card card">
                <div className="row">
                  <div className="col-xl-6 col-lg-6 col-md-8 col-sm-9 col-12">
                    <h2 class="text-white">Device Information</h2>
                    <FormControl className="w-100 mb-2">
                      <Select className="customSelect"
                        value={selectedDeviceIndex}
                        onChange={this.handleChange}
                      >
                        {device.map((item, index) => (
                          <MenuItem
                            key={index}
                            value={index}
                          >
                            {item.deviceIMEI}
                          </MenuItem>
                        ))}
                      </Select>
                    </FormControl>
                    <ul class="profiledetail">
                      <li>
                        <label>Device Name</label>
                        <span>{device[selectedDeviceIndex].deviceName}</span>
                      </li>
                      <li>
                        <label>Device IP</label>
                        <span>{device[selectedDeviceIndex].deviceIp}</span>
                      </li>
                      <li>
                        <label>Device IMEI</label>
                        <span>{device[selectedDeviceIndex].deviceIMEI}</span>
                      </li>
                      <li>
                        <label>Device Location</label>
                        <span>{device[selectedDeviceIndex].locationId}</span>
                      </li>
                    </ul>
                  </div>
                  <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                    {/* <img src={require('../../../../../assets/images/map-white.png')} /> */}
                  </div>
                </div>

              </div> : null}

          </div>

        </div>
      </div>
    );
  }
}
const mapStateToProps = ({ staff }) => {
  const { staffUserDetails, loader, alertMessage, showMessage, showMessageSuccess, alertMessageSuccess } = staff;
  return { staffUserDetails, loader, alertMessage, showMessage, showMessageSuccess, alertMessageSuccess }
};
const mapDispatchToProps = dispatch => ({
  staffUserGetStaffUsersRequest: payload => dispatch(actions.staffUserGetStaffUsersRequest(payload)),

});
export default connect(mapStateToProps, mapDispatchToProps)(UserInformation);