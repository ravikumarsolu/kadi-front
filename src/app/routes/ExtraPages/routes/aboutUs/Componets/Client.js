import React from './node_modules/react';

const Client = ({client}) => {
  const {image} = client;

  return (
    <div className="brand-logo">
      <div className="brand-logo-inner">
        <img src={image} alt="Clients"/>
      </div>
    </div>
  );
};


export default Client;