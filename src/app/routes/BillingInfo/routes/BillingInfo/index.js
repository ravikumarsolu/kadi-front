import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import { Link, NavLink, withRouter } from 'react-router-dom';
import { DatePicker } from 'material-ui-pickers'
import { connect } from 'react-redux';
import * as actions from '../../../../../actions';
import CircularProgress from '@material-ui/core/CircularProgress';

import moment from 'moment';
const actionsStyles = theme => ({
  root: {
    flexShrink: 0,
    color: theme.palette.text.secondary,
    marginLeft: theme.spacing.unit * 2.5,
  },
});

class TablePaginationActions extends React.Component {
  handleFirstPageButtonClick = event => {
    this.props.onChangePage(event, 0);
  };

  handleBackButtonClick = event => {
    this.props.onChangePage(event, this.props.page - 1);
  };

  handleNextButtonClick = event => {
    this.props.onChangePage(event, this.props.page + 1);
  };

  handleLastPageButtonClick = event => {
    this.props.onChangePage(
      event,
      Math.max(0, Math.ceil(this.props.count / this.props.rowsPerPage) - 1),
    );
  };


  render() {
    const { classes, count, page, rowsPerPage, theme } = this.props;

    return (
      <div className={classes.root}>
        <IconButton
          onClick={this.handleFirstPageButtonClick}
          disabled={page === 0}
          aria-label="First Page"
        >
          {theme.direction === 'rtl' ?
            <i className="zmdi zmdi-skip-next" /> : <i className="zmdi zmdi-skip-previous" />}
        </IconButton>
        <IconButton
          onClick={this.handleBackButtonClick}
          disabled={page === 0}
          aria-label="Previous Page"
        >
          {theme.direction === 'rtl' ?
            <i className="zmdi zmdi-chevron-right" /> : <i className="zmdi zmdi-chevron-left" />}
        </IconButton>
        <IconButton
          onClick={this.handleNextButtonClick}
          disabled={page >= Math.ceil(count / rowsPerPage) - 1}
          aria-label="Next Page"
        >
          {theme.direction === 'rtl' ?
            <i className="zmdi zmdi-chevron-left" /> : <i className="zmdi zmdi-chevron-right" />}
        </IconButton>
        <IconButton
          onClick={this.handleLastPageButtonClick}
          disabled={page >= Math.ceil(count / rowsPerPage) - 1}
          aria-label="Last Page"
        >
          {theme.direction === 'rtl' ?
            <i className="zmdi zmdi-skip-previous" /> : <i className="zmdi zmdi-skip-next" />}
        </IconButton>
      </div>
    );
  }
}

TablePaginationActions.propTypes = {
  classes: PropTypes.object.isRequired,
  count: PropTypes.number.isRequired,
  onChangePage: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
  theme: PropTypes.object.isRequired,
};

const TablePaginationActionsWrapped = withStyles(actionsStyles, { withTheme: true })(
  TablePaginationActions,
);

let counter = 0;

function createData(name, calories, fat) {
  counter += 1;
  return { id: counter, name, calories, fat };
}

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
  },
  table: {
    minWidth: 500,
  },
  tableWrapper: {
    overflowX: 'auto',
  },
});

class BillingInfo extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      month: new Date(),
      year: new Date().getFullYear().toString(),
      page: 0,
      rowsPerPage: 5,
      checkedA: false,
      password: ''
    };
  }

  componentWillMount() {
    this.props.billingGetAllPaymentHistory({ type: "all" });
  }
  handleChangePage = (event, page) => {
    this.setState({ page });
  };
  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
  };
  handleDateChange = (origin, value) => {
    if (origin == "Year") {
      this.setState({ year: moment(value).format("YYYY") });
    }
    else {
      this.setState({ month: value });
    }
  }
  handleApply = () => {
    const { month, year } = this.state;
    this.props.billingGetAllPaymentHistory({ type: "custom", month, year });
  }

  handleReset = () => {
    this.setState({
      month: new Date(),
      year: new Date().getFullYear().toString()
    })
  }

  render() {
    const { classes, allPaymentHistory, loader } = this.props;
    const { month, year } = this.state;
    return (
      <div className="dashboard animated slideInUpTiny animation-duration-3">
        <div className="row accountsetting">
          <div className="col-12">
            <div className="jr-card card h-100vh mb-0">
              <div className="d-flex flex-row flex-wrap align-items-center justify-content-between userhead">
                <div className="d-flex flex-row align-items-center justify-content-between">
                  <h2 className="mb-3 text-white">Payment History</h2>
                </div>
              </div>
              <div className="paymentHead d-flex flex-row flex-wrap align-items-center justify-content-between">
                <div className="d-flex flex-row align-items-center">
                  <div className="d-flex flex-row align-items-center justify-content-between">
                    <label className="text-white">Month</label>
                    <div className="formgroup mb-0">

                      <DatePicker className="custominput"
                        fullWidth
                        value={month}
                        views={["month"]}
                        format={'MMMM'}
                        onChange={(value) => this.handleDateChange("Month", value)}
                        animateYearScrolling={false}
                        leftArrowIcon={<i className="zmdi zmdi-arrow-back" />}
                        rightArrowIcon={<i className="zmdi zmdi-arrow-forward" />}
                      />
                    </div>
                  </div>
                  <div className="d-flex flex-row align-items-center justify-content-between">
                    <label className="text-white">Year</label>
                    <div className="formgroup mb-0">
                      <DatePicker className="custominput"
                        fullWidth
                        views={["year"]}
                        value={year}
                        onChange={(value) => this.handleDateChange("Year", value)}
                        animateYearScrolling={false}
                        leftArrowIcon={<i className="zmdi zmdi-arrow-back" />}
                        rightArrowIcon={<i className="zmdi zmdi-arrow-forward" />}
                      />
                    </div>
                  </div>
                </div>
                <div className="d-flex flex-row flex-wrap align-items-center justify-content-between">
                  <Button className="customButton"
                    variant="contained"
                    onClick={this.handleApply}
                    disabled={loader}
                  >Apply</Button>
                  <Button className="customButton lineButton"
                    variant="contained"
                    onClick={this.handleReset}
                  >Reset</Button>
                </div>
              </div>

              {allPaymentHistory.length > 0 ?
                <div className="row">
                  <div className="col-12">
                    <div className={classes.tableWrapper}>
                      <Table className={classes.table}>
                        <TableHead>
                          <TableRow>
                            <TableCell>Device Name</TableCell>
                            <TableCell>Device IMEI</TableCell>
                            <TableCell>Month</TableCell>
                            <TableCell>Year</TableCell>
                            <TableCell>Unit Cost</TableCell>
                            <TableCell>Total Units</TableCell>
                            <TableCell>Amount</TableCell>
                            <TableCell>Actions</TableCell>
                          </TableRow>
                        </TableHead>
                        {loader ? (<div className="loader-view">
                          <CircularProgress />
                        </div>) :
                          <TableBody>
                            {allPaymentHistory.map((item, index) => (
                              <TableRow key={index}>
                                <TableCell>{item.deviceIMEI.deviceName}</TableCell>
                                <TableCell>{item.deviceIMEI.deviceIMEI}</TableCell>
                                <TableCell>{item.month}</TableCell>
                                <TableCell>{item.year}</TableCell>
                                <TableCell>{item.amountPerUnit}</TableCell>
                                <TableCell>{item.units}</TableCell>
                                <TableCell>{item.units * item.amountPerUnit}</TableCell>
                                <TableCell>
                                  <IconButton className="icon-btn">
                                    <NavLink to={`/app/invoice/${item.billingId}`}>
                                      <i className="zmdi zmdi-eye text-white" />
                                    </NavLink>
                                  </IconButton>
                                  <IconButton className="icon-btn">
                                  <NavLink to={`/app/invoiceDownlaod/${item.billingId}`}>
                                    <i className="zmdi zmdi-download" style={{color:"white"}}/>
                                    </NavLink>
                                  </IconButton>
                                </TableCell>
                              </TableRow>
                            ))}

                          </TableBody>}
                        {/* <TableFooter>
                        <TableRow>
                          <TablePagination
                            count={data.length}
                            rowsPerPage={rowsPerPage}
                            page={page}
                            onChangePage={this.handleChangePage}
                            onChangeRowsPerPage={this.handleChangeRowsPerPage}
                            ActionsComponent={TablePaginationActionsWrapped}
                          />
                        </TableRow>
                      </TableFooter> */}
                      </Table>
                    </div>
                  </div>
                </div> : loader ? (<div className="loader-view">
                  <CircularProgress />
                </div>) : "No records found"}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ billing }) => {
  const { loader, allPaymentHistory } = billing;
  return { loader, allPaymentHistory }
};
const mapDispatchToProps = dispatch => ({
  billingShowLoader: () => dispatch(actions.billingShowLoader()),
  billingHideLoader: () => dispatch(actions.billingHideLoader()),
  billingGetAllPaymentHistory: payload => dispatch(actions.billingGetAllPaymentHistory(payload))


});
export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(BillingInfo));