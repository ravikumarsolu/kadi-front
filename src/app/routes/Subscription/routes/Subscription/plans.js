import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import Button from '@material-ui/core/Button';


const SubscriptionPlans = props => {
const {subscriptionPlans,buyLoader,loaderIndex,handleBuy} = props;
    return(
        <div className="row">
        {subscriptionPlans.length > 0 ?
          subscriptionPlans.map((item, index) => (
            <div className="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12 mb-3">
              <div className="jr-card card h-100">
                <h2 className="text-white mb-4">{item.name}</h2>
                <ul className="profiledetail">
                  <li>
                    <label>User</label>
                    <span>{item.totalUsers}</span>
                  </li>
                  <li>
                    <label>Devices</label>
                    <span>{item.totalDevices}</span>
                  </li>
                  <li>
                    <label>Price(USD)</label>
                    <span>${item.price}</span>
                  </li>
                </ul>
                {buyLoader && loaderIndex == index? (<div className="loader-view">
                  <CircularProgress />
                </div>) :
                  <Button
                    disabled={buyLoader}
                    className="customButton" onClick={() =>
                      handleBuy(item.subscriptionPlanId,index)}> Buy</Button>
                }
              </div>
            </div>
          ))
          : null
        }
      </div>
    );
}

export default SubscriptionPlans;