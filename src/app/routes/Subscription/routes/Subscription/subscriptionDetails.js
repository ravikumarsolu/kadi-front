import React from 'react';
import moment from 'moment';


const SubscriptionDetails = props => {
    const {  subscriptionDetails } = props;

    return(
        <div className="jr-card card">
        <div className="row">
          <div className="col-xl-8 col-lg-8 col-md-8 col-sm-9 col-12">
            <ul className="profiledetail">
              <li>
                <label>Active Plan</label>
                <span>{subscriptionDetails ? subscriptionDetails.subscriptionDetails ? subscriptionDetails.subscriptionDetails.name : "" : ""}</span>
              </li>
              <li>
                <label>Last Payment Date</label>
                <span>{subscriptionDetails ? moment(subscriptionDetails.lastPaymentDate).format("DD MMM YYYY") : ""}</span>
              </li>
              <li>
                <label>Due Date</label>
                <span>{subscriptionDetails ? moment(subscriptionDetails.dueDate).format("DD MMM YYYY") : ""}</span>
              </li>
              <li>
                <label>Remaining Days</label>
                <span>20 Days</span>
              </li>
              <li>
                <label>Number of User</label>
                <span>{subscriptionDetails ? subscriptionDetails.subscriptionDetails ? subscriptionDetails.subscriptionDetails.totalUsers : "" : ""}</span>
              </li>
              <li>
                <label>Number of Devices</label>
                <span>{subscriptionDetails ? subscriptionDetails.subscriptionDetails ? subscriptionDetails.subscriptionDetails.totalDevices : "" : ""}</span>
              </li>
              <li>
                <label>Price(USD)</label>
                <span>${subscriptionDetails ? subscriptionDetails.subscriptionDetails ? subscriptionDetails.subscriptionDetails.price : "" : ""}</span>
              </li>
            </ul>
          </div>
        </div>

      </div>
    );
}

export default SubscriptionDetails;