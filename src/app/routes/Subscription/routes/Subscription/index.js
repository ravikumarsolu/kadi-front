import React, { Component, lazy, Suspense } from "react";
import * as actions from '../../../../../actions';
import { connect } from 'react-redux';
import CircularProgress from '@material-ui/core/CircularProgress';
import ContainerHeader from "components/ContainerHeader/index";
import SubscriptionModal from "../../../components/routes/dialogs/subscription";
const SubscriptionPlans = lazy(() => import('./plans'));
const SubscriptionDetails = lazy(() => import('./subscriptionDetails'));

class Subscription extends Component {
  constructor(props) {
    super(props);
    this.state = {
      buyModal: false,
      planId: "",
      loaderIndex: 0
    };
  }

  toggleBuyModal = () => {
    this.setState({ buyModal: false, planId: "" });
  }

  handleBuy = (planId, index) => {
    this.setState({ buyModal: true, planId: planId, loaderIndex: index })
  }
  componentWillMount() {
    let userId = localStorage.getItem("user_id");
    this.props.subscriptionGetUserSubscriptionDetails();
    this.props.subscriptionGetUserSubscriptionPlans(userId);
  }
  render() {
    const { loader, subscriptionDetails, subscriptionPlans, match, buyLoader } = this.props;
    const { buyModal, planId, loaderIndex } = this.state;
    console.log("TCL: render -> loaderIndex", loaderIndex)
    return (
      <div className="subscription animated slideInUpTiny animation-duration-3">

        <ContainerHeader match={match} title={"Subscription"} />

        <div className="row">
          <div className="col-12">
          <Suspense fallback={<div><CircularProgress /></div>}>
            <SubscriptionDetails
              subscriptionDetails={subscriptionDetails}
              loader={loader}
            />
            </Suspense>
            <Suspense fallback={<div><CircularProgress /></div>}>
              <SubscriptionPlans
                subscriptionPlans={subscriptionPlans}
                buyLoader={buyLoader}
                loaderIndex={loaderIndex}
                handleBuy={this.handleBuy}
              />
            </Suspense>
          </div>
        </div>
        <SubscriptionModal
          open={buyModal}
          handleRequestClose={this.toggleBuyModal}
          planId={planId} />
      </div>
    );
  }
}

const mapStateToProps = ({ subscription }) => {
  const { loader, subscriptionDetails, subscriptionPlans, buyLoader } = subscription;
  return { loader, subscriptionDetails, subscriptionPlans, buyLoader }
};
const mapDispatchToProps = dispatch => ({
  subscriptionShowLoader: () => dispatch(actions.subscriptionShowLoader()),
  subscriptionHideLoader: () => dispatch(actions.subscriptionHideLoader()),
  subscriptionGetUserSubscriptionDetails: (payload) => dispatch(actions.subscriptionGetUserSubscriptionDetails(payload)),
  subscriptionGetUserSubscriptionPlans: (payload) => dispatch(actions.subscriptionGetUserSubscriptionPlans(payload)),
  subscriptionBuySubscriptionPlan: payload => dispatch(actions.subscriptionBuySubscriptionPlan(payload))


});
export default connect(mapStateToProps, mapDispatchToProps)(Subscription);