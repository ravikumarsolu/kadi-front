import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import { NavLink } from 'react-router-dom';
import DeleteDialog from '../../../../components/routes/dialogs/delete/DeleteDialog';
import Switch from '@material-ui/core/Switch';
import AddStaffDialog from '../../../../components/routes/dialogs/addStaff/AddStaffDialog';

const actionsStyles = theme => ({
  root: {
    flexShrink: 0,
    color: theme.palette.text.secondary,
    marginLeft: theme.spacing.unit * 2.5,
  },
});

class TablePaginationActions extends React.Component {
  handleFirstPageButtonClick = event => {
    this.props.onChangePage(event, 0);
  };

  handleBackButtonClick = event => {
    this.props.onChangePage(event, this.props.page - 1);
  };

  handleNextButtonClick = event => {
    this.props.onChangePage(event, this.props.page + 1);
  };

  handleLastPageButtonClick = event => {
    this.props.onChangePage(
      event,
      Math.max(0, Math.ceil(this.props.count / this.props.rowsPerPage) - 1),
    );
  };


  render() {
    const { classes, count, page, rowsPerPage, theme, staffUsers } = this.props;

    return (
      <div className={classes.root}>
        <IconButton
          onClick={this.handleFirstPageButtonClick}
          disabled={page === 0}
          aria-label="First Page"
        >
          {theme.direction === 'rtl' ?
            <i className="zmdi zmdi-skip-next" /> : <i className="zmdi zmdi-skip-previous" />}
        </IconButton>
        <IconButton
          onClick={this.handleBackButtonClick}
          disabled={page === 0}
          aria-label="Previous Page"
        >
          {theme.direction === 'rtl' ?
            <i className="zmdi zmdi-chevron-right" /> : <i className="zmdi zmdi-chevron-left" />}
        </IconButton>
        <IconButton
          onClick={this.handleNextButtonClick}
          disabled={page >= Math.ceil(count / rowsPerPage) - 1}
          aria-label="Next Page"
        >
          {theme.direction === 'rtl' ?
            <i className="zmdi zmdi-chevron-left" /> : <i className="zmdi zmdi-chevron-right" />}
        </IconButton>
        <IconButton
          onClick={this.handleLastPageButtonClick}
          disabled={page >= Math.ceil(count / rowsPerPage) - 1}
          aria-label="Last Page"
        >
          {theme.direction === 'rtl' ?
            <i className="zmdi zmdi-skip-previous" /> : <i className="zmdi zmdi-skip-next" />}
        </IconButton>
      </div>
    );
  }
}

TablePaginationActions.propTypes = {
  classes: PropTypes.object.isRequired,
  count: PropTypes.number.isRequired,
  onChangePage: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
  theme: PropTypes.object.isRequired,
};

const TablePaginationActionsWrapped = withStyles(actionsStyles, { withTheme: true })(
  TablePaginationActions,
);

let counter = 0;

function createData(name, calories, fat) {
  counter += 1;
  return { id: counter, name, calories, fat };
}

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
  },
  table: {
    minWidth: 500,
  },
  tableWrapper: {
    overflowX: 'auto',
  },
});

class CustomTable extends React.Component {
  handleChangePage = (event, page) => {
    this.setState({ page });
  };
  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
  };

  constructor(props, context) {
    super(props, context);

    this.state = {
      page: 0,
      rowsPerPage: 5,
      checkedA: false,
      open: false,
      userToEdit: [],
    };
  }

  handleRequestClose = () => {
    this.setState({ open: false });
    this.props.staffUserClear();
  };
  
  render() {
    const { classes, staffUsers } = this.props;
    const { data, rowsPerPage, page } = this.state;
    const emptyRows = rowsPerPage - Math.min(rowsPerPage, staffUsers.total - page * rowsPerPage);

    return (
      <Paper className="customTable">
        <div className={classes.tableWrapper}>
          <Table className={classes.table}>
            {/* <Table className="customTable"> */}
            <TableHead>
              <TableRow>
                <TableCell>Name</TableCell>
                <TableCell>Contact Number</TableCell>
                <TableCell>Email ID</TableCell>
                <TableCell>No. of Devices</TableCell>
                <TableCell>Status</TableCell>
                <TableCell>Action</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {staffUsers ?
                staffUsers.customers != undefined ?
                  staffUsers.customers.map((item, index) => (
                    <TableRow key={index}>
                      <TableCell>{item.firstName} {item.lastName}</TableCell>
                      <TableCell>{item.contactNumber}</TableCell>
                      <TableCell>{item.email}</TableCell>
                      <TableCell>{item.devices != null ? item.devices.length : 0}</TableCell>
                      <TableCell>
                        <Switch
                          className="customSwitch"
                          classes={{
                            checked: 'text-success',
                            bar: 'bg-white',
                          }}
                          defaultChecked={item.status}
                          onChange={(e) => {
                            this.props.staffUserStatusChanged({ id: item.id, status: e.target.checked, index })
                          }}
                        />
                      </TableCell>
                      <TableCell>
                        <NavLink to={`/app/userinforamtion/${item.id}`}>
                          <IconButton className="icon-btn">
                            <i className="zmdi zmdi-eye" />
                          </IconButton>
                        </NavLink>
                        <IconButton className="icon-btn" >
                          <i className="zmdi zmdi-edit"
                            onClick={() => {
                              this.props.staffUserEditFillUp(item);
                              this.setState({ open: true, userToEdit: item });
                            }}

                          />
                        </IconButton>
                        <IconButton className="icon-btn">
                          <DeleteDialog
                            key={index}
                            deleteStaffUser={this.props.deleteStaffUser}
                            userId={item.id}
                            isStaff
                          />
                        </IconButton>
                      </TableCell>
                    </TableRow>
                  )) : null : null}

            </TableBody>
            {/* <TableFooter>
              <TableRow>
                <TablePagination
                  count={staffUsers ? staffUsers.total : 0}
                  rowsPerPage={rowsPerPage}
                  page={page}
                  onChangePage={this.handleChangePage}
                  onChangeRowsPerPage={this.handleChangeRowsPerPage}
                  ActionsComponent={TablePaginationActionsWrapped}
                />
              </TableRow>
            </TableFooter> */}
          </Table>
          {this.state.open ? <AddStaffDialog open={this.state.open}
            handleRequestClose={this.handleRequestClose}
            isEdit
            userToEdit={this.state.userToEdit}
          /> : null }
        </div>
      </Paper>
    );
  }
}

CustomTable.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(CustomTable);