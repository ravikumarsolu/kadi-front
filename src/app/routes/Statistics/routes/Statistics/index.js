import React, { Component, Suspense, lazy } from "react";
import { Nav, NavItem, NavLink } from 'reactstrap';
import classnames from 'classnames';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import ContainerHeader from "components/ContainerHeader/index";
import * as actions from '../../../../../actions';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import CircularProgress from '@material-ui/core/CircularProgress';

const TotalConsumptionsChart = lazy(() => import("../../../DeviceInforamtion/routes/DeviceInforamtion/totalConsumptionChart"));
const GridChart = lazy(() => import("../../../DeviceInforamtion/routes/DeviceInforamtion/gridChart"));
const SolarChart = lazy(() => import("../../../DeviceInforamtion/routes/DeviceInforamtion/solarChart"));
const GeneratorChart = lazy(() => import("../../../DeviceInforamtion/routes/DeviceInforamtion/generatorChart"));
const DeviceStatus = lazy(() => import("../../../DeviceInforamtion/routes/DeviceInforamtion/deviceStatus"));

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
let INTERVAL_ID;

class Statistics extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: '1',
      deviceName: 0,
      dataflag: false,

    };
  }

  componentWillMount() {
    this.props.dashboardGetAllDevices();

  }

  handleDeviceChange = (item) => {
    const { allDevices } = this.props
    this.setState({ deviceName: item.target.value });
    this.getData(allDevices[item.target.value].deviceIMEI);
  }
  getData = (ID) => {
    console.log("TCL: Statistics -> getData -> ID", ID)
    const { allDevices } = this.props
    if (allDevices.length > 0) {
      clearInterval(INTERVAL_ID);
      INTERVAL_ID = setInterval(() => {
        this.props.dashboardDashboardDeviceChanged({ id: ID, trend: "current" });
      }, 10000);
      this.props.dashboardDashboardDeviceChanged({ id: ID, trend: "current" });
      this.props.dashboardDashboardDeviceChanged({ id: ID, trend: "daily" });
      this.props.dashboardDashboardDeviceChanged({ id: ID, trend: "weekly" });
      this.props.dashboardDashboardDeviceChanged({ id: ID, trend: "monthly" });
      this.props.dashboardDashboardDeviceChanged({ id: ID, trend: "quarterly" });
      this.props.dashboardDashboardDeviceChanged({ id: ID, trend: "yearly" });
    }
  }
  getDeviceStatistics = () => {
    const { dashboardDeviceDailyStatistics, dashboardDeviceWeeklyStatistics, dashboardDeviceMonthlyStatistics, dashboardDeviceYearlyStatistics, dashboardDeviceQuarterlyStatistics } = this.props;
    const { activeTab } = this.state;
    let data = [];
    if (activeTab === "1") {
      dashboardDeviceDailyStatistics.map((item, index) => {
        data.push({ Solar: item.powerSummation.solarPowerSum == null ? 0 : item.powerSummation.solarPowerSum, Grid: item.powerSummation.gridPowerSum == null ? 0 : item.powerSummation.gridPowerSum, Generator: item.powerSummation.generatorPowerSum == null ? 0 : item.powerSummation.generatorPowerSum });
      });
    }
    //  else if (activeTab === "2") {
    //   dashboardDeviceWeeklyStatistics.map((item, index) => {
    //     data.push({ Solar: item.powerSummation.solarPowerSum == null ? 0 : item.powerSummation.solarPowerSum, Grid: item.powerSummation.gridPowerSum == null ? 0 : item.powerSummation.gridPowerSum, Generator: item.powerSummation.generatorPowerSum == null ? 0 : item.powerSummation.generatorPowerSum });
    //   });
    // }
    else if (activeTab === "2") {
      dashboardDeviceMonthlyStatistics.map((item, index) => {
        data.push({ Solar: item.powerSummation.solarPowerSum == null ? 0 : item.powerSummation.solarPowerSum, Grid: item.powerSummation.gridPowerSum == null ? 0 : item.powerSummation.gridPowerSum, Generator: item.powerSummation.generatorPowerSum == null ? 0 : item.powerSummation.generatorPowerSum });
      });
    }
    else if (activeTab == "3") {
      dashboardDeviceQuarterlyStatistics.map((item, index) => {
        data.push({ Solar: item.powerSummation.solarPowerSum == null ? 0 : item.powerSummation.solarPowerSum, Grid: item.powerSummation.gridPowerSum == null ? 0 : item.powerSummation.gridPowerSum, Generator: item.powerSummation.generatorPowerSum == null ? 0 : item.powerSummation.generatorPowerSum });
      });
    }

    else if (activeTab === "4") {
      dashboardDeviceYearlyStatistics.map((item, index) => {
        data.push({ Solar: item.powerSummation.solarPowerSum == null ? 0 : item.powerSummation.solarPowerSum, Grid: item.powerSummation.gridPowerSum == null ? 0 : item.powerSummation.gridPowerSum, Generator: item.powerSummation.generatorPowerSum == null ? 0 : item.powerSummation.generatorPowerSum });
      });
    }
    return data;
  }
  toggle = (tab) => {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }
  componentWillUnmount() {
    clearInterval(INTERVAL_ID);
  }
  render() {
    const { match, allDevices, dashboardDeviceCurrentStatus } = this.props;
    const { deviceName, dataflag } = this.state;
    if (allDevices.length != 0 && !dataflag) {
      this.getData(allDevices[0].deviceIMEI);
      this.setState({ dataflag: true });
    }
    return (
      <div className="statistics animated slideInUpTiny animation-duration-3">

        <ContainerHeader match={match} title={"Statistics"} />

        <div className="row accountsetting">
          <div className="col-12">
            <div className="jr-card card">
              <div className="row">
                <div className="col-xl-6 col-lg-6 col-md-12 col-12">
                  <FormControl className="w-100 mb-2">
                    <Select
                      className="customSelect mt-0 my-0"

                      // defaultValue={deviceName}
                      value={deviceName}
                      onChange={this.handleDeviceChange}
                      // input={<Input id="name-multiple" />}
                      MenuProps={{
                        PaperProps: {
                          style: {
                            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
                            width: 50,
                          },
                        },
                      }}
                    >
                      {allDevices ? allDevices.map((item, index) => (
                        <MenuItem
                          key={index}
                          value={index}
                        // style={{
                        //     fontWeight: this.state.name.indexOf(item.deviceIMEI) !== -1 ? '500' : '400',
                        // }}
                        >
                          {item.deviceIMEI}
                        </MenuItem>
                      )) : null}
                    </Select>

                  </FormControl>
                </div>
                <div className="col-12">
                  <div className="row">
                    <div className="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-6 state">
                      <h5>Total Uptime</h5>
                      <h3>12 hrs.</h3>
                    </div>
                    <div className="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-6 state">
                      <h5>Grid Quality</h5>
                      <h3>Good</h3>
                    </div>
                    <div className="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-6 state">
                      <h5>Device Health</h5>
                      <h3>Average</h3>
                    </div>
                  </div>
                </div>
              </div>

            </div>
            <div className="jr-card card">
              <div className="row">
                <div className="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                  <h3 className="text-white">Solar Power</h3>
                  <ul class="profiledetail">
                    <li>
                      <label>Current</label>
                      <span>{dashboardDeviceCurrentStatus.solarCurrent != undefined ? dashboardDeviceCurrentStatus.solarCurrent + " (A)" : "NA"}</span>
                    </li>
                    <li>
                      <label>Voltage</label>
                      <span>{dashboardDeviceCurrentStatus.solarVoltage != undefined ? dashboardDeviceCurrentStatus.solarVoltage + " (V)" : "NA"}</span>
                    </li>
                    <li>
                      <label>Power</label>
                      <span>{dashboardDeviceCurrentStatus.solarPower != undefined ? dashboardDeviceCurrentStatus.solarPower + " KV." : "NA"}</span>
                    </li>
                  </ul>
                </div>
                <div className="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                  <h3 className="text-white">AC Generator</h3>
                  <ul class="profiledetail">
                    <li>
                      <label>Current</label>
                      <span>{dashboardDeviceCurrentStatus.generatorCurrent != undefined ? dashboardDeviceCurrentStatus.generatorCurrent + " (A)" : "NA"}</span>
                    </li>
                    <li>
                      <label>Voltage</label>
                      <span>{dashboardDeviceCurrentStatus.generatorVoltage != undefined ? dashboardDeviceCurrentStatus.generatorVoltage + " (V)" : "NA"}</span>
                    </li>
                    <li>
                      <label>Power</label>
                      <span>{dashboardDeviceCurrentStatus.generatorPower != undefined ? dashboardDeviceCurrentStatus.generatorPower + " KV." : "NA"}</span>
                    </li>
                    <li>
                      <label>Fuel Level</label>
                      <span>25%</span>
                    </li>
                  </ul>
                </div>
                <div className="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                  <h3 className="text-white">Grid Power</h3>
                  <ul class="profiledetail">
                    <li>
                      <label>Current</label>
                      <span>{dashboardDeviceCurrentStatus.gridCurrent != undefined ? dashboardDeviceCurrentStatus.gridCurrent + " (A)" : "NA"}</span>
                    </li>
                    <li>
                      <label>Voltage</label>
                      <span>{dashboardDeviceCurrentStatus.gridVoltage != undefined ? dashboardDeviceCurrentStatus.gridVoltage + " (V)" : "NA"}</span>
                    </li>
                    <li>
                      <label>Power</label>
                      <span>{dashboardDeviceCurrentStatus.gridPower != undefined ? dashboardDeviceCurrentStatus.gridPower + " KV." : "NA"}</span>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <Suspense fallback={<div><CircularProgress /></div>}>
              <DeviceStatus deviceCurrentStatus={dashboardDeviceCurrentStatus} />
            </Suspense>
            <div className="jr-card card">
              <div className="row">
                <div className="col-xl-6 col-lg-6 col-md-12 mb-3">
                  <Nav className="jr-tabs-pills-ctr nav-justified" pills>
                    <NavItem>
                      <NavLink
                        className={classnames({ active: this.state.activeTab === '1' })}
                        onClick={() => {
                          this.toggle('1');
                        }}>
                        Daily
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                        className={classnames({ active: this.state.activeTab === '2' })}
                        onClick={() => {
                          this.toggle('2');
                        }}
                      >
                        Monthly
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                        className={classnames({ active: this.state.activeTab === '3' })}
                        onClick={() => {
                          this.toggle('3');
                        }}>
                        Quarterly
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                        className={classnames({ active: this.state.activeTab === '4' })}
                        onClick={() => {
                          this.toggle('4');
                        }}>
                        Yearly
                      </NavLink>
                    </NavItem>
                  </Nav>
                </div>
                <div className="col-xl-6 col-lg-6 col-md-12 mb-3"></div>
                <div className="col-xl-6 col-lg-6 col-md-12 mb-3">
                  <h2 className="text-white mb-4">Total Consumptions</h2>
                  <Suspense fallback={<div><CircularProgress /></div>}>
                    <TotalConsumptionsChart getDeviceStatistics={this.getDeviceStatistics} />
                  </Suspense>

                </div>
                <div className="col-xl-6 col-lg-6 col-md-12 mb-3">

                  <h2 className="text-white mb-4">Solar</h2>
                  <Suspense fallback={<div><CircularProgress /></div>}>
                    <SolarChart getDeviceStatistics={this.getDeviceStatistics} />
                  </Suspense>
                </div>
                <div className="col-xl-6 col-lg-6 col-md-12 mb-3">
                  <h2 className="text-white mb-4">Grid</h2>
                  <Suspense fallback={<div><CircularProgress /></div>}>
                    <GridChart getDeviceStatistics={this.getDeviceStatistics} />
                  </Suspense>
                </div>
                <div className="col-xl-6 col-lg-6 col-md-12 mb-3">
                  <h2 className="text-white mb-4">Generator</h2>
                  <Suspense fallback={<div><CircularProgress /></div>}>
                    <GeneratorChart getDeviceStatistics={this.getDeviceStatistics} />
                  </Suspense>
                </div>
              </div>
            </div>

          </div>
        </div>

      </div>
    );
  }
}



const mapStateToProps = ({ dashboard }) => {
  const { loader, allDevices, dashboardDeviceCurrentStatus,
    dashboardDeviceDailyStatistics,
    dashboardDeviceWeeklyStatistics,
    dashboardDeviceMonthlyStatistics,
    dashboardDeviceYearlyStatistics,
    dashboardDeviceQuarterlyStatistics } = dashboard;
  return {
    loader, allDevices, dashboardDeviceCurrentStatus,
    dashboardDeviceDailyStatistics,
    dashboardDeviceWeeklyStatistics,
    dashboardDeviceMonthlyStatistics,
    dashboardDeviceYearlyStatistics,
    dashboardDeviceQuarterlyStatistics
  }
};
const mapDispatchToProps = dispatch => ({
  dashboardDashboardDeviceChanged: payload => dispatch(actions.dashboardDashboardDeviceChanged(payload)),
  dashboardGetAllDevices: () => dispatch(actions.dashboardGetAllDevices())
});
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Statistics));