import React from 'react';
import {Redirect, Route, Switch} from 'react-router-dom';

import asyncComponent from '../../../util/asyncComponent';

const AppModule = ({match}) => (
  <div className="app-wrapper h-100">
    <Switch>
      {/* <Redirect exact from={`${match.url}/`} to={`${match.url}/login-1`}/> */}
      <Route path={`${match.url}`} component={asyncComponent(() => import('./routes/AccountSettings'))}/>
    </Switch>
  </div>
);

export default AppModule;
