import React, { Component } from "react";
import ContainerHeader from "components/ContainerHeader/index";
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import * as actions from '../../../../../actions';
import Input from '@material-ui/core/Input';
import { toast } from "react-toastify";
import CircularProgress from '@material-ui/core/CircularProgress';


const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
class AccountSetting extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: '1',
      deviceIMEI: "Select Device",
      open: false
    };
  }
  handleChangePassword = e => {
    const { currentPassword, newPassword, confirmPassword } = this.props;
    if (currentPassword == "") {
      toast.error("Current password can't be blank", {
        toastId: "offlinePayment",
        hideProgressBar: true,
        autoClose: 3000,
        position: toast.POSITION.TOP_RIGHT
      });

    } else if (!currentPassword.replace(/\s/g, '').length) {
      toast.error("Current password can't be blank", {
        toastId: "offlinePayment",
        hideProgressBar: true,
        autoClose: 3000,
        position: toast.POSITION.TOP_RIGHT
      });
    } else if (newPassword == "") {
      toast.error("New password can't be blank", {
        toastId: "offlinePayment",
        hideProgressBar: true,
        autoClose: 3000,
        position: toast.POSITION.TOP_RIGHT
      });
    } else if (!newPassword.replace(/\s/g, '').length) {
      toast.error("New password can't be blank", {
        toastId: "offlinePayment",
        hideProgressBar: true,
        autoClose: 3000,
        position: toast.POSITION.TOP_RIGHT
      });
    } else if (confirmPassword == "") {
      toast.error("Confirm password can't be blank", {
        toastId: "offlinePayment",
        hideProgressBar: true,
        autoClose: 3000,
        position: toast.POSITION.TOP_RIGHT
      });
    } else if (!confirmPassword.replace(/\s/g, '').length) {
      toast.error("Confirm password can't be blank", {
        toastId: "offlinePayment",
        hideProgressBar: true,
        autoClose: 3000,
        position: toast.POSITION.TOP_RIGHT
      });
    } else if (newPassword != confirmPassword) {
      toast.error("New Password and Confirm new Password doesn't match", {
        toastId: "offlinePayment",
        hideProgressBar: true,
        autoClose: 3000,
        position: toast.POSITION.TOP_RIGHT
      });
    } else {
      this.props.accountChangePasswordRequest({ oldPassword: currentPassword, newPassword, confirmPassword })
    }
  }
  handleHelpSubmit = e => {
    const { helpDescription, accountHelpSupportRequest } = this.props;
    if (this.state.deviceIMEI == "Select Device") {

      toast.error("Please select device", {
        toastId: "offlinePayment",
        hideProgressBar: true,
        autoClose: 3000,
        position: toast.POSITION.TOP_RIGHT
      });

    }
    else if (helpDescription == "") {
      toast.error("Please enter description", {
        toastId: "offlinePayment",
        hideProgressBar: true,
        autoClose: 3000,
        position: toast.POSITION.TOP_RIGHT
      });
    }
    else {
      let customerId = localStorage.getItem("user_id");
      accountHelpSupportRequest({
        device: this.state.deviceIMEI,
        customerId,
        message: helpDescription
      });
    }
  }
  handleChange = event => {
    this.setState({ deviceIMEI: event.target.value });

  };
  componentWillMount() {
    this.props.accountGetAllDevices();
  }
  toggle = (tab) => {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }

  render() {
    const { deviceIMEI, password } = this.state;
    const { match, loader, allDevices, helpDescription, helpLoader, currentPassword, newPassword, confirmPassword, changePasswordLoader } = this.props;
    let userRole = localStorage.getItem("role");

    return (
      <div className="dashboard animated slideInUpTiny animation-duration-3">

        <ContainerHeader match={match} title={"Account Settings"} />


        <div className="row accountsetting">

          <div className="col-12">

            <div className="jr-card card">
              <div className="d-flex flex-row flex-wrap align-items-center justify-content-between userhead">
                <div className="d-flex flex-row align-items-center justify-content-between">
                  <h2 className="text-white">My Profile</h2>
                  <Button className="customButton d-block d-md-none d-lg-none d-xl-none" variant="contained">Edit</Button>
                </div>
                <div className="d-flex flex-row flex-wrap">
                  <Button className="customButton d-none d-md-block d-lg-block d-xl-block" variant="contained">Edit</Button>
                </div>
              </div>
              <div className="row">
                <div className="col-12 col-sm-6">

                  <ul className="profiledetail">
                    <li>
                      <label>Full Name</label>
                      <span>Jack Smith</span>
                    </li>
                    <li>
                      <label>Contact Number</label>
                      <span>0123456789</span>
                    </li>
                    <li>
                      <label>Email ID</label>
                      <span>jack@gmail.com</span>
                    </li>
                    <li>
                      <label>Company Name</label>
                      <span>Kadi Energy Company</span>
                    </li>
                  </ul>

                  {/* <FormControl className="w-100 mb-2">
                  <Select className="customSelect"
                    value="10"
                  >
                    <MenuItem value="">
                      <em>None</em>
                    </MenuItem>
                    <MenuItem value={10}>Device 1</MenuItem>
                    <MenuItem value={20}>Device 2</MenuItem>
                    <MenuItem value={30}>Device 3</MenuItem>
                  </Select>
                </FormControl> */}
                </div>
                <div className="col-12 col-sm-6">
                  <ul className="profiledetail">
                    <li>
                      <label>Company Location</label>
                      <span>Quincy, MA 02169, United St</span>
                    </li>
                    <li>
                      <label>Total Devices</label>
                      <span>6</span>
                    </li>
                    {userRole == "admin" ? <li>
                      <label>Total Staff Users</label>
                      <span>6</span>
                    </li> : null}
                  </ul>
                </div>
              </div>
            </div>
            <div className="jr-card card">
              <div className="d-flex flex-row">
                <h2 className="mb-3 text-white">Change Password</h2>
              </div>
              <div className="row">
                <div className="col-12 col-md-9 col-lg-9 col-xl-6">

                  <ul className="profiledetail mb-4">
                    <li className="align-items-center">
                      <label>Current Password</label>
                      <div className="formgroup">
                        <TextField
                          type="password"
                          placeholder="Enter Current Password"
                          fullWidth
                          onChange={(event) => this.props.accountCurrentPasswordChanged(event.target.value)}
                          value={currentPassword}
                          margin="normal"
                          className="mt-0 my-0 custominput w-100"
                        />
                      </div>
                    </li>
                    <li className="align-items-center">
                      <label>New Password</label>
                      <div className="formgroup">
                        <TextField
                          type="password"
                          placeholder="Enter New Password"
                          fullWidth
                          onChange={(event) => this.props.accountNewPasswordChanged(event.target.value)}
                          value={newPassword}
                          margin="normal"
                          className="mt-0 my-0 custominput w-100"
                        />
                      </div>
                    </li>
                    <li className="align-items-center">
                      <label>Confirm Password</label>
                      <div className="formgroup">
                        <TextField
                          type="password"
                          placeholder="Enter Confirm Password"
                          fullWidth
                          onChange={(event) => this.props.accountConfirmPasswordChanged(event.target.value)}
                          value={confirmPassword}
                          margin="normal"
                          className="mt-0 my-0 custominput w-100"
                        />
                      </div>
                    </li>
                  </ul>
                  {changePasswordLoader ? (<div className="loader-view">
                    <CircularProgress />
                  </div>) :
                    <div className="btngroup">
                      <Button className="customButton" variant="contained"
                        onClick={this.handleChangePassword}>
                        Update
                  </Button>

                      <Button
                        className="customButton lineButton"
                        variant="contained"
                        onClick={() => { this.props.accountClearChangePassword() }}
                      >
                        Clear
                  </Button>
                    </div>
                  }
                </div>
              </div>
            </div>
            {userRole == "admin" ?
              <div className="jr-card card">
                <div className="d-block">
                  <h2 className="mb-3 text-white w-100">Help & Support</h2>
                  <h4 className="mb-3 text-white w-100">Suggest Operational Change?</h4>
                </div>
                <div className="row">
                  <div className="col-12 col-md-7 col-lg-7 col-xl-4">

                    <FormControl className="w-100 mb-2">
                      <Select
                        className="customSelect mt-0 my-0"
                        value={deviceIMEI}
                        onChange={this.handleChange}
                        input={<Input id="name-multiple" />}
                        MenuProps={{
                          PaperProps: {
                            style: {
                              maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
                              width: 50,
                            },
                          },
                        }}
                      >
                        <MenuItem value="Select Device" disabled>
                          {"Select Device"}
                        </MenuItem>
                        {allDevices.length > 0 ? allDevices.map((item, index) => (
                          <MenuItem
                            key={index}
                            value={item.deviceIMEI}
                            style={{
                              fontWeight: deviceIMEI.indexOf(item.deviceIMEI) !== -1 ? '500' : '400',
                            }}
                          >
                            {item.deviceIMEI}
                          </MenuItem>
                        )) : <MenuItem disabled>No Device Available</MenuItem>}

                      </Select>
                    </FormControl>


                    <div className="formgroup textarea">
                      <TextField
                        type="text"
                        placeholder="Enter Description"
                        multiline
                        rowsMax="4"
                        fullWidth
                        value={helpDescription}
                        onChange={(event) => this.props.accountHelpSupportDescriptionChanged(event.target.value)}
                        defaultValue={password}
                        margin="normal"
                        className="mt-0 my-0 custominput w-100"
                      />
                    </div>
                    {helpLoader ? (<div className="loader-view">
                      <CircularProgress />
                    </div>) :
                      <Button
                        className="customButton mt-2"
                        variant="contained"
                        onClick={this.handleHelpSubmit}
                      >
                        Send
                        </Button>}

                  </div>
                </div>
              </div>
              : null}

          </div>

        </div>

      </div>
    );
  }
}


const mapStateToProps = ({ account }) => {
  const { loader, allDevices, helpDescription, helpLoader, currentPassword, newPassword, confirmPassword, changePasswordLoader } = account;
  return { loader, allDevices, helpDescription, helpLoader, currentPassword, newPassword, confirmPassword, changePasswordLoader }
};
const mapDispatchToProps = dispatch => ({
  accountGetAllDevices: () => dispatch(actions.accountGetAllDevices()),
  accountHelpSupportDescriptionChanged: payload => dispatch(actions.accountHelpSupportDescriptionChanged(payload)),
  accountHelpSupportRequest: payload => dispatch(actions.accountHelpSupportRequest(payload)),
  accountCurrentPasswordChanged: payload => dispatch(actions.accountCurrentPasswordChanged(payload)),
  accountNewPasswordChanged: payload => dispatch(actions.accountNewPasswordChanged(payload)),
  accountConfirmPasswordChanged: payload => dispatch(actions.accountConfirmPasswordChanged(payload)),
  accountClearChangePassword: () => dispatch(actions.accountClearChangePassword()),
  accountChangePasswordRequest: payload => dispatch(actions.accountChangePasswordRequest(payload))
});
export default connect(mapStateToProps, mapDispatchToProps)(AccountSetting);