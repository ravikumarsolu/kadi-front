import React from 'react';
import Thermometer from 'react-thermometer-component';
import PieChart from 'react-minimal-pie-chart';
const DeviceStatus = (props) => {
    return(
        
        <div className="jr-card card">
        <div className="d-flex flex-row">
          <h2 className="mb-3 text-white">Device Status</h2>
        </div>
        {props.deviceCurrentStatus != undefined ?
          <div className="row devicestate">
            <div className="col-xl-2 col-lg-4 col-md-4 col-6 piechart">
              <PieChart
                animate
                animationDuration={2000}
                animationEasing="ease-in"
                background="#272d49"
                cx={50}
                cy={50}
                data={[
                  {
                    color: '#fa9f1b',
                    value: props.deviceCurrentStatus.batteryCurrent
                  }
                ]}
                label
                labelPosition={0}
                labelStyle={{
                  fontFamily: 'sans-serif',
                  fontSize: '25px'
                }}
                lengthAngle={360}
                lineWidth={10}
                onClick={undefined}
                onMouseOut={undefined}
                onMouseOver={undefined}
                paddingAngle={0}
                radius={50}
                ratio={1}
                rounded
                startAngle={270}
                totalValue={100}
                style={{
                  height: '120px'
                }}
              />
              <span className="d-block text-center text-white">Current (A)</span>
            </div>
            <div className="col-xl-2 col-lg-4 col-md-4 col-6 piechart">
              <PieChart
                animate
                animationDuration={2000}
                animationEasing="ease-in"
                background="#272d49"
                cx={50}
                cy={50}
                data={[
                  {
                    color: '#fa9f1b',
                    value: props.deviceCurrentStatus.batteryVoltage
                  }
                ]}
                label
                labelPosition={0}
                labelStyle={{
                  fontFamily: 'sans-serif',
                  fontSize: '25px'
                }}
                lengthAngle={360}
                lineWidth={10}
                onClick={undefined}
                onMouseOut={undefined}
                onMouseOver={undefined}
                paddingAngle={0}
                radius={50}
                ratio={1}
                rounded
                startAngle={270}
                totalValue={100}
                style={{
                  height: '120px'
                }}
              />
              <span className="d-block text-center text-white">Voltage (V)</span>
            </div>
            <div className="col-xl-2 col-lg-4 col-md-4 col-6 piechart">
              <PieChart
                animate
                animationDuration={2000}
                animationEasing="ease-in"
                background="#272d49"
                cx={50}
                cy={50}
                data={[
                  {
                    color: '#fa9f1b',
                    value: props.deviceCurrentStatus.stateOfChargeBattery
                  }
                ]}
                label
                labelPosition={0}
                labelStyle={{
                  fontFamily: 'sans-serif',
                  fontSize: '25px'
                }}
                lengthAngle={360}
                lineWidth={10}
                onClick={undefined}
                onMouseOut={undefined}
                onMouseOver={undefined}
                paddingAngle={0}
                radius={50}
                ratio={1}
                rounded
                startAngle={270}
                totalValue={100}
                style={{
                  height: '120px'
                }}
              />
              <span className="d-block text-center text-white">State of Charge (%)</span>
            </div>
            <div className="col-xl-2 col-lg-4 col-md-4 col-6 piechart">
              <PieChart
                animate
                animationDuration={2000}
                animationEasing="ease-out"
                background="#272d49"
                cx={50}
                cy={50}
                data={[
                  {
                    color: '#fa9f1b',
                    value: props.deviceCurrentStatus.avgToFullBattery
                  }
                ]}
                label
                labelPosition={0}
                labelStyle={{
                  fontFamily: 'sans-serif',
                  fontSize: '25px'
                }}
                lengthAngle={360}
                lineWidth={10}
                onClick={undefined}
                onMouseOut={undefined}
                onMouseOver={undefined}
                paddingAngle={0}
                radius={50}
                ratio={1}
                rounded
                startAngle={270}
                totalValue={100}
                style={{
                  height: '120px'
                }}
              />
              <span className="d-block text-center text-white">Average Time to Full (min)</span>
            </div>
            <div className="col-xl-2 col-lg-4 col-md-4 col-6 piechart">
              <PieChart
                animate
                animationDuration={2000}
                animationEasing="ease-out"
                background="#272d49"
                cx={50}
                cy={50}
                data={[
                  {
                    color: '#fa9f1b',
                    value: props.deviceCurrentStatus.avgToEmptyBattery
                  }
                ]}
                label
                labelPosition={0}
                labelStyle={{
                  fontFamily: 'sans-serif',
                  fontSize: '25px'
                }}
                lengthAngle={360}
                lineWidth={10}
                onClick={undefined}
                onMouseOut={undefined}
                onMouseOver={undefined}
                paddingAngle={0}
                radius={50}
                ratio={1}
                rounded
                startAngle={270}
                totalValue={100}
                style={{
                  height: '120px'
                }}
              />
              <span className="d-block text-center text-white">Average Time to Empty (min)</span>
            </div>
            <div className="col-xl-2 col-lg-4 col-md-4 col-6">
              <Thermometer
                theme="light"
                value={props.deviceCurrentStatus.temperature}
                format="°C"
                size="small"
                height="120"
              />
              <span className="d-block text-center text-white">Temperature</span>
            </div>
          </div>
          : "Device staus not available"}

      </div>
    );
}

export default DeviceStatus;