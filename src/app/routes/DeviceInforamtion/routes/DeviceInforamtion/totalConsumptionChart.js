import React from 'react';
import { Bar, BarChart, CartesianGrid, Legend, Line, LineChart, ResponsiveContainer, Tooltip, XAxis, YAxis } from 'recharts';
const TotalConsumptionsChart = (props) => {

    return (
        <ResponsiveContainer width="100%" height={200}>
            <LineChart
                data={props.getDeviceStatistics()}
                margin={{ top: 10, right: 0, left: 5, bottom: 0 }}>
                <XAxis dataKey="name" />
                <YAxis />
                {/* <CartesianGrid strokeDasharray="3 3" /> */}
                <Tooltip />
                <Legend />
                <Line type="monotone" dataKey="Solar" stroke="#23c0a1" legendType="round"  dot={false}/>
                <Line type="monotone" dataKey="Grid" stroke="#4db103" legendType="round"  dot={false}/>
                <Line type="monotone" dataKey="Generator" stroke="#f8a01b" legendType="round"  dot={false} />
            </LineChart>
        </ResponsiveContainer>
    );
}

export default TotalConsumptionsChart;