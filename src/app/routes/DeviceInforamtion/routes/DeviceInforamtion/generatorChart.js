import React from 'react';
import { Bar, BarChart, Legend, ResponsiveContainer, Tooltip, XAxis, YAxis } from 'recharts';
const GeneratorChart = (props) => {

    return (
        <ResponsiveContainer width="100%" height={200}>
            <BarChart
                data={props.getDeviceStatistics()}
                margin={{ top: 10, right: 0, left: 5, bottom: 0 }}>
                <XAxis dataKey="name" />
                <YAxis />
                  <Tooltip cursor={false}/>
                <Legend />
                <XAxis dataKey="name" />
                <YAxis />
                  <Tooltip cursor={false}/>
                <Legend />
                <Bar dataKey="Generator" fill="#f8a01b" legendType="round" />
            </BarChart>
        </ResponsiveContainer>
    );
}

export default GeneratorChart;