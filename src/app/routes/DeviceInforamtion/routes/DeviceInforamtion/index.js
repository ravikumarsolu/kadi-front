import React, { Component, Suspense, lazy } from "react";
import { Nav, NavItem, NavLink, TabContent, TabPane } from 'reactstrap';
import classnames from 'classnames';
import Button from '@material-ui/core/Button';
import ContainerHeader from "components/ContainerHeader/index";
import { DatePicker } from 'material-ui-pickers'
import { connect } from 'react-redux';
import * as actions from '../../../../../actions';
import CircularProgress from '@material-ui/core/CircularProgress';
import DeviceStatus from "./deviceStatus";
const TotalConsumptionsChart = lazy(() => import('./totalConsumptionChart'));
const SolarChart = lazy(() => import('./solarChart'));
const GridChart = lazy(() => import('./gridChart'));
const GeneratorChart = lazy(() => import('./generatorChart'));
const TotalCostChart = lazy(() => import('./totalCostChart'));
let INTERVAL_ID;

class DeviceInformation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: '1',
      from: new Date(),
      to: new Date(),
      customDates: false
    };
  }

  componentWillMount() {
    let userRole = localStorage.getItem("role");
    let id = userRole == "admin" ? window.location.href.split("/")[5] : window.location.href.split("/")[6];

    this.props.deviceShowLoader();
    this.props.deviceGetDeviceRequest(id);
    this.props.deviceGetDeviceStatistics({ id: id, trend: "current" });
    this.props.deviceGetDeviceStatistics({ id: id, trend: "daily" });
    this.props.deviceGetDeviceStatistics({ id: id, trend: "weekly" });
    this.props.deviceGetDeviceStatistics({ id: id, trend: "monthly" }); 
    this.props.deviceGetDeviceStatistics({ id: id, trend: "yearly" });
    INTERVAL_ID = setInterval(() => {
      this.props.deviceGetDeviceStatistics({ id: id, trend: "current" });
    }, 10000);

  }

  componentWillUnmount() {
    this.props.deviceDeviceClear();
    clearInterval(INTERVAL_ID);
  }

  toggle = (tab) => {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab,
        customDates: false
      });
    }
  }

  handleDateChange = (origin, value) => {
    if (origin == "TO") {
      this.setState({ to: value, customDates: true });
    }
    else {
      this.setState({ from: value, customDates: true });
    }

  }

  handleCustomRequest = () => {
    const { from, to } = this.state;
    this.props.deviceGetDeviceStatistics({ id: window.location.href.split("/")[5], trend: "custom", from: from, to: to });
    this.setState({ activeTab: '0' })
  }
  deviceStatistics = () => {
    const { batteryVoltage, batteryCurrent, fuelStatus, gridQuality, totalUptime, fault, healthStatus } = this.props.deviceCurrentStatus;
    return (
      <div className="row">
        <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
          <ul className="profiledetail">
            <li>
              <label>Available Voltage (V)</label>
              <span>{batteryVoltage ? batteryVoltage : 0}</span>
            </li>
            <li>
              <label>Available Current (A)</label>
              <span>{batteryCurrent ? batteryCurrent : 0}</span>
            </li>
            <li>
              <label>Fault</label>
              <span>{fault ? fault : "NA"}</span>
            </li>
            <li>
              <label>Total Uptime</label>
              <span>{totalUptime ? totalUptime : 0} Hrs.</span>
            </li>
          </ul>
        </div>
        <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
          <ul className="profiledetail">
            <li>
              <label>Health Status</label>
              <span>{healthStatus ? healthStatus : "NA"}</span>
            </li>
            <li>
              <label>Fuel Status</label>
              <span>{fuelStatus ? fuelStatus : "NA"}</span>
            </li>
            <li>
              <label>Grid Quality</label>
              <span>{gridQuality ? gridQuality : "NA"}</span>
            </li>
          </ul>
        </div>
      </div>)
  }

  getDeviceStatistics = () => {
    const { deviceDailyStatistics, deviceWeeklyStatistics, deviceMonthlyStatistics, deviceYearlyStatistics, deviceCustomStatistics } = this.props;
    const { activeTab } = this.state;
    let data = [];
    if (activeTab === "1") {
      deviceDailyStatistics.map((item, index) => {
        data.push({ Solar: item.powerSummation.solarPowerSum == null ? 0 : item.powerSummation.solarPowerSum, Grid: item.powerSummation.gridPowerSum == null ? 0 : item.powerSummation.gridPowerSum, Generator: item.powerSummation.generatorPowerSum == null ? 0 : item.powerSummation.generatorPowerSum, solarTotal: item.powerSummation.solarPowerSum == null ? 0 : item.powerSummation.solarPowerSum * 11, gridTotal: item.powerSummation.gridPowerSum == null ? 0 : item.powerSummation.gridPowerSum * 11, generatorTotal: item.powerSummation.generatorPowerSum == null ? 0 : item.powerSummation.generatorPowerSum * 11 });
      });
    } else if (activeTab === "2") {
      deviceWeeklyStatistics.map((item, index) => {
        data.push({ Solar: item.powerSummation.solarPowerSum == null ? 0 : item.powerSummation.solarPowerSum, Grid: item.powerSummation.gridPowerSum == null ? 0 : item.powerSummation.gridPowerSum, Generator: item.powerSummation.generatorPowerSum == null ? 0 : item.powerSummation.generatorPowerSum, solarTotal: item.powerSummation.solarPowerSum == null ? 0 : item.powerSummation.solarPowerSum * 11, gridTotal: item.powerSummation.gridPowerSum == null ? 0 : item.powerSummation.gridPowerSum * 11, generatorTotal: item.powerSummation.generatorPowerSum == null ? 0 : item.powerSummation.generatorPowerSum * 11 });
      });
    } else if (activeTab === "3") {
      deviceMonthlyStatistics.map((item, index) => {
        data.push({ Solar: item.powerSummation.solarPowerSum == null ? 0 : item.powerSummation.solarPowerSum, Grid: item.powerSummation.gridPowerSum == null ? 0 : item.powerSummation.gridPowerSum, Generator: item.powerSummation.generatorPowerSum == null ? 0 : item.powerSummation.generatorPowerSum, solarTotal: item.powerSummation.solarPowerSum == null ? 0 : item.powerSummation.solarPowerSum * 11, gridTotal: item.powerSummation.gridPowerSum == null ? 0 : item.powerSummation.gridPowerSum * 11, generatorTotal: item.powerSummation.generatorPowerSum == null ? 0 : item.powerSummation.generatorPowerSum * 11 });
      });
    }
    else if (activeTab === "4") {
      deviceYearlyStatistics.map((item, index) => {
        data.push({ Solar: item.powerSummation.solarPowerSum == null ? 0 : item.powerSummation.solarPowerSum, Grid: item.powerSummation.gridPowerSum == null ? 0 : item.powerSummation.gridPowerSum, Generator: item.powerSummation.generatorPowerSum == null ? 0 : item.powerSummation.generatorPowerSum, solarTotal: item.powerSummation.solarPowerSum == null ? 0 : item.powerSummation.solarPowerSum * 11, gridTotal: item.powerSummation.gridPowerSum == null ? 0 : item.powerSummation.gridPowerSum * 11, generatorTotal: item.powerSummation.generatorPowerSum == null ? 0 : item.powerSummation.generatorPowerSum * 11 });
      });
    }
    else {
      deviceCustomStatistics.map((item, index) => {
        data.push({ Solar: item.powerSummation.solarPowerSum == null ? 0 : item.powerSummation.solarPowerSum, Grid: item.powerSummation.gridPowerSum == null ? 0 : item.powerSummation.gridPowerSum, Generator: item.powerSummation.generatorPowerSum == null ? 0 : item.powerSummation.generatorPowerSum, solarTotal: item.powerSummation.solarPowerSum == null ? 0 : item.powerSummation.solarPowerSum * 11, gridTotal: item.powerSummation.gridPowerSum == null ? 0 : item.powerSummation.gridPowerSum * 11, generatorTotal: item.powerSummation.generatorPowerSum == null ? 0 : item.powerSummation.generatorPowerSum * 11 });
      });
    }
    return data;
  }

  render() {
    const { loader, match, deviceInformation, deviceCurrentStatus } = this.props;
    const { from, to } = this.state;
    var minFromDate = new Date();
    minFromDate.setFullYear(minFromDate.getFullYear() - 1);
    return (
      <div className="deviceinfo animated slideInUpTiny animation-duration-3">
        <ContainerHeader match={match} title={deviceInformation ? deviceInformation.deviceName : null} />
        {loader ? (<div className="loader-view">
          <CircularProgress />
        </div>) :
          <div className="row">
            <div className="col-12">
              <div className="jr-card card">
                <div className="row">
                  <div className="col-xl-6 col-lg-6 col-md-8 col-sm-9 col-12">
                    <h2 className="text-white">Device Details</h2>
                    <ul className="profiledetail">
                      <li>
                        <label>Device Name</label>
                        <span>{deviceInformation ? deviceInformation.deviceName : ""}</span>
                      </li>
                      <li>
                        <label>Device IMEI</label>
                        <span>{deviceInformation ? deviceInformation.deviceIMEI : ""}</span>
                      </li>
                      <li>
                        <label>Meter Number</label>
                        <span>{deviceInformation ? deviceInformation.deviceIp : ""}</span>
                      </li>
                      <li>
                        <label>Device Location</label>
                        <span>{deviceInformation ? deviceInformation.location != undefined ? deviceInformation.location.address : "" : ""}</span>
                      </li>
                    </ul>
                  </div>
                  {/* <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                    <img src={require('../../../../../assets/images/map-white.png')} />
                  </div> */}
                </div>

              </div>

              <DeviceStatus
                deviceCurrentStatus={deviceCurrentStatus}
              />

              <div className="jr-card card">
                <div className="d-flex flex-row">
                  <h2 className="mb-3 text-white">Device Statistics</h2>
                </div>
                {this.deviceStatistics()}
              </div>
              <div className="jr-card card">
                <div className="row">
                  <div className="col-xl-6 col-lg-6 col-md-12 col-12">
                    <Nav className="jr-tabs-pills-ctr nav-justified" pills>
                      <NavItem>
                        <NavLink
                          className={classnames({ active: this.state.activeTab === '1' })}
                          onClick={() => {
                            this.toggle('1');
                          }}>
                          Daily
                    </NavLink>
                      </NavItem>
                      <NavItem>
                        <NavLink
                          className={classnames({ active: this.state.activeTab === '2' })}
                          onClick={() => {
                            this.toggle('2');
                          }}
                        >
                          Weekly
                    </NavLink>
                      </NavItem>
                      <NavItem>
                        <NavLink
                          className={classnames({ active: this.state.activeTab === '3' })}
                          onClick={() => {
                            this.toggle('3');
                          }}>
                          Monthly
                    </NavLink>
                      </NavItem>
                      <NavItem>
                        <NavLink
                          className={classnames({ active: this.state.activeTab === '4' })}
                          onClick={() => {
                            this.toggle('4');
                          }}>
                          Yearly
                    </NavLink>
                      </NavItem>
                      <NavItem style={{ display: 'none' }}>
                        <NavLink
                          className={classnames({ active: this.state.activeTab === '0' })}
                          onClick={() => {
                            this.toggle('0');
                          }}>

                        </NavLink>
                      </NavItem>
                    </Nav>
                  </div>
                  <div className="col-xl-6 col-lg-6 col-md-12 col-12">
                    
                    <div className="fromto d-flex flex-row align-items-center justify-content-between">

                      <div className="d-flex flex-row align-items-center justify-content-between">
                        <label className="text-white">From</label>
                        <div className="formgroup mb-0">
                          <DatePicker className="custominput"
                            fullWidth
                            value={from}
                            maxDate={new Date()}
                            minDate={minFromDate}
                            onChange={(value) => this.handleDateChange("FROM", value)}
                            animateYearScrolling={false}
                            leftArrowIcon={<i className="zmdi zmdi-arrow-back" />}
                            rightArrowIcon={<i className="zmdi zmdi-arrow-forward" />}
                          />
                        </div>
                      </div>
                      <div className="d-flex flex-row align-items-center justify-content-between">
                        <label className="text-white">To</label>
                        <div className="formgroup mb-0">
                          <DatePicker className="custominput"
                            fullWidth
                            maxDate={new Date()}
                            minDate={from}
                            value={to}
                            onChange={(value) => this.handleDateChange("TO", value)}
                            animateYearScrolling={false}
                            leftArrowIcon={<i className="zmdi zmdi-arrow-back" />}
                            rightArrowIcon={<i className="zmdi zmdi-arrow-forward" />}
                          />
                        </div>
                      </div>

                      <div className="d-flex flex-row flex-wrap align-items-center justify-content-between">
                        <Button className="customButton" variant="contained" onClick={this.handleCustomRequest}>Apply</Button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-xl-6 col-lg-6 col-md-12 mb-3">
                  <div className="jr-card card">
                    <h2 className="text-white mb-4">Total Consumptions</h2>

                    <Suspense fallback={<div><CircularProgress /></div>}>
                      <TotalConsumptionsChart getDeviceStatistics={this.getDeviceStatistics} />
                    </Suspense>
                  </div>
                </div>
                <div className="col-xl-6 col-lg-6 col-md-12 mb-3">
                  <div className="jr-card card">
                    <h2 className="text-white mb-4">Solar</h2>
                    <Suspense fallback={<div><CircularProgress /></div>}>
                      <SolarChart getDeviceStatistics={this.getDeviceStatistics} />
                    </Suspense>
                  </div>
                </div>
                <div className="col-xl-6 col-lg-6 col-md-12 mb-3">
                  <div className="jr-card card">
                    <h2 className="text-white mb-4">Grid</h2>
                    <Suspense fallback={<div><CircularProgress /></div>}>
                      <GridChart getDeviceStatistics={this.getDeviceStatistics} />
                    </Suspense>
                  </div>
                </div>
                <div className="col-xl-6 col-lg-6 col-md-12 mb-3">
                  <div className="jr-card card">
                    <h2 className="text-white mb-4">Generator</h2>
                    <Suspense fallback={<div><CircularProgress /></div>}>
                      <GeneratorChart getDeviceStatistics={this.getDeviceStatistics} />
                    </Suspense>
                  </div>
                </div>
                {/* <div className="col-xl-12 col-lg-12 col-md-12 mb-3">
                  <div className="jr-card card">
                    <h2 className="text-white mb-4">Total</h2>
                    <Suspense fallback={<div><CircularProgress /></div>}>
                      <TotalCostChart getDeviceStatistics={this.getDeviceStatistics} />
                    </Suspense>
                  </div>
                </div> */}
              </div>
            </div>
          </div>
        }
      </div>
    );
  }
}

const mapStateToProps = ({ devices }) => {
  const { deviceInformation, loader, deviceCurrentStatus, deviceDailyStatistics, deviceWeeklyStatistics, deviceMonthlyStatistics, deviceYearlyStatistics, deviceCustomStatistics } = devices;
  return { deviceInformation, loader, deviceCurrentStatus, deviceDailyStatistics, deviceWeeklyStatistics, deviceMonthlyStatistics, deviceYearlyStatistics, deviceCustomStatistics }
};
const mapDispatchToProps = dispatch => ({
  deviceGetDeviceRequest: payload => dispatch(actions.deviceGetDeviceRequest(payload)),
  deviceShowLoader: () => dispatch(actions.deviceShowLoader()),
  deviceHideLoader: () => dispatch(actions.deviceHideLoader()),
  deviceDeviceClear: () => dispatch(actions.deviceDeviceClear()),
  deviceGetDeviceStatistics: payload => dispatch(actions.deviceGetDeviceStatistics(payload))
});
export default connect(mapStateToProps, mapDispatchToProps)(DeviceInformation);