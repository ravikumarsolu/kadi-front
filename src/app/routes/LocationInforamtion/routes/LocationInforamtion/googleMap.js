import React from 'react';
import { Map, Marker, GoogleApiWrapper } from "google-maps-react";

const GoogleMaps = (props) => {


    return (
        <Map
            google={props.google}
            // style={{ height: "50vh", width: "550px" }}
            initialCenter={{
                lat:props.lat,
                lng:props.lng
            }}
            center={{
                lat: props.lat,
                lng: props.lng
            }}
            zoom={12}
            // onClick={this.onMapClicked}
        >
            <Marker
                name={props.name}
                position={{ lat: props.lat, lng: props.lng }}
                label={{
                    text: props.name,
                    fontFamily: "Arial",
                    fontSize: "19px"
                }}
            />
        </Map>
    );
}

export default GoogleApiWrapper({
    apiKey: "AIzaSyAhtksO2T1hYIOMJA0RSoklLD3uFRIG_T8"
})(GoogleMaps)