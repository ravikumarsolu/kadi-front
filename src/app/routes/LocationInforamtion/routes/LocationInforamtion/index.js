import React, { Component, Suspense, lazy } from "react";
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import ContainerHeader from "components/ContainerHeader";
import moment from 'moment';
import { connect } from 'react-redux';
import * as actions from '../../../../../actions';
import { Map, Marker, GoogleApiWrapper } from "google-maps-react";
// import GoogleMaps from "./googleMap";
const GoogleMaps = lazy(() => import('./googleMap'));

class LocationInformation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedDeviceIndex: 0
    };
  }
  componentWillMount() {
    this.props.locationGetLocation(window.location.href.split("/")[5]);
  }
  componentWillUnmount() {

  }
  handleChange = event => {
    this.setState({ selectedDeviceIndex: event.target.value });

  };

  render() {
    const { loader, locationInformation, match } = this.props;
    console.log("TCL: LocationInformation -> render -> locationInformation", locationInformation)
    return (
      <div className="deviceinfo animated slideInUpTiny animation-duration-3">
        <ContainerHeader match={match} title={"Location Details"} />
        <div className="row">
          <div className="col-12">
            <div className="jr-card card">
              <h2 className="text-white">Location Information</h2>
              <div className="row">
                <div className="col-xl-6 col-lg-6 col-md-8 col-sm-9 col-12">
                  <ul className="profiledetail">
                    <li>
                      <label>Address: </label>
                      <span>{locationInformation ? locationInformation.address : ""}</span>
                    </li>
                    <li>
                      <label>City: </label>
                      <span>{locationInformation ? locationInformation.city : ""}</span>
                    </li>
                    <li>
                      <label>State: </label>
                      <span>{locationInformation ? locationInformation.state : ""}</span>
                    </li>
                    <li>
                      <label>Country: </label>
                      <span>Ghana</span>
                    </li>
                  </ul>
                </div>
                <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                  {locationInformation.latitude != undefined ?
                    <Suspense fallback={<div>Loading...</div>}>
                      <GoogleMaps lat={locationInformation.latitude}
                        lng={locationInformation.longitude}
                        name={locationInformation.address}
                      />
                    </Suspense> : null}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = ({ location }) => {
  const { locationInformation, loader } = location;
  return { locationInformation, loader }
};
const mapDispatchToProps = dispatch => ({
  locationGetLocation: payload => dispatch(actions.locationGetLocation(payload)),

});
export default connect(mapStateToProps, mapDispatchToProps)(LocationInformation);