import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

class ChangeStatusDialog extends React.Component {
    state = {
        open: false,
    };

    handleResponse = () => {
        const { statusData, deviceDeviceStatusChanged, handleRequestClose } = this.props;
        deviceDeviceStatusChanged(statusData);
        handleRequestClose();
    };
    render() {
        const { open, handleRequestClose } = this.props;
        return (
            <div>
                <i className="zmdi zmdi-delete" onClick={this.handleClickOpen} />
                <Dialog open={open} onClose={handleRequestClose} className="customDialogue">
                    <DialogTitle className="text-center text-white">Alert</DialogTitle>
                    <DialogContent>
                        <DialogContentText className="text-center text-white">
                            Are you sure you want to change device status?
            </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleResponse} className="customButton">
                            Yes
            </Button>
                        <Button onClick={handleRequestClose} className="customButton lineButton">
                            No
            </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}

export default ChangeStatusDialog;