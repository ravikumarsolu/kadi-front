import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import * as actions from '../../../../../../actions';
import { connect } from 'react-redux';
class SubscriptionModal extends React.Component {

    handleBuyRequest = () => {
        const { planId, subscriptionBuySubscriptionPlan, handleRequestClose, subscriptionShowLoader, mobileNumber } = this.props;
        let userId = localStorage.getItem("user_id");

        subscriptionBuySubscriptionPlan({ partyId: mobileNumber, customerId: userId, planId });
        subscriptionShowLoader("buyLoader");
        handleRequestClose();

    }
    render() {
        const { open, handleRequestClose, mobileNumber } = this.props;
        return (
            <div>
                <Dialog open={open} onClose={handleRequestClose} className="customDialogue">
                    <DialogTitle className="text-center text-white">Buy Subscription</DialogTitle>
                    <DialogContent>
                        <div className="formgroup">
                            <TextField
                                fullWidth
                                placeholder="Enter Mobile Number"
                                type="text"
                                onChange={(event) => this.props.subscriptionMobileNumberChanged(event.target.value)}
                                value={mobileNumber}
                                margin="normal"
                                className="mt-0 my-0 custominput"
                            />
                        </div>

                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleBuyRequest} className="customButton">
                            Pay
                        </Button>
                        <Button onClick={handleRequestClose} className="customButton lineButton">
                            Cancel
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}
const mapStateToProps = ({ subscription }) => {
    const { mobileNumber } = subscription;
    return { mobileNumber }
};
const mapDispatchToProps = dispatch => ({
    subscriptionBuySubscriptionPlan: payload => dispatch(actions.subscriptionBuySubscriptionPlan(payload)),
    subscriptionMobileNumberChanged: payload => dispatch(actions.subscriptionMobileNumberChanged(payload)),
    subscriptionShowLoader: payload => dispatch(actions.subscriptionShowLoader(payload))
});
export default connect(mapStateToProps, mapDispatchToProps)(SubscriptionModal);