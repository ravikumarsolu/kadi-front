import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import * as actions from '../../../../../../actions';
import { connect } from 'react-redux';
// import Select from '@material-ui/core/Select';
import Select from "react-dropdown-select"
import { toast } from "react-toastify";
import AppModuleHeader from '../../../../../../components/AppModuleHeader';
import options from './options';

class AddDeviceDialog extends React.Component {
  state = {
    searchUser: '',
    selectValues: []
  }
  setValues = selectValues => {
    console.log("SSSSSSSSS", selectValues);

    this.setState({ selectValues });
  }
  handleAddRequest = () => {
    const { deviceIMEI, deviceLocation, deviceName, deviceIP, isEdit } = this.props;
    if (deviceName == '') {
      toast.error("Name cannot be blank", {
        toastId: "offlinePayment",
        hideProgressBar: true,
        autoClose: 3000,
        position: toast.POSITION.TOP_RIGHT
      });
    } else if (!deviceName.replace(/\s/g, '').length) {
      toast.error("Name cannot be blank", {
        toastId: "offlinePayment",
        hideProgressBar: true,
        autoClose: 3000,
        position: toast.POSITION.TOP_RIGHT
      });
    } else if (deviceIMEI == '') {
      toast.error("IMEI cannot be blank", {
        toastId: "offlinePayment",
        hideProgressBar: true,
        autoClose: 3000,
        position: toast.POSITION.TOP_RIGHT
      });
    } else if (!deviceIMEI.replace(/\s/g, '').length) {
      toast.error("IMEI cannot be blank", {
        toastId: "offlinePayment",
        hideProgressBar: true,
        autoClose: 3000,
        position: toast.POSITION.TOP_RIGHT
      });
    } else if (this.state.selectValues == []) {
      toast.error("Location cannot be blank", {
        toastId: "offlinePayment",
        hideProgressBar: true,
        autoClose: 3000,
        position: toast.POSITION.TOP_RIGHT
      });
    } else if (deviceIP == '') {
      toast.error("Meter Number cannot be blank", {
        toastId: "offlinePayment",
        hideProgressBar: true,
        autoClose: 3000,
        position: toast.POSITION.TOP_RIGHT
      });
    }
    //  else if (!/^(\d|[1-9]\d|1\d\d|2([0-4]\d|5[0-5]))\.(\d|[1-9]\d|1\d\d|2([0-4]\d|5[0-5]))\.(\d|[1-9]\d|1\d\d|2([0-4]\d|5[0-5]))\.(\d|[1-9]\d|1\d\d|2([0-4]\d|5[0-5]))$/.test(deviceIP)){
    //   toast.error("IP is invalid", {
    //     toastId: "offlinePayment",
    //     hideProgressBar: true,
    //     autoClose: 3000,
    //     position: toast.POSITION.TOP_RIGHT
    //   });
    // }
    else {
      this.props.deviceDeviceAddRequest({
        deviceImei: deviceIMEI,
        deviceName: deviceName,
        deviceIp: deviceIP,
        locationId: this.state.selectValues[0].locationId,
        isEdit
      });
      this.props.deviceShowLoader();
      this.props.handleRequestClose();
    }
  }
  updateContactUser(evt) {
    this.setState({
      searchUser: evt.target.value,
    });
    // this.filterContact(evt.target.value)
  }
  // filterContact = (userName) => {
  //   const {filterOption} = this.state;
  //   if (userName === '') {
  //     this.setState({contactList: this.state.allContact});
  //   } else {
  //     const filterContact = this.state.allContact.filter((contact) =>
  //       contact.name.toLowerCase().indexOf(userName.toLowerCase()) > -1);
  //     if (filterOption === 'All contacts') {
  //       this.setState({contactList: filterContact});
  //     } else if (filterOption === 'Frequently contacted') {
  //       this.setState({contactList: filterContact.filter((contact) => contact.frequently)});

  //     } else if (filterOption === 'Starred contacts') {
  //       this.setState({contactList: filterContact.filter((contact) => contact.starred)});
  //     }
  //   }
  // };
  render() {
    const { deviceIMEI, deviceIP, deviceName, open, deviceLocation, isEdit, allLocations } = this.props;
    return (
      <div>
        <Dialog open={open} onClose={this.props.handleRequestClose} className="customDialogue">
          <DialogTitle className="text-center text-white">{isEdit ? "Edit" : "Add"} Device</DialogTitle>
          <DialogContent>
            {/* <DialogContentText>
              To subscribe to this website, please enter your email address here. We will send
              updates occationally.
            </DialogContentText> */}
            <div className="formgroup">
              <TextField
                fullWidth
                placeholder="Enter Device Name"
                type="text"
                onChange={(event) => this.props.deviceDeviceNameChanged(event.target.value)}
                defaultValue={deviceName}
                margin="normal"
                className="mt-0 my-0 custominput"
              />
            </div>
            <div className="formgroup">
              <TextField
                fullWidth
                placeholder="Enter IMEI"
                type="text"
                disabled={isEdit}
                onChange={(event) => this.props.deviceDeviceIMEIChanged(event.target.value)}
                defaultValue={deviceIMEI}
                margin="normal"
                className="mt-0 my-0 custominput"
              />
            </div>
            <FormControl className="w-100">
              <Select
                selectValues
                searchable={true}
                className="customSelect"
                options={allLocations}
                placeholder="Select Address"
                onChange={(values) => this.setValues(values)}
                searchBy="address"
                valueField="address"
                labelField="address"
                noDataLabel="No matches found"
              // values={[options.find(opt => opt.username === "Delphine")]}
              />
            </FormControl>
            <div className="formgroup">
              <TextField
                fullWidth
                placeholder="Enter Meter Number"
                type="number"
                onChange={(event) => this.props.deviceDeviceIPChanged(event.target.value)}
                defaultValue={deviceIP}
                margin="normal"
                className="mt-0 my-0 custominput"
              />
            </div>


          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleAddRequest} className="customButton">
              {isEdit ? "Update" : "Add"}
            </Button>
            <Button onClick={this.props.handleRequestClose} className="customButton lineButton">
              Cancel
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}
const mapStateToProps = ({ devices }) => {
  const { deviceName, deviceIMEI, deviceIP, deviceLocation, allLocations } = devices;
  return { deviceName, deviceIMEI, deviceIP, deviceLocation, allLocations }
};
const mapDispatchToProps = dispatch => ({
  deviceDeviceNameChanged: payload => dispatch(actions.deviceDeviceNameChanged(payload)),
  deviceDeviceIMEIChanged: payload => dispatch(actions.deviceDeviceIMEIChanged(payload)),
  deviceDeviceLocationChanged: payload => dispatch(actions.deviceDeviceLocationChanged(payload)),
  deviceDeviceIPChanged: payload => dispatch(actions.deviceDeviceIPChanged(payload)),
  deviceDeviceAddRequest: payload => dispatch(actions.deviceDeviceAddRequest(payload)),
  deviceShowLoader: payload => dispatch(actions.deviceShowLoader()),
  deviceHideLoader: payload => dispatch(actions.deviceHideLoader()),



});
export default connect(mapStateToProps, mapDispatchToProps)(AddDeviceDialog);