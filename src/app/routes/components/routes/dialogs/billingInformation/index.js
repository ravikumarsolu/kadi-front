import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import * as actions from '../../../../../../actions';
import { connect } from 'react-redux';
class BillingInformationModal extends React.Component {

    handleBuyRequest = () => {
        const { mobileNumber, billingPayBill, handleRequestClose, amount,billingId } = this.props;
        let userId = localStorage.getItem("user_id");

        billingPayBill({ partyId: mobileNumber, billingId, amount });
        handleRequestClose();

    }
    render() {
        const { open, handleRequestClose, mobileNumber, amount } = this.props;
        return (
            <div>
                <Dialog open={open} onClose={handleRequestClose} className="customDialogue">
                    <DialogTitle className="text-center text-white">Pay Bill</DialogTitle>
                    <DialogContent>
                        <div className="formgroup">
                            <TextField
                                fullWidth
                                placeholder="Enter Mobile Number"
                                type="text"
                                onChange={(event) => this.props.billingMobileNumberChanged(event.target.value)}
                                value={mobileNumber}
                                margin="normal"
                                className="mt-0 my-0 custominput"
                            />
                        </div>

                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleBuyRequest} className="customButton">
                            Pay
                        </Button>
                        <Button onClick={handleRequestClose} className="customButton lineButton">
                            Cancel
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}
const mapStateToProps = ({ billing }) => {
    const { mobileNumber } = billing;
    return { mobileNumber }
};
const mapDispatchToProps = dispatch => ({
    billingPayBill: payload => dispatch(actions.billingPayBill(payload)),
    billingMobileNumberChanged: payload => dispatch(actions.billingMobileNumberChanged(payload))
});
export default connect(mapStateToProps, mapDispatchToProps)(BillingInformationModal);