import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import * as actions from '../../../../../../actions';
import { connect } from 'react-redux';
import Select from '@material-ui/core/Select';
import { toast } from "react-toastify";
import Geocode from "react-geocode";
import data from '../../../../../../constants/cities.js';
let cityArray = [];
class AddLocationDialog extends React.Component {
  state = {
    stateName: "Select State",
    cityName: "Select City",
    editTouch:false
  }
  handleStateChange = (e) => {
    this.setState({ stateName: e.target.value });
    this.setState({ cityName: "Select City" });
    this.props.locationLocationStateChanged(e.target.value)
    cityArray = data[0].states[e.target.value];
  }
  handleCityChange = (e) => {
    this.setState({ cityName: e.target.value });
    this.props.locationLocationCityChanged(e.target.value)
  }
  handleAddRequest = () => {
    const { address1, state, city, locationAddLocation, handleRequestClose, isEdit, locationId } = this.props;
    const {stateName,cityName} = this.state;

    if (address1 == "") {
      toast.error("Address cannot be blank", {
        toastId: "offlinePayment",
        hideProgressBar: true,
        autoClose: 3000,
        position: toast.POSITION.TOP_RIGHT
      });
    } else if (stateName == "" || stateName=="Select State") {
      toast.error("State cannot be blank", {
        toastId: "offlinePayment",
        hideProgressBar: true,
        autoClose: 3000,
        position: toast.POSITION.TOP_RIGHT
      });
    } else if (cityName == "" || cityName=="Select City") {
      toast.error("City cannot be blank", {
        toastId: "offlinePayment",
        hideProgressBar: true,
        autoClose: 3000,
        position: toast.POSITION.TOP_RIGHT
      });
    } else {
      Geocode.setApiKey("AIzaSyAhtksO2T1hYIOMJA0RSoklLD3uFRIG_T8");
      Geocode.fromAddress(address1 + "," + city + "," + state).then(
        response => {
          const { lat, lng } = response.results[0].geometry.location;
          locationAddLocation({
            address: address1,
            city: city,
            state: state,
            latitude: lat,
            longitude: lng,
            isEdit,
            locationId
          });
          handleRequestClose()
        },
        error => {
          console.error("error:", error);
        }
      );
    }
  }

  componentWillUnmount() {
  }
  render() {
    const { address1, city, state, address2, open, isEdit } = this.props;
    const { cityName, stateName, editTouch } = this.state;
    console.log("TCL: render -> this.state", this.state)
    let statesArray = Object.keys(data[0].states).map((item) => (item));

    if (isEdit && (cityName == "Select City" || stateName == "Select State") && !editTouch) {
      cityArray = data[0].states[state];
      this.setState({ cityName: city, stateName: state, editTouch:true });
    }
    return (
      <div>
        <Dialog open={open} onClose={this.props.handleRequestClose} className="customDialogue">
          <DialogTitle className="text-center text-white">
            {isEdit ? "Edit" : "Add"} Location
          </DialogTitle>
          <DialogContent>
            {/* <DialogContentText>
              To subscribe to this website, please enter your email address here. We will send
              updates occationally.
            </DialogContentText> */}
            <div className="formgroup">
              <TextField
                fullWidth
                placeholder="Address"
                type="text"
                onChange={(event) => this.props.locationLocationAddress1Changed(event.target.value)}
                defaultValue={address1}
                margin="normal"
                className="mt-0 my-0 custominput"
              />
            </div>
            <div className="formgroup">
              <TextField
                fullWidth
                placeholder="Country"
                type="text"
                value="Ghana"
                disabled
                onChange={(event) => this.props.locationLocationAddress1Changed(event.target.value)}
                defaultValue={address1}
                margin="normal"
                className="mt-0 my-0 custominput"
              />
            </div>
            <FormControl className="w-100 mb-4">
              <Select className="customSelect"
                value={this.state.stateName}
                onChange={this.handleStateChange}
              >
                <MenuItem value="Select State" disabled>
                  {"Select State"}
                </MenuItem>
                {statesArray.map((item, index) => (<MenuItem
                  key={index}
                  value={item}
                  style={{
                    fontWeight: this.state.stateName.indexOf(item.deviceIMEI) !== -1 ? '500' : '400',
                  }}
                >{item}</MenuItem>))}
              </Select>
            </FormControl>
            <FormControl className="w-100">
              <Select className="customSelect"
                value={this.state.cityName}
                onChange={this.handleCityChange}
              >
                <MenuItem value="Select City" disabled>
                  {"Select City"}
                </MenuItem>
                {cityArray.map((item, index) => (<MenuItem
                  key={index}
                  value={item}
                  style={{
                    fontWeight: this.state.cityName.indexOf(item.deviceIMEI) !== -1 ? '500' : '400',
                  }}
                >{item}</MenuItem>))}
              </Select>
            </FormControl>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleAddRequest} className="customButton">
              {isEdit ? "Update" : "Add"}

            </Button>
            <Button onClick={this.props.handleRequestClose} className="customButton lineButton">
              Cancel
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}
const mapStateToProps = ({ location }) => {
  const { address1, address2, city, state, locationId } = location;
  return { address1, address2, city, state, locationId }
};
const mapDispatchToProps = dispatch => ({
  locationLocationAddress1Changed: payload => dispatch(actions.locationLocationAddress1Changed(payload)),
  locationLocationAddress2Changed: payload => dispatch(actions.locationLocationAddress2Changed(payload)),
  locationAddLocation: payload => dispatch(actions.locationAddLocation(payload)),
  locationLocationStateChanged: payload => dispatch(actions.locationLocationStateChanged(payload)),
  locationLocationCityChanged: payload => dispatch(actions.locationLocationCityChanged(payload))


});
export default connect(mapStateToProps, mapDispatchToProps)(AddLocationDialog);