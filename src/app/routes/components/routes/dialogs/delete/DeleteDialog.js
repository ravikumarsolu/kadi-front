import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';


class DeleteDialog extends React.Component {
  state = {
    open: false,
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleRequestClose = () => {
    this.setState({ open: false });

  };

  handleResponse = () => {
    const { isStaff, isDevice, userId, deviceId, isLocation, locationId, deleteStaffUser, deleteDevice, deleteLocation } = this.props;
    console.log("TCL: DeleteDialog -> handleResponse -> deviceId", deviceId)
    if (isStaff) {
      deleteStaffUser(userId);
    } else if (isDevice) {
      deleteDevice(deviceId);
    }
    else if (isLocation) {
      deleteLocation(locationId)
    }
    this.setState({ open: false });
  };
  render() {
    const { isStaff, isDevice, isLocation } = this.props;
    return (
      <div>
        <i className="zmdi zmdi-delete" onClick={this.handleClickOpen} />
        <Dialog open={this.state.open} onClose={this.handleRequestClose} className="customDialogue">
          <DialogTitle className="text-center text-white">Delete</DialogTitle>
          <DialogContent>
            <DialogContentText className="text-center text-white">
              Are you sure you want to delete this {isStaff ? "user" : isDevice ? "device" : isLocation ? "location" : ""}?
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleResponse} className="customButton">
              Yes
            </Button>
            <Button onClick={this.handleRequestClose} className="customButton lineButton">
              No
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

export default DeleteDialog;