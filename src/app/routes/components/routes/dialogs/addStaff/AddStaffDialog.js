import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import Input from '@material-ui/core/Input';
import DialogTitle from '@material-ui/core/DialogTitle';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { connect } from 'react-redux';
import * as actions from '../../../../../../actions';
import { toast } from "react-toastify";
const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;

class AddStaffDialog extends React.Component {
  state = {
    open: false,
    name: [],
    devicesId: [],
    addData: false,
  };

  handleChange = event => {
    this.setState({ name: event.target.value });

  };
  handleRequestClose = () => {
    this.setState({ open: false, addData: false });
  };
  handleAddStaff = () => {
    const { firstName, lastName, contactNumber, email, userToEdit, isEdit, address } = this.props;
    var regexOnlyAlphabates = /\d/g;
    if (firstName == '') {
      toast.error("First Name cannot be blank", {
        toastId: "offlinePayment",
        hideProgressBar: true,
        autoClose: 3000,
        position: toast.POSITION.TOP_RIGHT
      });
    } else if (regexOnlyAlphabates.test(firstName)) {
      toast.error("First Name cannot contain numbers", {
        toastId: "offlinePayment",
        hideProgressBar: true,
        autoClose: 3000,
        position: toast.POSITION.TOP_RIGHT
      });
    } else if (lastName == '') {
      toast.error("Last Name cannot be blank", {
        toastId: "offlinePayment",
        hideProgressBar: true,
        autoClose: 3000,
        position: toast.POSITION.TOP_RIGHT
      });
    } else if (regexOnlyAlphabates.test(lastName)) {
      toast.error("Last Name cannot contain numbers", {
        toastId: "offlinePayment",
        hideProgressBar: true,
        autoClose: 3000,
        position: toast.POSITION.TOP_RIGHT
      });
    }else if (contactNumber == '') {
      toast.error("Contact number cannot be blank", {
        toastId: "offlinePayment",
        hideProgressBar: true,
        autoClose: 3000,
        position: toast.POSITION.TOP_RIGHT
      });
    } else if (email.trim() == '') {
      toast.error("Email cannot be blank", {
        toastId: "offlinePayment",
        hideProgressBar: true,
        autoClose: 3000,
        position: toast.POSITION.TOP_RIGHT
      });
    } else if (!/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$/.test(email)) {
      toast.error("Invalid Email", {
        toastId: "offlinePayment",
        hideProgressBar: true,
        autoClose: 3000,
        position: toast.POSITION.TOP_RIGHT
      });
    } else if (address == '') {
      toast.error("Address cannot be blank", {
        toastId: "offlinePayment",
        hideProgressBar: true,
        autoClose: 3000,
        position: toast.POSITION.TOP_RIGHT
      });
    } else if (this.state.name == []) {
      toast.error("Address cannot be blank", {
        toastId: "offlinePayment",
        hideProgressBar: true,
        autoClose: 3000,
        position: toast.POSITION.TOP_RIGHT
      });
    } else {
      this.props.staffUserAddRequest({
        firstName: firstName,
        lastName: lastName,
        email: email,
        contactNumber: contactNumber,
        deviceIMEI: this.state.name,
        isEdit: isEdit,
        address: address,
        userId: userToEdit ? userToEdit.id : null
      });
      this.setState({ addData: false });
      this.props.staffShowLoader();
      this.props.handleRequestClose();
    }
  }
  render() {
    const { firstName, lastName, contactNumber, email, devices, availableDevices, open, isEdit, address } = this.props;
    if (this.props.isEdit && !this.state.addData) {
      this.setState({ name: [...this.state.name, ...this.props.devices], addData: true });
    } else if (!this.props.isEdit && !this.state.addData) {
      this.setState({ addData: true });
    }
    return (
      <div>

        <Dialog open={open} onClose={this.props.handleRequestClose} className="customDialogue">
          <DialogTitle className="text-center text-white">{isEdit ? "Edit" : "Add"} User</DialogTitle>
          <DialogContent>
            {/* <DialogContentText>
              To subscribe to this website, please enter your email address here. We will send
              updates occationally.
            </DialogContentText> */}
            <div className="formgroup">
              <TextField
                fullWidth
                placeholder="Enter First Name"
                type="text"
                onChange={(e) => this.props.staffUserFirstNameChanged(e.target.value)}
                defaultValue={firstName}
                margin="normal"
                inputProps={{ pattern: "[a-z]" }}
                className="mt-0 my-0 custominput"
              />
            </div>
            <div className="formgroup">
              <TextField
                fullWidth
                placeholder="Enter Last Name"
                type="text"
                onChange={(e) => {
                  this.props.staffUserLastNameChanged(e.target.value);
                }}
                defaultValue={lastName}
                margin="normal"
                className="mt-0 my-0 custominput"
              />
            </div>
            <div className="formgroup">
              <TextField
                fullWidth
                placeholder="Contact Number"
                type="number"
                onChange={(e) => this.props.staffUserContactNumberChanged(e.target.value)}
                defaultValue={contactNumber}
                margin="normal"
                className="mt-0 my-0 custominput"
              />
            </div>
            <div className="formgroup">
              <TextField
                fullWidth
                placeholder="Email ID"
                type="email"
                onChange={(e) => this.props.staffUserEmailChanged(e.target.value)}
                defaultValue={email}
                margin="normal"
                className="mt-0 my-0 custominput"
                disabled={isEdit}
              />
            </div>
            <div className="formgroup">
              <TextField
                fullWidth
                placeholder="Address"
                type="text"
                onChange={(e) => this.props.staffUserAddressChanged(e.target.value)}
                defaultValue={address}
                margin="normal"
                className="mt-0 my-0 custominput"
              />
            </div>
            <label className="text-white mb-2">Select Devices</label>
            <FormControl className="w-100 multideviceselect">
              <Select
                className="customSelect mt-0 my-0"
                multiple
                value={this.state.name}
                onChange={this.handleChange}
                input={<Input id="name-multiple" />}
                MenuProps={{
                  PaperProps: {
                    style: {
                      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
                      width: 50,
                    },
                  },
                }}
              >
                {availableDevices.length > 0 ? availableDevices.map((item, index) => (
                  <MenuItem
                    key={index}
                    value={item.deviceIMEI}
                    style={{
                      fontWeight: this.state.name.indexOf(item.deviceIMEI) !== -1 ? '500' : '400',
                    }}
                  >
                    {item.deviceIMEI}
                  </MenuItem>
                )) : isEdit ? <MenuItem disabled>No New Device Available</MenuItem> : <MenuItem disabled>No Device Available</MenuItem>}

                {devices && isEdit ? devices.map((item, index) => (
                  <MenuItem
                    key={index}
                    value={item}
                    style={{
                      fontWeight: this.state.name.indexOf(item) !== -1 ? '500' : '400',
                    }}
                  >
                    {item}
                  </MenuItem>
                )) : null}
              </Select>
            </FormControl>

          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleAddStaff} className="customButton">
              {isEdit ? "Update" : "Add"}
            </Button>
            <Button onClick={this.props.handleRequestClose} className="customButton lineButton">
              Cancel
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}
const mapStateToProps = ({ staff }) => {
  const { firstName, lastName, contactNumber, email, deviceName, availableDevices, devices, address } = staff;
  return { firstName, lastName, contactNumber, email, deviceName, availableDevices, devices, address }
};
const mapDispatchToProps = dispatch => ({
  staffUserFirstNameChanged: payload => dispatch(actions.staffUserFirstNameChanged(payload)),
  staffUserLastNameChanged: payload => dispatch(actions.staffUserLastNameChanged(payload)),
  staffUserContactNumberChanged: payload => dispatch(actions.staffUserContactNumberChanged(payload)),
  staffUserEmailChanged: payload => dispatch(actions.staffUserEmailChanged(payload)),
  staffUserDeviceNameChanged: payload => dispatch(actions.staffUserDeviceNameChanged(payload)),
  staffUserDeviceChanged: payload => dispatch(actions.staffUserDeviceChanged(payload)),
  staffUserAddRequest: payload => dispatch(actions.staffUserAddRequest(payload)),
  staffShowLoader: () => dispatch(actions.staffShowLoader()),
  staffUserAddressChanged: payload => dispatch(actions.staffUserAddressChanged(payload))
});
export default connect(mapStateToProps, mapDispatchToProps)(AddStaffDialog);
