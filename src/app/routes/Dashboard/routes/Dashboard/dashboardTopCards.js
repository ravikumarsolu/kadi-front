import React from 'react';
import { Card, CardBody, CardText, CardImg, Nav, NavItem, TabContent, TabPane } from 'reactstrap';
import { withRouter, NavLink, Link } from 'react-router-dom';

const DashboardTopCards = (props) => {
    return (
        <div className="row dashboard-top-cards">
            {props.userRole == "admin" ?
                <>
                    <div className="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6">
                        <Card className={`border-0`}>
                            <CardBody className="text-center" >
                                <Link to="/app/staffusers">
                                    <CardImg className="rounded-0 mb-3" src={require("assets/images/cards/users.svg")}
                                        alt="Card image cap" />
                                    <CardText className="text-white mb-0 main-card-content">
                                        Staff Users
                            </CardText>
                                    <CardText className="text-white mb-0 main-card-count">12</CardText>
                                </Link>
                            </CardBody>
                        </Card>
                    </div>
                    <div className="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6">
                        <Card className={`border-0`}>
                            <CardBody className="text-center" >
                                <NavLink to="/app/devices">
                                    <CardImg className="rounded-0 mb-3" src={require("assets/images/cards/devices.svg")}
                                        alt="Card image cap" />
                                    <CardText className="text-white mb-0 main-card-content">
                                        Total Devices
                                    </CardText>
                                    <CardText className="text-white mb-0 main-card-count">6</CardText>
                                </NavLink>
                            </CardBody>
                        </Card>
                    </div>
                </> : null}
            <div className={props.userRole == "admin" ? "col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6" : "col-6"}>
                <Card className={`border-0`}>
                    <CardBody className="text-center">
                        <CardImg className="rounded-0 mb-3" src={require("assets/images/cards/fault.svg")}
                            alt="Card image cap" />
                        <CardText className="text-white mb-0 main-card-content">
                            Fault
              </CardText>
                        <CardText className="text-white mb-0 main-card-count">0</CardText>
                    </CardBody>
                </Card>
            </div>
            <div className={props.userRole == "admin" ? "col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6" : "col-6"}>
                <Card className={`border-0`}>
                    <CardBody className="text-center">
                        <CardImg className="rounded-0 mb-3" src={require("assets/images/cards/maintenance.svg")}
                            alt="Card image cap" />
                        <CardText className="text-white mb-0 main-card-content">
                            Maintenance Reminder
              </CardText>
                        <CardText className="text-white mb-0 main-card-count">0</CardText>
                    </CardBody>
                </Card>
            </div>
        </div>
    );
}

export default withRouter(DashboardTopCards);