import React, { Component, Suspense, lazy } from "react";
import * as actions from '../../../../../actions';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import CircularProgress from '@material-ui/core/CircularProgress';
const DashboardTopCards = lazy(() => import('./dashboardTopCards.js'));
const DeviceStatistics = lazy(() => import('./deviceStatistics.js'));
const TrendsChart = lazy(() => import('./trendsChart.js'));
let total = 0;
class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: 1,
      deviceName: 0,
      totalPower: 0
    };
  }
  componentWillMount() {
    this.props.dashboardGetAllDevices();
  }
  toggle = (tab) => {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }
  handleDeviceChange = (item) => {
    const { allDevices } = this.props
    this.setState({ deviceName: item.target.value });
    this.props.dashboardDashboardDeviceChanged({ id: allDevices[item.target.value].deviceIMEI, trend: "current" });
    this.props.dashboardDashboardDeviceChanged({ id: allDevices[item.target.value].deviceIMEI, trend: "daily" });
    this.props.dashboardDashboardDeviceChanged({ id: allDevices[item.target.value].deviceIMEI, trend: "weekly" });
    this.props.dashboardDashboardDeviceChanged({ id: allDevices[item.target.value].deviceIMEI, trend: "monthly" });
    this.props.dashboardDashboardDeviceChanged({ id: allDevices[item.target.value].deviceIMEI, trend: "quarterly" });
    this.props.dashboardDashboardDeviceChanged({ id: allDevices[item.target.value].deviceIMEI, trend: "yearly" });
  }
  getDeviceStatistics = () => {
    const { dashboardDeviceDailyStatistics, dashboardDeviceWeeklyStatistics, dashboardDeviceMonthlyStatistics, dashboardDeviceYearlyStatistics, dashboardDeviceQuarterlyStatistics } = this.props;
    const { activeTab } = this.state;
    let data = [];
    total = 0;
    if (activeTab === 1) {
      dashboardDeviceDailyStatistics.map((item, index) => {
        total += item.powerSummation.totalPower;
        data.push({ PowerConsumed: item.powerSummation.totalPower == null ? 0 : item.powerSummation.totalPower, TotalCost: item.powerSummation.totalPower == null ? 0 : item.powerSummation.totalPower * 11 });
      });

    }
    else if (activeTab === 2) {
      dashboardDeviceMonthlyStatistics.map((item, index) => {
        total += item.powerSummation.totalPower;
        data.push({ PowerConsumed: item.powerSummation.totalPower == null ? 0 : item.powerSummation.totalPower, TotalCost: item.powerSummation.totalPower == null ? 0 : item.powerSummation.totalPower * 11 });
      });
    }
    else if (activeTab === 3) {
      dashboardDeviceQuarterlyStatistics.map((item, index) => {
        total += item.powerSummation.totalPower;
        data.push({ PowerConsumed: item.powerSummation.totalPower == null ? 0 : item.powerSummation.totalPower, TotalCost: item.powerSummation.totalPower == null ? 0 : item.powerSummation.totalPower * 11 });
      });

    }

    else if (activeTab === 4) {
      total = 0;
      dashboardDeviceYearlyStatistics.map((item, index) => {
        total += item.powerSummation.totalPower;
        data.push({ PowerConsumed: item.powerSummation.totalPower == null ? 0 : item.powerSummation.totalPower, TotalCost: item.powerSummation.totalPower == null ? 0 : item.powerSummation.totalPower * 11 });
      });
      console.log("TCL: Dashboard -> getDeviceStatistics -> total", total)

    }
    return data;
  }

  handleStaffDeviceCard = (value) => {
    // if(value === "staff")
    this.props.history.push("/app/staffusers");
    // else if(value === "devices")
    // this.props.history.push("/app/devices");
  }

  render() {
    const { allDevices, dashboardDeviceCurrentStatus, loader } = this.props;
    const { activeTab } = this.state;
    let userRole = localStorage.getItem("role");
    { this.getDeviceStatistics() };


    return (
      <div className="dashboard animated slideInUpTiny animation-duration-3">

        {loader ? (<div className="loader-view">
          <CircularProgress />
        </div>) :
          <React.Fragment>
            <Suspense fallback={<div className="row dashboard-top-cards"> <CircularProgress /></div>}>
              <DashboardTopCards
                handleStaffDeviceCard={this.handleStaffDeviceCard}
                userRole={userRole}
              />
            </Suspense>
            <div className="row">
              <Suspense fallback={<div className="col-12">
                <div className="jr-card card"><CircularProgress /></div></div>}>
                <DeviceStatistics
                  toggle={this.toggle}
                  activeTab={this.state.activeTab}
                  handleDeviceChange={this.handleDeviceChange}
                  deviceName={this.state.deviceName}
                  allDevices={allDevices}
                  dashboardDeviceCurrentStatus={dashboardDeviceCurrentStatus}
                  totalPower={total}
                  maxVal={activeTab === 1 ? 400 : activeTab === 2 ? 12000 : activeTab === 3 ? 36000 : activeTab === 4 ? 150000 : 1000}
                />
              </Suspense>
              <div className="col-xl-12 col-lg-12 col-md-12 col-12">

                <Suspense fallback={<div className="col-xl-12 col-lg-12 col-md-12 col-12"><CircularProgress /></div>}>
                  <TrendsChart getDeviceStatistics={this.getDeviceStatistics} toggle={this.toggle} activeTab={activeTab} />
                </Suspense>

              </div>
            </div>
          </React.Fragment>}
      </div>
    );
  }
}

const mapStateToProps = ({ dashboard }) => {
  const {
    loader,
    allDevices,
    dashboardDeviceCurrentStatus,
    dashboardDeviceDailyStatistics,
    dashboardDeviceWeeklyStatistics,
    dashboardDeviceMonthlyStatistics,
    dashboardDeviceYearlyStatistics,
    dashboardDeviceQuarterlyStatistics
  } = dashboard;
  return {
    loader,
    allDevices,
    dashboardDeviceCurrentStatus,
    dashboardDeviceDailyStatistics,
    dashboardDeviceWeeklyStatistics,
    dashboardDeviceMonthlyStatistics,
    dashboardDeviceYearlyStatistics,
    dashboardDeviceQuarterlyStatistics
  }
};
const mapDispatchToProps = dispatch => ({
  dashboardDashboardDeviceChanged: payload => dispatch(actions.dashboardDashboardDeviceChanged(payload)),
  dashboardGetAllDevices: () => dispatch(actions.dashboardGetAllDevices())
});
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Dashboard));
