import React from "react";
import { Area, AreaChart, ResponsiveContainer, Tooltip, XAxis, YAxis, CartesianGrid } from "recharts";
import Widget from "components/Widget/index";
import { Nav, NavItem, NavLink } from 'reactstrap';
import classnames from 'classnames';
const TrendsChart = (props) => {

  return (
    <Widget>

      <div className="d-flex flex-row">
        <h2 className="mb-3 text-white">Trends</h2>
      </div>
      {/* <div className="jr-card card"> */}
      <div className="col-xl-6 col-lg-6 col-md-12 col-12">
        <Nav className="jr-tabs-pills-ctr nav-justified" pills>
          <NavItem>
            <NavLink
              className={classnames({ active: props.activeTab === 1 })}
              onClick={() => {
                props.toggle(1);
              }}>
              Daily
                      </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={classnames({ active: props.activeTab === 2 })}
              onClick={() => {
                props.toggle(2);
              }}>
              Monthly
                      </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={classnames({ active: props.activeTab === 3 })}
              onClick={() => {
                props.toggle(3);
              }}>
              Quarterly
                      </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={classnames({ active: props.activeTab === 4 })}
              onClick={() => {
                props.toggle(4);
              }}>
              Yearly
                      </NavLink>
          </NavItem>
        </Nav>
      </div>
      {/* </div> */}
      <div className="row">
        <div className="col-xl-6 col-lg-6 col-md-12 col-12 mt-4">
          <ResponsiveContainer width="100%" height={360}>
            <AreaChart data={props.getDeviceStatistics()}
              margin={{ top: 10, right: 0, left: -15, bottom: 0 }}>
              <XAxis dataKey="name" />
              <YAxis />
              <CartesianGrid strokeDasharray="0 1" />
              <Tooltip />
              <defs>
                <linearGradient id="color15" x1="0" y1="0" x2="0" y2="1">
                  <stop offset="15%" stopColor="#fa9f1b" stopOpacity={0.8} />
                  <stop offset="85%" stopColor="#1b223f" stopOpacity={0} />
                </linearGradient>
              </defs>
              <Area type="monotone" dataKey="PowerConsumed" stroke="#23c0a1" fill="#23c0a1" legendType="round" fillOpacity={0.4} dot={false} />

            </AreaChart>
          </ResponsiveContainer>
          <h3 style={{ color: "#23c0a1", marginLeft: "250px" }}>Power Consumed</h3>
        </div>
        <div className="col-xl-6 col-lg-6 col-md-12 col-12 mt-4">
          <ResponsiveContainer width="100%" height={360}>
            <AreaChart data={props.getDeviceStatistics()}
              margin={{ top: 10, right: 0, left: -15, bottom: 0 }}>
              <XAxis dataKey="name" />
              <YAxis />
              <CartesianGrid strokeDasharray="0 1" />
              <Tooltip />
              <defs>
                <linearGradient id="color15" x1="0" y1="0" x2="0" y2="1">
                  <stop offset="15%" stopColor="#fa9f1b" stopOpacity={0.8} />
                  <stop offset="85%" stopColor="#1b223f" stopOpacity={0} />
                </linearGradient>
              </defs>

              <Area type="monotone" dataKey="TotalCost" stroke="#4db103" fill="#4db103" legendType="round" fillOpacity={0.6} dot={false} />

            </AreaChart>
          </ResponsiveContainer>
          <h3 style={{ color: "#4db103", marginLeft: "250px" }}>Total Cost</h3>
        </div>
      </div>
    </Widget>
  );
};

export default TrendsChart;
