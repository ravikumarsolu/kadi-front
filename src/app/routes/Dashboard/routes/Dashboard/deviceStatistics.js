import React, { Component } from 'react';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import ReactSpeedometer from "react-d3-speedometer";
const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
class DeviceStatistics extends Component {
    render() {
        const { allDevices, deviceName, handleDeviceChange, totalPower, maxVal } = this.props;
        return (
            <div className="col-12">
                <div className="jr-card card">
                    <div className="d-flex flex-row">
                        <h2 className="mb-3 text-white">Device Statistics</h2>
                    </div>
                    <div className="row">
                        <div className="col-xl-6 col-lg-6 col-md-12 col-12">
                            <FormControl className="w-100 mb-2">
                                <Select
                                    className="customSelect mt-0 my-0"

                                    // defaultValue={deviceName}
                                    value={deviceName}
                                    onChange={handleDeviceChange}
                                    // input={<Input id="name-multiple" />}
                                    MenuProps={{
                                        PaperProps: {
                                            style: {
                                                maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
                                                width: 50,
                                            },
                                        },
                                    }}
                                >
                                    {allDevices ? allDevices.map((item, index) => (
                                        <MenuItem
                                            key={index}
                                            value={index}
                                        // style={{
                                        //     fontWeight: this.state.name.indexOf(item.deviceIMEI) !== -1 ? '500' : '400',
                                        // }}
                                        >
                                            {item.deviceIMEI}
                                        </MenuItem>
                                    )) : null}
                                </Select>

                            </FormControl>
                        </div>

                    </div>
                    {/* {this.getDeviceStatistics()} */}
                    <div className="row devicestate">
                        <div>
                            <div className="svg-container">
                                <ReactSpeedometer
                                    id="gauge-chart1"
                                    fluidWidth={true}
                                    ringWidth={12}
                                    segments={10}
                                    minValue={0}
                                    forceRender
                                    maxValue={1000}
                                    // value={dashboardDeviceCurrentStatus.current ? dashboardDeviceCurrentStatus.current : 0}
                                    value={totalPower}
                                    needleColor="#f8a01b"
                                    segmentColors={['#f8a01b', '#f8a01b', '#f8a01b', '#f8a01b', '#f8a01b', '#f8a01b', '#f8a01b', '#f8a01b', '#f8a01b', '#f8a01b']}
                                    needleHeightRatio={0.5}
                                    needleTransitionDuration={4000}
                                    needleTransition="easeElastic"
                                />
                            </div>
                            <span style={{ marginTop: -20 }} className="d-block text-center text-white">Power Consumed(kWh)</span>
                        </div>
                        <div>
                            <div className="svg-container">
                                <ReactSpeedometer
                                    id="gauge-chart2"
                                    fluidWidth={true}
                                    ringWidth={12}
                                    segments={10}
                                    minValue={0}
                                    forceRender
                                    maxValue={maxVal}
                                    maxSegmentLabels={7}
                                    value={(totalPower * 11)}
                                    // value={dashboardDeviceCurrentStatus.current ? dashboardDeviceCurrentStatus.current : 0}
                                    needleColor="#f8a01b"
                                    segmentColors={['#f8a01b', '#f8a01b', '#f8a01b', '#f8a01b', '#f8a01b', '#f8a01b', '#f8a01b', '#f8a01b', '#f8a01b', '#f8a01b']}
                                    needleHeightRatio={0.5}
                                    needleTransitionDuration={4000}
                                    needleTransition="easeElastic"
                                    valueFormat="d"
                                />
                            </div>
                            <span style={{ marginTop: -20 }} className="d-block text-center text-white">Total cost</span>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}


export default DeviceStatistics;