import React from 'react';
import {Redirect, Route, Switch} from 'react-router-dom';
import asyncComponent from '../../../util/asyncComponent';

const Dashboard = ({match}) => (
  <div className="app-wrapper">
    <Switch>
      <Route path={`${match.url}/`} component={asyncComponent(() => import('./routes/Dashboard'))}/>
      {/* <Route component={asyncComponent(() => import('../../routes/extraPages/routes/404'))}/> */}
    </Switch>
  </div>
);

export default Dashboard;