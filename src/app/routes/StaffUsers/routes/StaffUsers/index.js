import React, { Component } from "react";
import CustomTable from '../../../table/routes/data/Components/CustomTable';
import AddStaffDialog from '../../../components/routes/dialogs/addStaff/AddStaffDialog';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import CircularProgress from '@material-ui/core/CircularProgress';
import * as actions from '../../../../../actions';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
class StaffUsers extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: '1',
      openAddStaffeDialog: false,
    };
  }
  handleClickOpen = () => {
    this.setState({ openAddStaffeDialog: true });
  };
  handleRequestClose = () => {
    this.setState({ openAddStaffeDialog: false });
    this.props.staffUserClear();
  };
  componentWillMount() {
    this.props.staffUserGetAllStaffUsersRequest();
    this.props.staffUserGetAllUnallocatedDevices();
    this.props.staffShowLoader();
  }
  componentDidMount() {
    this.props.staffHideLoader();
  }

  render() {
    const { staffUsers, showMessage, loader, alertMessage } = this.props;
    const { openAddStaffeDialog } = this.state;
    return (
      <div className="dashboard animated slideInUpTiny animation-duration-3">

        <div className="row accountsetting">

          <div className="col-12">

            <div className="jr-card card">
              <div className="d-flex flex-row flex-wrap align-items-center justify-content-between userhead">
                <div className="d-flex flex-row align-items-center justify-content-between">
                  <h2 className="text-white">Staff Users</h2>
                  <div className="d-block d-md-none d-lg-none d-xl-none">
                  <Button className="customButton"
                      variant="contained"
                      onClick={this.handleClickOpen}
                      disabled={loader}
                    >Add Staff</Button>
                  </div>
                </div>
                <div className="d-flex flex-row flex-wrap">
                  {/* <div className="formgroup">
                    <img src={require('../../../../../assets/images/search.png')} />
                    <TextField
                      type="text"
                      placeholder="Search"
                      fullWidth
                      onChange={(event) => this.setState({ password: event.target.value })}
                      defaultValue={password}
                      margin="normal"
                      className="mt-0 my-0 custominput"
                    />
                  </div> */}
                  <div className="d-none d-md-block d-lg-block d-xl-block">
                    <Button className="customButton"
                      variant="contained"
                      onClick={this.handleClickOpen}
                      disabled={loader}
                    >Add Staff</Button>
                  </div>
                </div>
              </div>
              {loader ?
                (<div className="loader-view">
                  <CircularProgress />
                </div>) : staffUsers ? staffUsers.customers != undefined ? staffUsers.customers.length >0 ?
                  (<div className="row">
                    <div className="col-12">
                      <CustomTable
                        staffUsers={staffUsers}
                        deleteStaffUser={this.props.deleteStaffUser}
                        staffUserStatusChanged={this.props.staffUserStatusChanged}
                        staffUserEditFillUp={this.props.staffUserEditFillUp}
                        staffUserClear={this.props.staffUserClear}
                      />
                    </div>
                  </div>) : (<h2 className="text-white"> There is not Data</h2>) : (<h2 className="text-white"> There is not Data</h2>) :(<h2 className="text-white"> There is not Data</h2>)
              }
            </div>

          </div>
          {openAddStaffeDialog ?
            <AddStaffDialog open={openAddStaffeDialog}
              handleRequestClose={this.handleRequestClose}
            /> : null}
        </div>
        {showMessage && NotificationManager.error(alertMessage)}
        <NotificationContainer />
      </div>
    );
  }
}

const mapStateToProps = ({ staff }) => {
  const { staffUsers, loader, alertMessage, showMessage, showMessageSuccess, alertMessageSuccess } = staff;
  return { staffUsers, loader, alertMessage, showMessage, showMessageSuccess, alertMessageSuccess }
};
const mapDispatchToProps = dispatch => ({
  staffUserGetAllStaffUsersRequest: () => dispatch(actions.staffUserGetAllStaffUsersRequest()),
  staffHideMessage: () => dispatch(actions.staffHideMessage()),
  staffHideMessageSuccess: () => dispatch(actions.staffHideMessageSuccess()),
  deleteStaffUser: payload => dispatch(actions.deleteStaffUser(payload)),
  staffShowLoader: () => dispatch(actions.staffShowLoader()),
  staffHideLoader: () => dispatch(actions.staffHideLoader()),
  staffUserStatusChanged: payload => dispatch(actions.staffUserStatusChanged(payload)),
  staffUserEditFillUp: payload => dispatch(actions.staffUserEditFillUp(payload)),
  staffUserGetUserDevices: () => dispatch(actions.staffUserGetUserDevices()),
  staffUserGetAllUnallocatedDevices: () => dispatch(actions.staffUserGetAllUnallocatedDevices()),
  staffUserClear: () => dispatch(actions.staffUserClear())


});
export default connect(mapStateToProps, mapDispatchToProps)(StaffUsers);