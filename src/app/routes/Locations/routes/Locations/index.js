import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import { NavLink } from 'react-router-dom';
import AddDeviceDialog from '../../../components/routes/dialogs/addDevice/AddDeviceDialog';
import { connect } from 'react-redux';
import * as actions from '../../../../../actions';
import Switch from '@material-ui/core/Switch';
import DeleteDialog from '../../../components/routes/dialogs/delete/DeleteDialog';
import CircularProgress from '@material-ui/core/CircularProgress';
import AddLocationDialog from '../../../components/routes/dialogs/addLocation/AddLocationDialog';


const actionsStyles = theme => ({
  root: {
    flexShrink: 0,
    color: theme.palette.text.secondary,
    marginLeft: theme.spacing.unit * 2.5,
  },
});

class TablePaginationActions extends React.Component {
  handleFirstPageButtonClick = event => {
    this.props.onChangePage(event, 0);
  };

  handleBackButtonClick = event => {
    this.props.onChangePage(event, this.props.page - 1);
  };

  handleNextButtonClick = event => {
    this.props.onChangePage(event, this.props.page + 1);
  };

  handleLastPageButtonClick = event => {
    this.props.onChangePage(
      event,
      Math.max(0, Math.ceil(this.props.count / this.props.rowsPerPage) - 1),
    );
  };


  render() {
    const { classes, count, page, rowsPerPage, theme } = this.props;

    return (
      <div className={classes.root}>
        <IconButton
          onClick={this.handleFirstPageButtonClick}
          disabled={page === 0}
          aria-label="First Page"
        >
          {theme.direction === 'rtl' ?
            <i className="zmdi zmdi-skip-next" /> : <i className="zmdi zmdi-skip-previous" />}
        </IconButton>
        <IconButton
          onClick={this.handleBackButtonClick}
          disabled={page === 0}
          aria-label="Previous Page"
        >
          {theme.direction === 'rtl' ?
            <i className="zmdi zmdi-chevron-right" /> : <i className="zmdi zmdi-chevron-left" />}
        </IconButton>
        <IconButton
          onClick={this.handleNextButtonClick}
          disabled={page >= Math.ceil(count / rowsPerPage) - 1}
          aria-label="Next Page"
        >
          {theme.direction === 'rtl' ?
            <i className="zmdi zmdi-chevron-left" /> : <i className="zmdi zmdi-chevron-right" />}
        </IconButton>
        <IconButton
          onClick={this.handleLastPageButtonClick}
          disabled={page >= Math.ceil(count / rowsPerPage) - 1}
          aria-label="Last Page"
        >
          {theme.direction === 'rtl' ?
            <i className="zmdi zmdi-skip-previous" /> : <i className="zmdi zmdi-skip-next" />}
        </IconButton>
      </div>
    );
  }
}

TablePaginationActions.propTypes = {
  classes: PropTypes.object.isRequired,
  count: PropTypes.number.isRequired,
  onChangePage: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
  theme: PropTypes.object.isRequired,
};

const TablePaginationActionsWrapped = withStyles(actionsStyles, { withTheme: true })(
  TablePaginationActions,
);

let counter = 0;

function createData(name, calories, fat) {
  counter += 1;
  return { id: counter, name, calories, fat };
}

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
  },
  table: {
    minWidth: 500,
  },
  tableWrapper: {
    overflowX: 'auto',
  },
});

class Locations extends React.Component {

  constructor(props, context) {
    super(props, context);

    this.state = {
      page: 0,
      rowsPerPage: 10,
      checkedA: false,
      password: '',
      openAddLocationDialog: false,
      isEdit: false
    };
  }
  componentWillMount() {
    this.props.locationShowLoader();
    this.props.locationGetAllLocations();
  }
  componentDidMount() {
    this.props.locationHideLoader();
  }
  handleChangePage = (event, page) => {
    this.setState({ page });
  };
  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
  };
  handleRequestClose = () => {
    this.setState({ openAddLocationDialog: false, isEdit: false });
    this.props.locationClear();

  };
  handleClickOpen = () => {
    this.setState({ openAddLocationDialog: true })
  }

  render() {
    const { classes, allLocations, loader } = this.props;
    const { data, rowsPerPage, page, password, openAddLocationDialog } = this.state;
    return (
      <div className="dashboard animated slideInUpTiny animation-duration-3">

        {/* <ContainerHeader match={match} title={"Account Settings"}/> */}

        <div className="row accountsetting">

          <div className="col-12">
            <div className="jr-card card h-100vh mb-0">
              <div className="d-flex flex-row flex-wrap align-items-center justify-content-between userhead">
                <div className="d-flex flex-row align-items-center justify-content-between">
                  <h2 className="text-white">Locations</h2>
                  <div className="d-block d-md-none d-lg-none d-xl-none">
                    <Button className="customButton"
                      variant="contained"
                      onClick={this.handleClickOpen}
                      disabled={loader}
                    >Add Location</Button>
                  </div>

                </div>
                <div className="d-flex flex-row flex-wrap">
                  {/* <div className="formgroup">
                    <img src={require('../../../../../assets/images/search.png')} />
                    <TextField
                      type="text"
                      placeholder="Search"
                      fullWidth
                      onChange={(event) => this.setState({ password: event.target.value })}
                      defaultValue={password}
                      margin="normal"
                      className="mt-0 my-0 custominput"
                    />
                  </div> */}
                  <div className="d-none d-md-block d-lg-block d-xl-block">
                    <Button className="customButton"
                      variant="contained"
                      onClick={this.handleClickOpen}
                      disabled={loader}
                    >Add Location</Button>
                  </div>
                </div>
              </div>
              {loader ? (<div className="loader-view">
                <CircularProgress />
              </div>) : allLocations.length > 0 ?
                  (<div className="row">
                    <div className="col-12">
                      {/* <Paper className={classes.root}> */}
                      <div className={classes.tableWrapper}>

                        <Table className={classes.table}>
                          <TableHead>
                            <TableRow>
                              <TableCell>Address</TableCell>
                              <TableCell>City</TableCell>
                              <TableCell>State</TableCell>
                              <TableCell>Action</TableCell>
                            </TableRow>
                          </TableHead>
                          <TableBody>

                            {allLocations ?
                              allLocations != undefined ?
                                allLocations.map((item, index) => (
                                  <TableRow key={index}>
                                    <TableCell>{item.address}</TableCell>
                                    <TableCell>{item.city}</TableCell>
                                    <TableCell>{item.state}</TableCell>
                                    <TableCell>
                                      <NavLink to={`/app/LocationInformation/${item.locationId}`}>
                                        <IconButton className="icon-btn">
                                          <i className="zmdi zmdi-eye" />
                                        </IconButton>
                                      </NavLink>
                                      <IconButton className="icon-btn" >
                                        <i className="zmdi zmdi-edit"
                                          onClick={() => {
                                            this.props.locationLocationEditFillUp(item);
                                            this.setState({ openAddLocationDialog: true, isEdit: true });
                                          }}

                                        />
                                      </IconButton>
                                      <IconButton className="icon-btn">
                                        <DeleteDialog
                                          key={index}
                                          deleteLocation={this.props.deleteLocation}
                                          locationId={item.locationId}
                                          isLocation
                                        />
                                      </IconButton>
                                    </TableCell>
                                  </TableRow>
                                )) : null : null}

                          </TableBody>
                          {/* <TableFooter>
                        <TableRow>
                          <TablePagination
                            count={allDevices.length}
                            rowsPerPage={rowsPerPage}
                            page={page}
                            onChangePage={this.handleChangePage}
                            onChangeRowsPerPage={this.handleChangeRowsPerPage}
                            ActionsComponent={TablePaginationActionsWrapped}
                          />
                        </TableRow>
                      </TableFooter> */}
                        </Table>
                      </div>
                      {/* </Paper> */}
                    </div>
                  </div>) : (<h2 className="text-white"> There is not Data</h2>)}
            </div>
          </div>
        </div>
        {openAddLocationDialog ? <AddLocationDialog
          open={openAddLocationDialog}
          handleRequestClose={this.handleRequestClose}
          isEdit={this.state.isEdit}
        /> : null}
      </div>
    );
  }
}

Locations.propTypes = {
  classes: PropTypes.object.isRequired,
};

const mapStateToProps = ({ location }) => {
  const { allLocations, loader } = location;
  return { allLocations, loader }
};
const mapDispatchToProps = dispatch => ({
  locationGetAllLocations: () => dispatch(actions.locationGetAllLocations()),
  locationShowLoader: () => dispatch(actions.locationShowLoader()),
  locationHideLoader: () => dispatch(actions.locationHideLoader()),
  deleteLocation: payload => dispatch(actions.deleteLocation(payload)),
  locationLocationEditFillUp: payload => dispatch(actions.locationLocationEditFillUp(payload)),
  locationClear : () => dispatch(actions.locationClear())
});
export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(Locations));