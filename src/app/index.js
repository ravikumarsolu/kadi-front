import React from 'react';
import { Route, Switch, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import Header from 'components/Header/index';
import Sidebar from 'containers/SideNav/index';
import Dashboard from './routes/Dashboard';
// import SuperAdminDashboard from './SuperAdmin/dashboard';

import {
  ABOVE_THE_HEADER,
  BELOW_THE_HEADER,
  COLLAPSED_DRAWER,
  FIXED_DRAWER,
  HORIZONTAL_NAVIGATION,
} from 'constants/ActionTypes';
import ColorOption from 'containers/Customizer/ColorOption';
import { isIOS, isMobile } from 'react-device-detect';
import asyncComponent from '../util/asyncComponent';
import TopNav from 'components/TopNav';
import SidenavContentMobile from "../containers/SideNav/SidenavContentMobile";

class App extends React.Component {

  render() {
    let userRole = localStorage.getItem("role");
    const { match, drawerType, navigationStyle, horizontalNavPosition } = this.props;
    const drawerStyle = drawerType.includes(FIXED_DRAWER) ? 'fixed-drawer' : drawerType.includes(COLLAPSED_DRAWER) ? 'collapsible-drawer' : 'mini-drawer';

    //set default height and overflow for iOS mobile Safari 10+ support.
    if (isIOS && isMobile) {
      document.body.classList.add('ios-mobile-view-height')
    }
    else if (document.body.classList.contains('ios-mobile-view-height')) {
      document.body.classList.remove('ios-mobile-view-height')
    }

    return (
      <div className={`app-container ${drawerStyle}`}>
        {/* <Tour /> */}

        <Sidebar />
        <SidenavContentMobile />
        <div className="app-main-container">
          <div
            className={`app-header ${navigationStyle === HORIZONTAL_NAVIGATION ? 'app-header-horizontal' : ''}`}>
            {(navigationStyle === HORIZONTAL_NAVIGATION && horizontalNavPosition === ABOVE_THE_HEADER) &&
              <TopNav styleName="app-top-header" />}
            <Header />
            {(navigationStyle === HORIZONTAL_NAVIGATION && horizontalNavPosition === BELOW_THE_HEADER) &&
              <TopNav />}
          </div>

          <main className="app-main-content-wrapper">
            <div className="app-main-content">
              {
                userRole == "admin" ?
                  <Switch>
                    <Route path={`${match.url}/dashboard`} exact component={Dashboard} />
                    <Route path={`${match.url}/myaccount`}
                      component={asyncComponent(() => import('./routes/AccountSettings'))} />

                    <Route path={`${match.url}/devices`}
                      component={asyncComponent(() => import('./routes/Devices'))} />
                    <Route path={`${match.url}/control-devices`}
                      component={asyncComponent(() => import('./routes/ControlDevice'))} />
                    <Route path={`${match.url}/locations`}
                      component={asyncComponent(() => import('./routes/Locations'))} />
                    <Route path={`${match.url}/staffusers`}
                      component={asyncComponent(() => import('./routes/StaffUsers'))} />
                    <Route path={`${match.url}/userinforamtion`}
                      component={asyncComponent(() => import('./routes/UserInforamtion'))} />
                    <Route path={`${match.url}/deviceinforamtion`}
                      component={asyncComponent(() => import('./routes/DeviceInforamtion'))} />
                    <Route path={`${match.url}/locationinformation`}
                      component={asyncComponent(() => import('./routes/LocationInforamtion'))} />
                    <Route path={`${match.url}/subscription`}
                      component={asyncComponent(() => import('./routes/Subscription'))} />
                    <Route path={`${match.url}/billinginfo`}
                      component={asyncComponent(() => import('./routes/BillingInfo'))} />
                    <Route path={`${match.url}/invoice`}
                      component={asyncComponent(() => import('./routes/Invoice'))} />
                    <Route path={`${match.url}/invoiceDownlaod`}
                      component={asyncComponent(() => import('./routes/InvoiceDownload/routes'))} />
                    <Route path={`${match.url}/statistics`}
                      component={asyncComponent(() => import('./routes/Statistics'))} />
                    <Route component={asyncComponent(() => import('./routes/ExtraPages/routes/404'))} />
                  </Switch> :
                  userRole == "user" ?
                    <Switch>
                      <Route path={`${match.url}/user/dashboard`} exact component={Dashboard} />
                      <Route path={`${match.url}/user/myaccount`}
                        component={asyncComponent(() => import('./routes/AccountSettings'))} />
                      <Route path={`${match.url}/user/devices`}
                        component={asyncComponent(() => import('./routes/Devices'))} />
                      <Route path={`${match.url}/user/deviceinforamtion`}
                        component={asyncComponent(() => import('./routes/DeviceInforamtion'))} />
                      <Route component={asyncComponent(() => import('./routes/ExtraPages/routes/404'))} />
                    </Switch> :


                    <Switch>
                      <Route component={asyncComponent(() => import('./routes/ExtraPages/routes/404'))} />
                    </Switch>
              }
            </div>
          </main>
        </div>
      </div>
    );
  }
}


const mapStateToProps = ({ settings }) => {
  const { drawerType, navigationStyle, horizontalNavPosition } = settings;
  return { drawerType, navigationStyle, horizontalNavPosition }
};
export default withRouter(connect(mapStateToProps)(App));