import {  BILLING_GET_ALL_PAYMENT_HISTORY, BILLING_GET_ALL_PAYMENT_HISTORY_SUCCESS, BILLING_HIDE_LOADER, BILLING_SHOW_LOADER, BILLING_GET_INVOICE_DETAILS, BILLING_GET_INVOICE_DETAILS_SUCCESS, BILILNG_MOBILE_NUMBER_CHANGED, BILLING_PAY_BILL } from "../constants/ActionTypes";

export const billingGetAllPaymentHistory = payload => {
    return {
        type: BILLING_GET_ALL_PAYMENT_HISTORY,
        payload
    };
};

export const billingGetAllPaymentHistorySuccess = payload => {
    return {
        type: BILLING_GET_ALL_PAYMENT_HISTORY_SUCCESS,
        payload
    };
};

export const billingGetInvoiceDetails = payload => {
    return {
        type: BILLING_GET_INVOICE_DETAILS,
        payload
    };
}


export const billingGetInvoiceDetailsSuccess = payload => {
    return {
        type: BILLING_GET_INVOICE_DETAILS_SUCCESS,
        payload
    };
}

export const billingShowLoader = () => {
    return{
        type:BILLING_SHOW_LOADER
    }
}

export const billingHideLoader = () => {
    return{
        type:BILLING_HIDE_LOADER
    }
}

export const billingMobileNumberChanged = payload =>{
    return{
        type:BILILNG_MOBILE_NUMBER_CHANGED,
        payload
    }
}


export const billingPayBill = payload =>{
    return{
        type:BILLING_PAY_BILL,
        payload
    }
}