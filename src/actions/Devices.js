import { DEVICE_DEVICE_NAME_CHANGED, DEVICE_DEVICE_IMEI_CHANGED, DEVICE_DEVICE_LOCATION_CHANGED, DEVICE_GET_ALL_DEVICE_REQUEST, DEVICE_GET_ALL_DEVICE_REQUEST_SUCCESS, DEVICE_DELETE_DEVICE, DEVICE_SHOW_LOADER, DEVICE_HIDE_LOADER, DEVICE_DEVICE_STATUS_CHANGED, DEVICE_DEVICE_ADD_REQUEST, DEVICE_DEVICE_ADD_REQUEST_SUCCESS, DEVICE_DEVICE_CLEAR, DEVICE_DEVICE_IP_CHANGED, DEVICE_GET_DEVICE_REQUEST, DEVICE_GET_DEVICE_REQUEST_SUCCESS, DEVICE_DEVICE_EDIT_FILL_UP, DEVICE_DEVICE_STATUS_CHANGED_SUCCESS, DEVICE_GET_DEVICE_STATISTICS, DEVICE_GET_DEVICE_STATISTICS_SUCCESS, DEVICE_GET_ALL_LOCATION_REQUEST, DEVICE_GET_ALL_LOCATION_REQUEST_SUCCESS } from "../constants/ActionTypes";

export const deviceDeviceNameChanged = payload => {
    return {
        type: DEVICE_DEVICE_NAME_CHANGED,
        payload
    };
};

export const deviceDeviceIMEIChanged = payload => {
    return {
        type: DEVICE_DEVICE_IMEI_CHANGED,
        payload
    };
};

export const deviceDeviceLocationChanged = payload => {
    return {
        type: DEVICE_DEVICE_LOCATION_CHANGED,
        payload
    };
};

export const deviceGetAllDeviceRequest = () => {
    return {
        type:DEVICE_GET_ALL_DEVICE_REQUEST
    }
};

export const deviceGetAllDeviceRequestSuccess = payload => {
    return {
        type:DEVICE_GET_ALL_DEVICE_REQUEST_SUCCESS,
        payload
    }
}

export const deviceGetDeviceRequest = payload => {
    return {
        type:DEVICE_GET_DEVICE_REQUEST,
        payload
    }
};

export const deviceGetDeviceRequestSuccess = payload => {
    return {
        type:DEVICE_GET_DEVICE_REQUEST_SUCCESS,
        payload
    }
}

export const deleteDevice = payload => {
    return{
        type:DEVICE_DELETE_DEVICE,
        payload
    }
}


export const deviceShowLoader = () => {
    return{
        type:DEVICE_SHOW_LOADER
    }
}

export const deviceHideLoader = () => {
    return{
        type:DEVICE_HIDE_LOADER
    }
}

export const deviceDeviceStatusChanged = payload => {
    return{
        type:DEVICE_DEVICE_STATUS_CHANGED,
        payload
    }
}

export const deviceDeviceStatusChangedSuccess = payload => {
    return{
        type:DEVICE_DEVICE_STATUS_CHANGED_SUCCESS,
        payload
    }
}

export const deviceDeviceAddRequest = payload => {
    return{
        type:DEVICE_DEVICE_ADD_REQUEST,
        payload
    }
}

export const deviceDeviceAddRequestSuccess = payload => {
    return{
        type:DEVICE_DEVICE_ADD_REQUEST_SUCCESS,
        payload
    }
}

export const deviceDeviceClear = () => {
    return{
        type: DEVICE_DEVICE_CLEAR
    }
}

export const deviceDeviceIPChanged = payload => {
    return{
        type:DEVICE_DEVICE_IP_CHANGED,
        payload
    }
}

export const deviceDeviceEditFillUp = payload => {
    return{
        type:DEVICE_DEVICE_EDIT_FILL_UP,
        payload
    }
}

export const deviceGetDeviceStatistics = payload => {
    return {
        type:DEVICE_GET_DEVICE_STATISTICS,
        payload
    }
};

export const deviceGetDeviceStatisticsSuccess = payload => {
    return {
        type:DEVICE_GET_DEVICE_STATISTICS_SUCCESS,
        payload
    }
};

export const deviceGetAllLocationRequest = () => {

        return{
            type:DEVICE_GET_ALL_LOCATION_REQUEST
        }
}

export const deviceGetAllLocationRequestSuccess = payload => {

    return{
        type:DEVICE_GET_ALL_LOCATION_REQUEST_SUCCESS,
        payload
    }
}