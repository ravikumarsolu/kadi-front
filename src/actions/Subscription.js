import { SUBSCRIPTION_GET_USER_SUBSCRIPTION_DETAILS, SUBSCRIPTION_SHOW_LOADER, SUBSCRIPTION_HIDE_LOADER, SUBSCRIPTION_GET_USER_SUBSCRIPTION_DETAILS_SUCCESS, SUBSCRIPTION_GET_USER_SUBSCRIPTION_PLANS, SUBSCRIPTION_GET_USER_SUBSCRIPTION_PLANS_SUCCESS, SUBSCRIPTION_BUY_SUBSCRIPTION_PLAN, SUBSCRIPTION_MOBILE_NUMBER_CHANGED, SUBSCRIPTION_BUY_SUBSCRIPTION_PLAN_SUCCESS } from "../constants/ActionTypes";



export const subscriptionShowLoader = payload => {
    return{
        type: SUBSCRIPTION_SHOW_LOADER,
        payload
    }
}

export const subscriptionHideLoader = payload => {
    return{
        type: SUBSCRIPTION_HIDE_LOADER,
        payload

    }
}

export const subscriptionGetUserSubscriptionDetails = payload => {
    return{
        type:SUBSCRIPTION_GET_USER_SUBSCRIPTION_DETAILS,
        payload
    }
}

export const subscriptionGetUserSubscriptionDetailsSuccess = payload => {
    return{
        type:SUBSCRIPTION_GET_USER_SUBSCRIPTION_DETAILS_SUCCESS,
        payload
    }
}

export const subscriptionGetUserSubscriptionPlans = payload => {
    return{
        type:SUBSCRIPTION_GET_USER_SUBSCRIPTION_PLANS,
        payload
    }
}

export const subscriptionGetUserSubscriptionPlansSuccess = payload => {
    return{
        type:SUBSCRIPTION_GET_USER_SUBSCRIPTION_PLANS_SUCCESS,
        payload
    }
}

export const subscriptionBuySubscriptionPlan = payload => {
    return{
        type:SUBSCRIPTION_BUY_SUBSCRIPTION_PLAN,
        payload
    }
}


export const subscriptionBuySubscriptionPlanSuccess = payload => {
    return{
        type:SUBSCRIPTION_BUY_SUBSCRIPTION_PLAN_SUCCESS,
        payload
    }
}

export const subscriptionMobileNumberChanged = (payload) => {
    return{
        type:SUBSCRIPTION_MOBILE_NUMBER_CHANGED,
        payload
    }
}