import { LOCATION_LOCATION_ADDRESS_1_CHANGED, LOCATION_LOCATION_ADDRESS_2_CHANGED, LOCATION_GET_ALL_LOCATIONS, LOCATION_GET_ALL_LOCATIONS_SUCCESS, LOCATION_SHOW_LOADER, LOCATION_HIDE_LOADER, LOCATION_DELETE_LOCATION, LOCATION_DELETE_LOCATION_SUCCESS, LOCATION_ADD_LOCATION, LOCATION_LOCATION_STATE_CHANGED, LOCATION_LOCATION_CITY_CHANGED, LOCATION_LOCATION_EDIT_FILLUP, LOCATION_LOCATION_CLEAR, LOCATION_GET_LOCATION, LOCATION_GET_LOCATION_SUCCESS } from "../constants/ActionTypes";

export const locationLocationAddress1Changed = payload => {
    return {
        type: LOCATION_LOCATION_ADDRESS_1_CHANGED,
        payload
    };
};

export const locationLocationStateChanged = payload => {
    return {
        type: LOCATION_LOCATION_STATE_CHANGED,
        payload
    };
};

export const locationLocationCityChanged = payload => {
    return {
        type: LOCATION_LOCATION_CITY_CHANGED,
        payload
    };
};


export const locationLocationAddress2Changed = payload => {
    return {
        type: LOCATION_LOCATION_ADDRESS_2_CHANGED,
        payload
    };
};


export const locationGetAllLocations = () => {
    return{
        type: LOCATION_GET_ALL_LOCATIONS
    }
}

export const locationGetAllLocationsSuccess = payload => {
    return{
        type: LOCATION_GET_ALL_LOCATIONS_SUCCESS,
        payload
    }
}

export const locationShowLoader = () => {
    return{
        type: LOCATION_SHOW_LOADER
    }
}

export const locationHideLoader = () => {
    return{
        type: LOCATION_HIDE_LOADER
    }
}

export const deleteLocation = payload => {
    return{
        type:LOCATION_DELETE_LOCATION,
        payload
    }
}

export const deleteLocationSuccess = payload => {
    return{
        type:LOCATION_DELETE_LOCATION_SUCCESS,
        payload
    }
}

export const locationAddLocation = payload =>{
    return{
        type:LOCATION_ADD_LOCATION,
        payload

    }
}

export const locationLocationEditFillUp = payload => {
    return{
        type:LOCATION_LOCATION_EDIT_FILLUP,
        payload
    }
}

export const locationClear=()=>{
    return{
        type:LOCATION_LOCATION_CLEAR
    }
}

export const locationGetLocation = payload => {
    return{
        type:LOCATION_GET_LOCATION,
        payload
    }
}

export const locationGetLocationSuccess = payload => {
    return{
        type:LOCATION_GET_LOCATION_SUCCESS,
        payload
    }
}