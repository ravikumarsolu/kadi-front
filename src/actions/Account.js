import { ACCOUNT_GET_ALL_DEVICES, ACCOUNT_SHOW_LOADER, ACCOUNT_HIDE_LOADER, ACCOUNT_GET_ALL_DEVICES_SUCCESS, ACCOUNT_HELP_SUPPORT_DESCRIPTION_CHANGED, ACCOUNT_HELP_SUPPORT_REQUEST, ACCOUNT_HELP_SUPPORT_REQUEST_SUCCESS, ACCOUNT_CURRENT_PASSWORD_CHANGED, ACCOUNT_NEW_PASSWORD_CHANGED, ACCOUNT_CONFIRM_PASSWORD_CHANGED, ACCOUNT_CLEAR_CHANGE_PASSWORD, ACCOUNT_CHANGE_PASSWORD_REQUEST } from "../constants/ActionTypes";



export const accountShowLoader = payload => {
    return {
        type: ACCOUNT_SHOW_LOADER,
        payload
    }
}

export const accountHideLoader = payload => {
    return {
        type: ACCOUNT_HIDE_LOADER,
        payload
    }
}


export const accountGetAllDevices = () => {
    return {
        type: ACCOUNT_GET_ALL_DEVICES
    }
}


export const accountGetAllDevicesSuccess = payload => {
    return {
        type: ACCOUNT_GET_ALL_DEVICES_SUCCESS,
        payload
    }
}

export const accountHelpSupportDescriptionChanged = payload => {
    return {
        type: ACCOUNT_HELP_SUPPORT_DESCRIPTION_CHANGED,
        payload
    }
}

export const accountHelpSupportRequest = payload => {
    return {
        type: ACCOUNT_HELP_SUPPORT_REQUEST,
        payload
    }
}

export const accountHelpSupportRequestSuccess = payload => {
    return {
        type: ACCOUNT_HELP_SUPPORT_REQUEST_SUCCESS,
        payload
    }
}

export const accountCurrentPasswordChanged= payload =>{
    return{
        type:ACCOUNT_CURRENT_PASSWORD_CHANGED,
        payload
    }
}

export const accountNewPasswordChanged= payload => {
    return{
        type:ACCOUNT_NEW_PASSWORD_CHANGED,
        payload
    }
}

export const accountConfirmPasswordChanged= payload =>{
    return{
        type:ACCOUNT_CONFIRM_PASSWORD_CHANGED,
        payload
    }
}

export const accountClearChangePassword = () => {
    return{
        type:ACCOUNT_CLEAR_CHANGE_PASSWORD
    }
}


export const accountChangePasswordRequest = payload => {
    return{
        type:ACCOUNT_CHANGE_PASSWORD_REQUEST,
        payload
    }
}

export const accountChangePasswordRequestSuccess = payload => {
    return{
        type:ACCOUNT_CHANGE_PASSWORD_REQUEST,
        payload
    }
}