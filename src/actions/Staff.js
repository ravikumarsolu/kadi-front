import { STAFF_USER_CONTACT_NUMBER_CHANGED, STAFF_USER_DEVICE_NAME_CHANGED, STAFF_USER_EMAIL_CHANGED, STAFF_USER_DEVICE_CHANGED, STAFF_USER_ADD_REQUEST, STAFF_USER_FIRST_NAME_CHANGED, STAFF_USER_LAST_NAME_CHANGED, STAFF_USER_CLEAR, STAFF_USER_GET_ALL_STAFF_USERS_REQUEST, STAFF_USER_GET_ALL_STAFF_USERS_REQUEST_SUCCESS, STAFF_SHOW_MESSAGE, STAFF_HIDE_MESSAGE, STAFF_HIDE_MESSAGE_SUCCESS, STAFF_SHOW_MESSAGE_SUCCESS, STAFF_SHOW_LOADER, STAFF_HIDE_LOADER, DELETE_STAFF_USER, DELETE_STAFF_USER_SUCCESS, STAFF_USER_GET_STAFF_USERS_REQUEST, STAFF_USER_GET_STAFF_USERS_REQUEST_SUCCESS, STAFF_USER_STATUS_CHANGED, STAFF_USER_EDIT_FILL_UP, STAFF_USER_GET_USER_DEVICES, STAFF_USER_GET_USER_DEVICES_SUCCESS, STAFF_USER_GET_ALL_UNALLOCATED_DEVICES, STAFF_USER_GET_ALL_UNALLOCATED_DEVICES_SUCCESS, STAFF_USER_ADDRESS_CHANGED, STAFF_USER_STATUS_CHANGED_SUCCESS } from "../constants/ActionTypes";

export const staffUserFirstNameChanged = payload => {
    return {
        type: STAFF_USER_FIRST_NAME_CHANGED,
        payload
    };
};

export const staffUserLastNameChanged = payload => {
    return {
        type: STAFF_USER_LAST_NAME_CHANGED,
        payload
    };
};

export const staffUserContactNumberChanged = payload => {
    return {
        type: STAFF_USER_CONTACT_NUMBER_CHANGED,
        payload
    };
};

export const staffUserEmailChanged = payload => {
    return {
        type: STAFF_USER_EMAIL_CHANGED,
        payload
    };
};

export const staffUserAddressChanged = payload => {
    return {
        type: STAFF_USER_ADDRESS_CHANGED,
        payload
    };
};

export const staffUserDeviceNameChanged = payload => {
    return {
        type: STAFF_USER_DEVICE_NAME_CHANGED,
        payload
    };
};

export const staffUserDeviceChanged = payload => {
    return {
        type: STAFF_USER_DEVICE_CHANGED,
        payload
    };
};

export const staffUserAddRequest = payload => {
    return {
        type: STAFF_USER_ADD_REQUEST,
        payload
    };
};

export const staffUserClear = () => {
    return {
        type: STAFF_USER_CLEAR
    }
}

export const staffUserGetAllStaffUsersRequest = () => {
    return {
        type: STAFF_USER_GET_ALL_STAFF_USERS_REQUEST
    }
}

export const staffUserGetAllStaffUsersRequestSuccess = payload => {
    return {
        type: STAFF_USER_GET_ALL_STAFF_USERS_REQUEST_SUCCESS,
        payload
    }
}

export const staffUserGetStaffUsersRequest = payload => {
    return {
        type: STAFF_USER_GET_STAFF_USERS_REQUEST,
        payload
    }
}

export const staffUserGetStaffUsersRequestSuccess = payload => {
    return {
        type: STAFF_USER_GET_STAFF_USERS_REQUEST_SUCCESS,
        payload
    }
}

export const staffShowMessage = payload => {
    return {
        type: STAFF_SHOW_MESSAGE,
        payload
    }
}

export const staffHideMessage = () => {
    return {
        type: STAFF_SHOW_MESSAGE
    }
}

export const staffShowMessageSuccess = payload => {
    return {
        type: STAFF_SHOW_MESSAGE_SUCCESS,
        payload
    }
}

export const staffHideMessageSuccess = () => {
    return {
        type: STAFF_HIDE_MESSAGE_SUCCESS
    }
}

export const staffShowLoader = () => {
    return {
        type: STAFF_SHOW_LOADER
    }
}

export const staffHideLoader = () => {
    return {
        type: STAFF_HIDE_LOADER
    }
}

export const deleteStaffUser = payload => {
    return {
        type: DELETE_STAFF_USER,
        payload
    }
}

export const deleteStaffUserSuccess = payload => {
    return {
        type: DELETE_STAFF_USER_SUCCESS,
        payload
    }
}

export const staffUserStatusChanged = payload => {
    return{
        type:STAFF_USER_STATUS_CHANGED,
        payload
    }
}

export const staffUserStatusChangedSuccess = payload => {
    return{
        type:STAFF_USER_STATUS_CHANGED_SUCCESS,
        payload
    }
}


export const staffUserEditFillUp = payload => {
    return{
        type:STAFF_USER_EDIT_FILL_UP,
        payload
    }
}

export const staffUserGetUserDevices = payload => {
    return{
        type:STAFF_USER_GET_USER_DEVICES,
        payload
    }
}

export const staffUserGetUserDevicesSuccess = payload => {
    return{
        type:STAFF_USER_GET_USER_DEVICES_SUCCESS,
        payload
    }
}

export const staffUserGetAllUnallocatedDevices = () => {
    return{
        type:STAFF_USER_GET_ALL_UNALLOCATED_DEVICES
    }
}

export const staffUserGetAllUnallocatedDevicesSuccess = payload => {
    return{
        type:STAFF_USER_GET_ALL_UNALLOCATED_DEVICES_SUCCESS ,
        payload
    }
}