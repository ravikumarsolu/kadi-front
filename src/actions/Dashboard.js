import { DASHBOARD_DASHBOARD_DEVICE_CHANGED, DASHBOARD_GET_ALL_DEVICES_SUCCESS, DASHBOARD_GET_ALL_DEVICES, DASHBOARD_SHOW_LOADER, DASHBOARD_HIDE_LOADER, DASHBOARD_DASHBOARD_DEVICE_CHANGED_SUCCESS } from "../constants/ActionTypes";

export const dashboardDashboardDeviceChanged = payload => {
    return {
        type: DASHBOARD_DASHBOARD_DEVICE_CHANGED,
        payload
    };
};

export const dashboardDashboardDeviceChangedSuccess = payload => {
    return {
        type: DASHBOARD_DASHBOARD_DEVICE_CHANGED_SUCCESS,
        payload
    };
};

export const dashboardGetAllDevices = () => {
    return {
        type: DASHBOARD_GET_ALL_DEVICES
    }
}

export const dashboardGetAllDevicesSuccess = payload => {

    return {
        type: DASHBOARD_GET_ALL_DEVICES_SUCCESS,
        payload
    }
}

export const dashboardShowLoader = () => {
    return{
        type: DASHBOARD_SHOW_LOADER
    }
}

export const dashboardHideLoader = () => {
    return{
        type: DASHBOARD_HIDE_LOADER
    }
}